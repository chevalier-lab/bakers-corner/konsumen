package com.aitekteam.developer.bakerscornerapps.general

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import org.junit.After
import org.junit.Before
import org.junit.Test

class ProductViewTest {

    // utils
    private var itemCount: Int? = null
    private lateinit var recyclerview: RecyclerView

    // scenario
    private lateinit var activityScenario: ActivityScenario<HostActivity>

    @Before
    fun setUp() {
        activityScenario = ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun loadFeed() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_feed)).perform(click())

        // check shimmer visibility
        onView(withId(R.id.shimmer_product)).check(matches(withEffectiveVisibility(Visibility.GONE)))

        // check displayed view
        onView(withId(R.id.rv_feed)).check(matches(isDisplayed()))

        // get total item of rv
        activityScenario.onActivity { activity ->
            recyclerview = activity.findViewById(R.id.rv_feed)
            itemCount = recyclerview.adapter?.itemCount
        }

        itemCount?.let {
            // scroll to end of list
            onView(withId(R.id.rv_feed)).scrollTo(it.minus(1))
        }
    }

    @Test
    fun searchProduct() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_feed)).perform(click())

        // check shimmer visibility
        onView(withId(R.id.shimmer_product)).check(matches(withEffectiveVisibility(Visibility.GONE)))

        // check displayed view
        onView(withId(R.id.rv_feed)).check(matches(isDisplayed()))

        // perform search
        onView(withId(R.id.et_search)).perform(typeText("muffin"), closeSoftKeyboard())
    }

    @Test
    fun filterProduct() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_feed)).perform(click())

        // check shimmer visibility
        onView(withId(R.id.shimmer_product)).check(matches(withEffectiveVisibility(Visibility.GONE)))

        // check displayed view
        onView(withId(R.id.rv_feed)).check(matches(isDisplayed()))

        // perform filter
        onView(withId(R.id.layout_filter)).perform(click())
        onView(withId(R.id.select_harga_termahal)).perform(click())
        onView(withId(R.id.radio_harga_termahal)).check(matches(isChecked()))
        onView(withId(R.id.btn_submit)).perform(click())

        // get total item of rv
        activityScenario.onActivity { activity ->
            recyclerview = activity.findViewById(R.id.rv_feed)
            itemCount = recyclerview.adapter?.itemCount
        }

        itemCount?.let {
            // scroll to end of list
            onView(withId(R.id.rv_feed)).scrollTo(it.minus(1))
        }
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }

    private fun ViewInteraction.scrollTo(pos: Int) {
        perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(pos))
    }
}