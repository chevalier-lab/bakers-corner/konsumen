package com.aitekteam.developer.bakerscornerapps.general

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import org.junit.After
import org.junit.Before
import org.junit.Test

class WishlistTest {

    @Before
    fun setUp() {
        ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun addToWishlist() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_feed)).perform(click())

        // check shimmer visibility
        onView(withId(R.id.shimmer_product)).check(matches(withEffectiveVisibility(Visibility.GONE)))

        // check displayed view
        onView(withId(R.id.rv_feed)).check(matches(isDisplayed()))

        // perform navigate
        onView(withId(R.id.rv_feed)).atItem(0, click())

        // check displayed layout (product detail)
        onView(withId(R.id.layout_shimmer)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.layout_main)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.layout_button)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        // perform favorite
        onView(withId(R.id.btn_favorite)).perform(click())
    }

    @Test
    fun loadWishlist() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_wishlist)).perform(click())

        // check displayed view
        onView(withId(R.id.shimmer_wishlist)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.rv_wishlist)).check(matches(isDisplayed()))
    }

    private fun ViewInteraction.atItem(pos: Int, action: ViewAction) {
        perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(pos, action)
        )
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }
}