package com.aitekteam.developer.bakerscornerapps.transaction

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.NestedScrollViewExtension
import junit.framework.AssertionFailedError
import org.junit.After
import org.junit.Before
import org.junit.Test

class TransactionProductTest {

    @Before
    fun setUp() {
        ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun doProductOrder() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // load home
        loadHome()

        // perform navigate to product detail
        onView(withId(R.id.rv_recomended_product)).atItem(0, click())

        // check displayed layout (product detail)
        onView(withId(R.id.layout_shimmer)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.layout_main)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.layout_button)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        // perform buy
        onView(withId(R.id.btn_buy_cart)).perform(click())
        onView(withId(R.id.btn_plus)).perform(click())
        onView(withId(R.id.btn_submit)).perform(click())

        // perform navigate to cart
        onView(withId(R.id.favorite)).perform(click())
        onView(withId(R.id.rv_cart)).check(matches(isDisplayed()))
    }

    @Test
    fun doCheckoutProduct() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_cart)).perform(click())

        // perform navigate to checkout
        onView(withId(R.id.btn_submit_checkout)).perform(click())
        onView(withId(R.id.select_cash)).perform(click())
        onView(withId(R.id.radio_cash)).check(matches(isChecked()))

        // perform check voucher
        onView(withId(R.id.card_check_voucher)).perform(NestedScrollViewExtension(), click())
        onView(withId(R.id.et_voucher)).perform(typeText(VOUCHER_CODE), closeSoftKeyboard())
        onView(withId(R.id.btn_submit)).perform(click())

        // perform checkout
        onView(withId(R.id.btn_submit_checkout)).perform(NestedScrollViewExtension(), click())
        onView(withId(R.id.btn_positive)).perform(click())

        // check displayed view
        onView(withId(R.id.tv_success)).check(matches(withText(R.string.transaksi_berhasil)))
    }

    @Test
    fun filterTransaction() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_transaction)).perform(click())

        // check displayed view
        onView(withId(R.id.shimmer_trx)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.rv_transaction)).check(matches(isDisplayed()))

        // perform filter
        onView(withId(R.id.layout_filter)).perform(click())
        onView(withId(R.id.select_wait_confirm)).perform(click())
        onView(withId(R.id.radio_wait_confirm)).check(matches(isChecked()))

        // perform submit
        onView(withId(R.id.btn_submit)).perform(click())

        // check displayed view
        if (onView(withId(R.id.layout_empty)).isExistLayout()) {
            onView(withId(R.id.rv_transaction)).check(matches(withEffectiveVisibility(Visibility.GONE)))
            onView(withId(R.id.layout_empty)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        } else {
            onView(withId(R.id.shimmer_trx)).check(matches(withEffectiveVisibility(Visibility.GONE)))
            onView(withId(R.id.rv_transaction)).check(matches(isDisplayed()))

            // perform navigate
            onView(withId(R.id.rv_transaction)).atItem(0, click())
        }
    }
    
    private fun loadHome() {
        onView(withId(R.id.shimmer_banner))
            .check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.shimmer_cateogry))
            .check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.shimmer_recommended_product))
            .check(matches(withEffectiveVisibility(Visibility.GONE)))

        // check displayed view
        onView(withId(R.id.announcement)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_category)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_recomended_product)).check(matches(isDisplayed()))
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }

    private fun ViewInteraction.isExistLayout(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: AssertionFailedError) {
            false
        }
    }

    private fun ViewInteraction.atItem(pos: Int, action: ViewAction) {
        perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(pos, action)
        )
    }

    companion object {
        const val VOUCHER_CODE = "SAATNYATHR"
    }
}