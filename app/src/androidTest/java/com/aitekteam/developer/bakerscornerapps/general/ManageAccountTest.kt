package com.aitekteam.developer.bakerscornerapps.general

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.aitekteam.developer.bakerscornerapps.utils.NestedScrollViewExtension
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import org.junit.After
import org.junit.Before
import org.junit.Test

class ManageAccountTest {

    @Before
    fun setUp() {
        ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun loadManageAccount() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_kelola_akun)).perform(click())

        // check displayed view
        onView(withId(R.id.container_manage_account)).check(matches(isDisplayed()))
    }

    @Test
    fun updateDataProfile() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_kelola_akun)).perform(click())

        // check displayed view
        onView(withId(R.id.container_manage_account))
            .check(matches(isDisplayed()))

        // perform type edit text
        onView(withId(R.id.et_full_name)).perform(
            clearText(),
            typeText(FULL_NAME),
            closeSoftKeyboard()
        )
        onView(withId(R.id.et_phone_number)).perform(
            clearText(),
            typeText(PHONE_NUMBER),
            closeSoftKeyboard()
        )
        onView(withId(R.id.et_address))
            .perform(NestedScrollViewExtension())
        onView(withId(R.id.et_address))
            .perform(clearText(), typeText(ADDRESS), closeSoftKeyboard())

        onView(withId(R.id.btn_submit)).perform(click())
        onView(withId(R.id.btn_positive)).perform(click())

    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }

    companion object {
        const val FULL_NAME = "Muhamad Azmi"
        const val PHONE_NUMBER = "08212082364"
        const val ADDRESS = "FIT Telkom University"
    }
}