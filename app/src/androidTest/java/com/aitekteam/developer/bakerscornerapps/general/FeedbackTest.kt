package com.aitekteam.developer.bakerscornerapps.general

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.NestedScrollViewExtension
import junit.framework.AssertionFailedError
import org.junit.After
import org.junit.Before
import org.junit.Test

class FeedbackTest {

    @Before
    fun setUp() {
        ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun loadMyFeedback() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_feedback)).perform(click())

        // check displayed view
        onView(withId(R.id.rv_feedback)).check(matches(isDisplayed()))
    }

    @Test
    fun loadDetailMyFeedback() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_feedback)).perform(click())

        // check displayed view
        onView(withId(R.id.shimmer_feedback)).
        check(matches(withEffectiveVisibility(Visibility.GONE)))

        if (onView(withId(R.id.rv_feedback)).isExist())
            onView(withId(R.id.rv_feedback)).atItem(0, click())

    }

    @Test
    fun loadFeedbackFromProductDetail() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_feed)).perform(click())

        // check shimmer visibility
        onView(withId(R.id.shimmer_product)).check(matches(withEffectiveVisibility(Visibility.GONE)))

        // check displayed view
        onView(withId(R.id.rv_feed)).check(matches(isDisplayed()))

        // perform navigate
        onView(withId(R.id.rv_feed)).atItem(0, click())

        // check displayed layout (product detail)
        onView(withId(R.id.layout_shimmer)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.layout_main)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.layout_button)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        // perform scroll
        onView(withId(R.id.layout_recommended_product)).perform(NestedScrollViewExtension())

        // check displayed feedback
        if (onView(withId(R.id.layout_feedback)).isExistLayout()) {
            onView(withId(R.id.btn_show_feedback)).perform(NestedScrollViewExtension(), click())
            onView(withId(R.id.layout_shimmer)).check(matches(withEffectiveVisibility(Visibility.GONE)))
            onView(withId(R.id.rv_feedback)).check(matches(isDisplayed()))
        }

    }

    private fun ViewInteraction.atItem(pos: Int, action: ViewAction) {
        perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(pos, action)
        )
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }

    private fun ViewInteraction.isExistLayout(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: AssertionFailedError) {
            false
        }
    }
}