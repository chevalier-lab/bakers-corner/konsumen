package com.aitekteam.developer.bakerscornerapps.services

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import org.junit.After
import org.junit.Before
import org.junit.Test

class ChatTest {

    @Before
    fun setUp() {
        ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun loadHistoryChat() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.chat)).perform(click())

        // check displayed view
        onView(withId(R.id.rv_chat)).check(matches(isDisplayed()))
    }

    @Test
    fun sendMessage() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.chat)).perform(click())

        // check displayed view
        onView(withId(R.id.rv_chat)).check(matches(isDisplayed()))

        // perform type some message
        onView(withId(R.id.et_message))
            .perform(typeText("halo admin"), closeSoftKeyboard())

        onView(withId(R.id.btn_send)).perform(click())
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }
}