package com.aitekteam.developer.bakerscornerapps.general

import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.web.assertion.WebViewAssertions.webContent
import androidx.test.espresso.web.matcher.DomMatchers.hasElementWithId
import androidx.test.espresso.web.sugar.Web.onWebView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import org.junit.After
import org.junit.Before
import org.junit.Test

class HomeViewTest {

    // utils
    private var itemCount: Int? = null
    private lateinit var recyclerview: RecyclerView

    // scenario
    private lateinit var activityScenario: ActivityScenario<HostActivity>

    @Before
    fun setUp() {
        activityScenario = ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun loadHome() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // check shimmer visibility
        hideShimmer()

        // check displayed view
        onView(withId(R.id.announcement)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_category)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_recomended_product)).check(matches(isDisplayed()))
    }

    @Test
    fun loadDetailBanner() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // check shimmer visibility
        hideShimmer()

        // check displayed web view
        onView(withId(R.id.image_slider)).perform(click())
        onWebView(withId(R.id.wv_banner)).check(webContent(hasElementWithId("title")))
        onWebView(withId(R.id.wv_banner)).check(webContent(hasElementWithId("update")))
        onWebView(withId(R.id.wv_banner)).check(webContent(hasElementWithId("banner-promo-img")))
    }

    @Test
    fun loadCategory() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // check shimmer visibility
        hideShimmer()

        // check displayed view
        onView(withId(R.id.rv_category)).check(matches(isDisplayed()))

        // get total item of rv
        activityScenario.onActivity { activity ->
            recyclerview = activity.findViewById(R.id.rv_category)
            itemCount = recyclerview.adapter?.itemCount
        }

        itemCount?.let {
            // scroll to end of list
            onView(withId(R.id.rv_category)).scrollTo(it.minus(1))

            // perform navigate
            onView(withId(R.id.rv_category)).atItem(it.minus(1), click())
        }

        // check displayed view
        onView(withId(R.id.vp_category)).check(matches(isDisplayed()))
        onView(withId(R.id.tab_layout_category)).check(matches(isDisplayed()))
    }

    @Test
    fun loadRecommendedProducts() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // check shimmer visibility
        hideShimmer()

        // check displayed view
        onView(withId(R.id.rv_recomended_product)).check(matches(isDisplayed()))

        // get total item of rv
        activityScenario.onActivity { activity ->
            recyclerview = activity.findViewById(R.id.rv_recomended_product)
            itemCount = recyclerview.adapter?.itemCount
        }

        itemCount?.let {
            // scroll to end of list
            onView(withId(R.id.rv_recomended_product)).scrollTo(it.minus(1))
        }

        // perform navigate
        onView(withId(R.id.tv_recommended_list)).perform(click())

        // check displayed view
        onView(withId(R.id.shimmer_product)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.rv_recommended)).check(matches(isDisplayed()))

        // perform search
        onView(withId(R.id.search)).perform(click())
        onView(isAssignableFrom(EditText::class.java)).perform(typeText("muffin"))
    }

    private fun hideShimmer() {
        onView(withId(R.id.shimmer_banner)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.shimmer_cateogry)).check(matches(withEffectiveVisibility(Visibility.GONE)))
        onView(withId(R.id.shimmer_recommended_product)).check(
            matches(
                withEffectiveVisibility(
                    Visibility.GONE
                )
            )
        )
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }

    private fun ViewInteraction.atItem(pos: Int, action: ViewAction) {
        perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(pos, action)
        )
    }

    private fun ViewInteraction.scrollTo(pos: Int) {
        perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(pos))
    }

}