package com.aitekteam.developer.bakerscornerapps.services

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.web.assertion.WebViewAssertions.webContent
import androidx.test.espresso.web.matcher.DomMatchers.hasElementWithId
import androidx.test.espresso.web.sugar.Web.onWebView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import org.junit.After
import org.junit.Before
import org.junit.Test

class AnnouncementTest {

    @Before
    fun setUp() {
        ActivityScenario.launch(HostActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun loadAnnouncement() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_announcement)).perform(click())

        // check displayed view
        onView(withId(R.id.rv_announcement))
            .check(matches(ViewMatchers.isDisplayed()))

        // check shimmer visibility
        val shimmer = onView(withId(R.id.shimmer_product))
        if (shimmer.isExist())
            shimmer.check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)))

    }

    @Test
    fun loadDetailAnnouncement() {
        // check feedback
        if (onView(withId(R.id.btn_swipe_sheet)).isExist()) {
            onView(withId(R.id.btn_swipe_sheet)).perform(click())
        }

        // perform navigate
        onView(withId(R.id.navigation_profile)).perform(click())
        onView(withId(R.id.btn_announcement)).perform(click())

        // check displayed view
        val rv = onView(withId(R.id.rv_announcement))
        rv.check(matches(ViewMatchers.isDisplayed()))

        // check shimmer visibility
        val shimmer = onView(withId(R.id.shimmer_product))
        if (shimmer.isExist()) {
            shimmer.check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)))
        }

        // perform navigate
        rv.atItem(0, click())

        onWebView(withId(R.id.wv_announcement)).check(webContent(hasElementWithId("title")))
        onWebView(withId(R.id.wv_announcement)).check(webContent(hasElementWithId("subtitle")))
    }

    private fun ViewInteraction.atItem(pos: Int, action: ViewAction) {
        perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(pos, action)
        )
    }

    private fun ViewInteraction.isExist(): Boolean {
        return try {
            check(matches(ViewMatchers.isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }
}