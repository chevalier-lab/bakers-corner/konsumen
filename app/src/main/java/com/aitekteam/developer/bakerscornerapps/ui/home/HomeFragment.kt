package com.aitekteam.developer.bakerscornerapps.ui.home

import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.core.content.pm.PackageInfoCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.Banner
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.Setting
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ProductDomain
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentHomeBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.banner.BannerActivity
import com.aitekteam.developer.bakerscornerapps.ui.home.category.CategoryActivity
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.home.recommended.RecommendedProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.detail.DetailAnnouncementActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.TransactionActivity
import com.aitekteam.developer.bakerscornerapps.ui.service.chat.ChatActivity
import com.aitekteam.developer.bakerscornerapps.ui.service.notification.NotificationActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.*
import com.bumptech.glide.Glide
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.interfaces.ItemClickListener
import com.denzcoskun.imageslider.models.SlideModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_host.*
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.android.synthetic.main.item_recommended_product.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by viewModel()

    // utils
    private var isLoading = true
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var categoryAdapter: ReusableAdapter<Category>
    private lateinit var productAdapter: ReusableAdapter<ProductDomain>
    private lateinit var recommendedProductAdapter: ReusableAdapter<Product>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        // toolbar
        setHasOptionsMenu(true)
        activity?.et_search?.setText("")
        activity?.toolbar_title?.text = getString(R.string.bakers_corner)
        activity?.search_bar?.visibility = View.GONE
        activity?.layout_filter?.visibility = View.GONE
        activity?.toolbar?.visibility = View.VISIBLE

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(requireActivity(), AUTH_TOKEN)
        categoryAdapter = ReusableAdapter(requireContext())
        productAdapter = ReusableAdapter(requireContext())
        recommendedProductAdapter = ReusableAdapter(requireContext())

        // setup adapter
        setupAdapter.product(binding.layoutProductHome.rvProduct)
        setupAdapter.category(binding.layoutCateogry.rvCategory)
        setupAdapter.recommendedProduct(binding.layoutRecommendedProduct.rvRecomendedProduct)

        // init UI
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    fun toFeedback() {
        startActivity(Intent(requireContext(), TransactionActivity::class.java))
    }

    private fun initUI() {
        binding.layoutCateogry.tvCategoryList.setOnClickListener {
            startActivity(Intent(requireContext(), CategoryActivity::class.java).putExtra(CategoryActivity.EXTRA_INDEX, 0))
        }

        binding.layoutRecommendedProduct.tvRecommendedList.setOnClickListener {
            startActivity(Intent(requireContext(), RecommendedProductActivity::class.java))
        }

        binding.layoutProductHome.tvFeedList.setOnClickListener {
            it.findNavController().navigate(R.id.navigation_feed)
        }

        binding.layoutProductHome.btnShowMore.setOnClickListener {
            it.findNavController().navigate(R.id.navigation_feed)
        }

        sharedPref.get(AUTH_TOKEN)?.let { token ->
            // fetch announcements
            viewModel.activeAnnouncement(token).observe(viewLifecycleOwner, observer.announcement)

            // fetch banners
            viewModel.getBanners(token).observe(viewLifecycleOwner, observer.banner)

            // fetch categories
            viewModel.getCategories(token).observe(viewLifecycleOwner, observer.category)

            // fetch setting
            viewModel.getSetting(token).observe(viewLifecycleOwner, observer.setting(token))

            // fetch products
            viewModel.getproducts(token, 0, "", "ASC")
                .observe(viewLifecycleOwner, observer.products)

            // fetch recommended products
            viewModel.recomendedProducts(token, 0, "", "ASC")
                .observe(viewLifecycleOwner, observer.recommendedProducts)

            // fetch feedback check
            viewModel.getFeedbackCheck(token)
                .observe(viewLifecycleOwner) {
                    when(it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            it.data?.let { data ->
                                if (data.toInt() > 0) {
                                    BottomSheetFeedback(requireActivity(), this).showBottomSheet(data)
                                }
                            }
                        }
                        Status.ERROR -> {}
                    }
                }
        }
    }

    private val observer = object {
        val announcement = Observer<Resource<Announcement>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading)
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading)
                    it.data?.let { data ->
                        if (data.visible != null) {
                            binding.announcement.visibility = View.VISIBLE
                            binding.tvAnnouncement.text = data.subtitle
                            binding.announcement.setOnClickListener {
                                startActivity(
                                    Intent(requireContext(), DetailAnnouncementActivity::class.java)
                                        .putExtra(DetailAnnouncementActivity.EXTRA_DATA, data)
                                )
                            }
                        } else
                            binding.announcement.visibility = View.GONE
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.e("error => ${it.message}")
                }
            }
        }

        val banner = Observer<Resource<List<Banner>>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading)
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading)
                    val slides = mutableListOf<SlideModel>()
                    it.data?.let { data ->
                        val dataBanner = data.filter { b -> b.visible == "1" }
                        dataBanner.map { banner ->
                            slides.add(SlideModel(banner.media?.url))
                        }

                        binding.imageSlider.apply {
                            setImageList(slides, ScaleTypes.CENTER_CROP)
                            setItemClickListener(object : ItemClickListener {
                                override fun onItemSelected(position: Int) {
                                    startActivity(
                                        Intent(
                                            requireContext(),
                                            BannerActivity::class.java
                                        ).putExtra(
                                            BannerActivity.EXTRA_BANNER,
                                            dataBanner[position]
                                        )
                                    )
                                }

                            })
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.e("error => ${it.message}")
                }
            }
        }

        val category = Observer<Resource<List<Category>>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading)
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading)
                    it.data?.let { items ->
                        categoryAdapter.addData(items)
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.e("error => ${it.message}")
                }
            }
        }

        fun setting(token: String) = Observer<Resource<Setting>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading)
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading)
                    it.data?.let { data ->
                        val packageInfo = requireContext().packageManager.getPackageInfo(
                            requireContext().packageName,
                            0
                        )
                        val versionCode = PackageInfoCompat.getLongVersionCode(packageInfo)

                        if (data.versionCode!!.toLong() > versionCode) {
                            UpdateDialogFragment {
                                startActivity(
                                    Intent(Intent.ACTION_VIEW, Uri.parse(Constants.PLAYSTORE_URL))
                                )
                            }.show(parentFragmentManager, "customDialogUpdate")
                        } else if (data.versionCode!!.toLong() < versionCode) {
                            val rawData = JsonObject().apply {
                                addProperty("version_code", versionCode)
                            }

                            viewModel.changeVersionCode(token, rawData)
                                .observe(viewLifecycleOwner, {})
                        }
                    }
                }
                Status.ERROR -> {
                    Timber.e("error => ${it.message}")
                }
            }
        }

        val products = Observer<Resource<List<ProductDomain>>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading)
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading)
                    it.data?.let { items ->
                        val randItems = items.shuffled(Random(System.currentTimeMillis()))
                        productAdapter.addData(randItems.take(6))
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.e("error prod => ${it.message}")
                }
            }
        }

        val recommendedProducts = Observer<Resource<ProductsResponse>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading)
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading)
                    it.data?.let { res ->
                        res.data?.let { items ->
                            recommendedProductAdapter.addData(items)
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.e("error prod => ${it.message}")
                }
            }
        }
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.imageSlider.visibility = View.GONE
            binding.shimmerBanner.visibility = View.VISIBLE
            binding.layoutCateogry.rootLayout.visibility = View.GONE
            binding.shimmerCateogry.rootLayout.visibility = View.VISIBLE
            binding.layoutRecommendedProduct.rootLayout.visibility = View.GONE
            binding.shimmerRecommendedProduct.rootLayout.visibility = View.VISIBLE
            binding.layoutProductHome.rootLayout.visibility = View.GONE
        } else {
            binding.imageSlider.visibility = View.VISIBLE
            binding.shimmerBanner.visibility = View.GONE
            binding.layoutCateogry.rootLayout.visibility = View.VISIBLE
            binding.shimmerCateogry.rootLayout.visibility = View.GONE
            binding.layoutRecommendedProduct.rootLayout.visibility = View.VISIBLE
            binding.shimmerRecommendedProduct.rootLayout.visibility = View.GONE
            binding.layoutProductHome.rootLayout.visibility = View.VISIBLE
        }
    }

    private val setupAdapter = object {
        fun category(recyclerView: RecyclerView) {
            categoryAdapter.adapterCallback(adapterCallback.category)
                .setLayout(R.layout.item_category)
                .isHorizontalView()
                .build(recyclerView)
        }

        fun product(recyclerView: RecyclerView) {
            productAdapter.adapterCallback(adapterCallback.product)
                .setLayout(R.layout.item_product)
                .isStaggedView(2)
                .buildStagged(recyclerView)
        }

        fun recommendedProduct(recyclerView: RecyclerView) {
            recommendedProductAdapter.adapterCallback(adapterCallback.recommendedProduct)
                .setLayout(R.layout.item_recommended_product)
                .isHorizontalView()
                .build(recyclerView)
        }
    }

    private val adapterCallback = object {
        val category = object : AdapterCallback<Category> {
            override fun initComponent(itemView: View, data: Category, itemIndex: Int) {
                // set category
                itemView.tv_category.text = data.category
                Glide.with(requireContext())
                    .load(data.icon?.image?.url)
                    .into(itemView.image_category)
            }

            override fun onItemClicked(itemView: View, data: Category, itemIndex: Int) {
                startActivity(Intent(requireContext(), CategoryActivity::class.java).putExtra(CategoryActivity.EXTRA_INDEX, itemIndex))
            }
        }

        val product = object : AdapterCallback<ProductDomain> {
            override fun initComponent(itemView: View, data: ProductDomain, itemIndex: Int) {
                // set utils
                itemView.tv_nama_produk.text = data.name

                // set gambar
                Glide.with(requireContext())
                    .load(data.image)
                    .into(itemView.image_product)

                // set status makanan
                val setStatus = fun(text: String, color: Int) {
                    itemView.tv_status.text = StringBuilder().append(text)
                    itemView.status.setBackgroundResource(color)
                }

                when (data.visible) {
                    "0" -> setStatus("Habis", R.drawable.status_habis)
                    "1" -> setStatus("Ready", R.drawable.status_ready)
                }

                // set discount
                if (data.discount == 0) {
                    itemView.layout_discount.visibility = View.GONE
                    itemView.tv_harga.text = Helpers.changeToRupiah(data.price!!.toDouble())
                }
                else {
                    itemView.layout_discount.visibility = View.VISIBLE

                    // set new price
                    val oldPrice = data.price!!
                    val discount = oldPrice * data.discount!!.toLong() / 100
                    val newPrice = oldPrice - discount

                    // set view
                    itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                    itemView.tv_discount.text = StringBuilder().append("${data.discount}% OFF")
                    itemView.tv_price_before.text = Helpers.changeToRupiah(oldPrice.toDouble())
                    itemView.tv_harga.text = Helpers.changeToRupiah(newPrice.toDouble())
                }
            }

            override fun onItemClicked(itemView: View, data: ProductDomain, itemIndex: Int) {
                startActivity(
                    Intent(requireContext(), DetailProductActivity::class.java).putExtra(
                        DetailProductActivity.EXTRA_ID,
                        data.productId
                    )
                )
            }
        }

        val recommendedProduct = object : AdapterCallback<Product> {
            override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
                // set utils
                itemView.tv_nama_produk_recommended.text = data.name

                // set gambar
                Glide.with(requireContext())
                    .load(data.thumbnail?.url)
                    .into(itemView.image_product_recommended)

                // set status makanan
                val setStatus = fun(text: String, color: Int) {
                    itemView.tv_status_recommended.text = StringBuilder().append(text)
                    itemView.status_recommended.setBackgroundResource(color)
                }

                when (data.visible) {
                    "0" -> setStatus("Habis", R.drawable.status_habis)
                    "1" -> setStatus("Ready", R.drawable.status_ready)
                }

                // set discount
                if (data.discount == "0") {
                    itemView.tv_harga_recommended.text = Helpers.changeToRupiah(data.price!!.toDouble())
                } else {
                    // set new price
                    val oldPrice = data.price!!
                    val discount = oldPrice * data.discount!!.toLong() / 100
                    val newPrice = oldPrice - discount

                    // set view
                    itemView.tv_price_before_recommended.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                    itemView.tv_discount_recommended.text = StringBuilder().append("${data.discount}% OFF")
                    itemView.tv_price_before_recommended.text = Helpers.changeToRupiah(oldPrice.toDouble())
                    itemView.tv_harga_recommended.text = Helpers.changeToRupiah(newPrice.toDouble())
                }
            }

            override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
                startActivity(
                    Intent(requireContext(), DetailProductActivity::class.java).putExtra(
                        DetailProductActivity.EXTRA_ID,
                        data.id
                    )
                )
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.chat -> {
                startActivity(Intent(requireContext(), ChatActivity::class.java))
                true
            }
            R.id.notification -> {
                startActivity(Intent(requireContext(), NotificationActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}