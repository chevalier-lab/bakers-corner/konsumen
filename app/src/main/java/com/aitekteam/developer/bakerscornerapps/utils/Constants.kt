package com.aitekteam.developer.bakerscornerapps.utils

object Constants {
    const val AUTH_TOKEN = "AUTHORIZATION_TOKEN"
    const val USER_NAME = "USER_NAME"
    const val USER_EMAIL = "USER_EMAIL"
    const val USER_PHONE = "USER_PHONE"
    const val USER_ADDRESS = "USER_ADDRESS"
    const val USER_MEDIA = "USER_MEDIA"
    const val USER_ID = "USER_ID"
    const val CHAT_ID = "CHAT_ID"
    const val CHAT_TOKEN = "CHAT_TOKEN"
    const val TERMS_AND_CONDITION_URL = "http://213.190.4.40/bc/"
    const val BANNER_DETAIL_URL = "http://213.190.4.40/administrator/index.php/webview/banner"
    const val ANNOUNCEMENT_DETAIL_URL = "http://213.190.4.40/administrator/index.php/webview/announcement"
    const val PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=com.aitekteam.developer.bakerscornerapps"
}