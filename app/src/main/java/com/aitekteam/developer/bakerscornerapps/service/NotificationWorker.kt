package com.aitekteam.developer.bakerscornerapps.service

import android.content.Context
import android.content.Intent
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import timber.log.Timber
import java.lang.Exception

class NotificationWorker(
    appContext: Context,
    params: WorkerParameters
) : CoroutineWorker(appContext, params) {

    override suspend fun doWork(): Result {
        return try {
            Timber.i("StatusWorker : worker running")

            // send broadcast
            val broadcastIntent = Intent()
            broadcastIntent.apply {
                action = "StartService"
                setClass(applicationContext, ReceiverService::class.java)
            }
            applicationContext.sendBroadcast(broadcastIntent)

            Result.success()
        } catch (exception: Exception) {
            Timber.i("StatusWorker : restarting worker")
            Result.retry()
        }
    }
}