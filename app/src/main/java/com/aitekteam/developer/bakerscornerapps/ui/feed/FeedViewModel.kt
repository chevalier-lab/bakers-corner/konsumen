package com.aitekteam.developer.bakerscornerapps.ui.feed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import kotlinx.coroutines.flow.Flow

class FeedViewModel(
    private val productService: ProductService
) : ViewModel() {

    fun getProducts(
        token: String,
        search: String,
        direction: String,
        is_price_order: Boolean?,
        is_discount_order: Boolean?
    ): Flow<PagingData<Product>> = Pager(PagingConfig(12)) {
        FeedPagingSource(
            token,
            search,
            direction,
            is_price_order,
            is_discount_order,
            productService
        )
    }.flow.cachedIn(viewModelScope)
}
