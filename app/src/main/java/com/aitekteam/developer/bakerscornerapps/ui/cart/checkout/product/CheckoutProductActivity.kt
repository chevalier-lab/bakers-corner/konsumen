package com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.product

import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.DiffUtilAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityCheckoutPoductBinding
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.CheckoutViewModel
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.success.SuccessCheckoutActivity
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ADDRESS
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_NAME
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_PHONE
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.changeToRupiah
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import com.paginate.Paginate
import kotlinx.android.synthetic.main.activity_checkout_poduct.view.*
import kotlinx.android.synthetic.main.item_product_checkout.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class CheckoutProductActivity : AppCompatActivity(), BottomSheetListener {
    private lateinit var binding: ActivityCheckoutPoductBinding
    private val viewModel: CheckoutViewModel by viewModel()

    // paging
    private var page = 0
    private var totalData = 0
    private var loading = false

    // utils
    private var payment = ""
    private var voucher = ""
    private var discount: Long = 0
    private var totalHarga: Long = 0
    private var biayaLayanan: Long = 0
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: DiffUtilAdapter<CartProduct>
    private lateinit var tempDataList: MutableList<CartProduct>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutPoductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        tempDataList = mutableListOf()
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = DiffUtilAdapter(this)

        // setup adapter
        setupAdapter(binding.rvCheckout)

        // init ui
        initUI()

        // handle paging
        Paginate.with(binding.rvCheckout, pagingCallback)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .build()
    }

    private fun initUI() {
        // select payment method
        binding.selectCash.setOnClickListener {
            binding.radioCash.isChecked = true
            if (binding.radioCash.isChecked) {
                binding.radioCash.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.green)
                binding.radioLinkAja.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.dark)
            }
        }

        binding.selectLinkAja.setOnClickListener {
            binding.radioLinkAja.isChecked = true
            if (binding.radioLinkAja.isChecked) {
                binding.radioLinkAja.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.green)
                binding.radioCash.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.dark)
            }
        }

        // set customer identity
        binding.tvName.text = StringBuilder().append("Nama : ").append(sharedPref.get(USER_NAME))
        binding.tvPhoneNumber.text =
            StringBuilder().append("No HP : ").append(sharedPref.get(USER_PHONE))
        binding.tvAddress.text =
            StringBuilder().append("Alamat : ").append(sharedPref.get(USER_ADDRESS))
        binding.bottomCart.btnSubmitCheckout.text = getString(R.string.proses)
        binding.bottomCart.tvTitle.text = getString(R.string.total_tagihan)

        // fetch data
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getCartProducts(token, 0, "DESC").observe(this, {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        it.data?.let { res ->
                            // set total data
                            totalData = res.meta?.size?.total!!

                            res.data?.let { items ->
                                adapter.setData(items)
                                tempDataList.addAll(items)

                                items.map { cp ->
                                    // check discount
                                    if (cp.product?.discount == "0")
                                        totalHarga += (cp.product?.price!!.times(cp.qty!!.toLong()))
                                    else {
                                        // set new price
                                        val oldPrice = cp.product!!.price!!
                                        val discount = oldPrice * cp.product!!.discount!!.toLong() / 100
                                        val newPrice = oldPrice - discount

                                        // set total price
                                        totalHarga += newPrice.times(cp.qty!!.toLong())
                                    }
                                }
                                paymentSummary()
                            }
                        }
                    }
                    Status.ERROR -> {
                    }
                }
            })
        }

        // process
        binding.rgPayment.setOnCheckedChangeListener { _, checkedId ->
            payment = when (checkedId) {
                R.id.radio_cash -> "cash"
                R.id.radio_link_aja -> "linkAja"
                else -> ""
            }
        }

        binding.cardCheckVoucher.setOnClickListener {
            BottomSheetVoucher(this).showBottomSheet()
        }

        binding.btnCancelVoucher.setOnClickListener {
            binding.cardCheckVoucher.visibility = View.VISIBLE
            binding.cardVoucherActived.visibility = View.GONE
            binding.tvVoucherSuccess.visibility = View.GONE

            discount = 0
            voucher = ""
            paymentSummary()
        }

        binding.bottomCart.btnSubmitCheckout.setOnClickListener {
            when (payment) {
                "cash" -> {
                    CustomDialogFragment(
                        getString(R.string.perhatian),
                        getString(R.string.checkout_disclaimer)
                    ) { doCheckoutCash() }.show(supportFragmentManager, "cutomDialog")
                }
                "linkAja" -> {
                    Toast.makeText(this, "linkAja belum bisa ya:)", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(this, "Harap pilih metode pembayaran!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private val pagingCallback = object : Paginate.Callbacks {
        override fun onLoadMore() {
            page++
            loading = true
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                viewModel.getCartProducts(token, page, "DESC").observe(this@CheckoutProductActivity) {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            it.data?.let { res ->
                                res.data?.let { items ->
                                    if (items.isNotEmpty()) {
                                        tempDataList.addAll(items)
                                        adapter.setData(tempDataList)

                                        var totalHargaTambah: Long = 0
                                        items.map { cp ->
                                            // check discount
                                            if (cp.product?.discount == "0")
                                                totalHargaTambah += (cp.product?.price!!.times(cp.qty!!.toLong()))
                                            else {
                                                // set new price
                                                val oldPrice = cp.product!!.price!!
                                                val discount = oldPrice * cp.product!!.discount!!.toLong() / 100
                                                val newPrice = oldPrice - discount

                                                // set total price
                                                totalHargaTambah += newPrice.times(cp.qty!!.toLong())
                                            }
                                        }

                                        totalHarga += totalHargaTambah
                                        paymentSummary()
                                    }
                                }

                                loading = false
                            }
                        }
                        Status.ERROR -> {
                        }
                    }
                }
            }
        }

        override fun isLoading(): Boolean {
            return loading
        }

        override fun hasLoadedAllItems(): Boolean {
            Timber.i("paging total => ${adapter.itemCount} $totalData")
            return adapter.itemCount == totalData
        }

    }

    private fun doCheckoutCash() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.checkoutProduct(token, voucher).observe(this, {
                when (it.status) {
                    Status.LOADING -> {
                        binding.bottomCart.btnSubmitCheckout.isEnabled = false
                        binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                            ContextCompat.getColor(
                                this,
                                R.color.divider
                            )
                        )
                    }
                    Status.SUCCESS -> {
                        it.data.let { data ->
                            finish()
                            startActivity(
                                Intent(this, SuccessCheckoutActivity::class.java)
                                    .putExtra(SuccessCheckoutActivity.EXTRA_PRODUCT, data)
                                    .putExtra(SuccessCheckoutActivity.EXTRA_TYPE, "product")
                            )
                            Toast.makeText(this, "Berhasil checkout pesanan!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    Status.ERROR -> {
                        startActivity(Intent(this, HostActivity::class.java).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            putExtra(HostActivity.EXTRA_MOVE_CART, true)
                        })
                        Toast.makeText(this, "Gagal checkout pesanan!", Toast.LENGTH_SHORT).show()
                    }
                }
            })
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_product_checkout)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<CartProduct> {
        override fun initComponent(itemView: View, data: CartProduct, itemIndex: Int) {
            // set utils
            val price = data.product?.price?.times(data.qty!!.toLong())
            itemView.tv_product_name.text = data.product?.name
            itemView.tv_qty.text = StringBuilder().append(data.qty).append(" produk")

            Glide.with(this@CheckoutProductActivity)
                .load(data.product?.thumbnail?.url)
                .into(itemView.image_product)

            // set discount
            if (data.product?.discount == "0")
                itemView.tv_price.text = changeToRupiah(price!!.toDouble())
            else {
                itemView.tv_price_before.visibility = View.VISIBLE

                // set new price
                val oldPrice = data.product!!.price!!
                val discount = oldPrice * data.product!!.discount!!.toLong() / 100
                val newPrice = oldPrice - discount

                // set displayed price
                val priceAfter = newPrice.times(data.qty!!.toLong())
                val priceBefore = oldPrice.times(data.qty!!.toLong())

                // set view
                itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tv_price_before.text = changeToRupiah(priceBefore.toDouble())
                itemView.tv_price.text = changeToRupiah(priceAfter.toDouble())
            }
        }

        override fun onItemClicked(itemView: View, data: CartProduct, itemIndex: Int) {}

    }

    private fun paymentSummary() {
        // set payment summary
        binding.tvTotalPrice.text = changeToRupiah(totalHarga.toDouble())
        binding.tvBiayaLayanan.text = changeToRupiah(biayaLayanan.toDouble())
        binding.tvDiscount.tv_discount.text =
            StringBuilder().append("- ").append(
                changeToRupiah(discount.toDouble())
            )

        // total tagihan
        val totalTagihan = totalHarga + biayaLayanan - discount
        binding.bottomCart.tvTotalPrice.text =
            changeToRupiah(totalTagihan.toDouble())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                startActivity(Intent(this, HostActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    putExtra(HostActivity.EXTRA_MOVE_CART, true)
                })
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun checkVoucher(code: String) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.checkVoucher(token, code).observe(this, {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        voucher = code
                        binding.cardCheckVoucher.visibility = View.GONE
                        binding.cardVoucherActived.visibility = View.VISIBLE
                        binding.tvVoucherSuccess.visibility = View.VISIBLE

                        it.data?.let { data ->
                            discount = data.discountPrice!!
                            binding.tvTotalDiscount.text =
                                StringBuilder().append("Kamu bisa hemat ")
                                    .append(changeToRupiah(data.discountPrice!!.toDouble()))
                            paymentSummary()
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, "Kode voucher tidak valid!", Toast.LENGTH_SHORT).show()
                    }
                }
            })
        }
    }

}