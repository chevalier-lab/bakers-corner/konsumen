package com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.request

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityCheckoutRequestBinding
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.CheckoutViewModel
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.success.SuccessCheckoutActivity
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ADDRESS
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_NAME
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_PHONE
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import kotlinx.android.synthetic.main.item_request_checkout.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class CheckoutRequestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCheckoutRequestBinding
    private val viewModel: CheckoutViewModel by viewModel()

    // utils
    private var payment = ""
    private var totalHarga: Long = 0
    private var biayaLayanan: Long = 0
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusableAdapter<CartRequest>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutRequestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvCheckout)

        // init UI
        initUI()
    }

    private fun initUI() {
        // select payment method
        binding.selectCash.setOnClickListener {
            binding.radioCash.isChecked = true
            if (binding.radioCash.isChecked) {
                binding.radioCash.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.green)
                binding.radioLinkAja.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.dark)
            }
        }

        binding.selectLinkAja.setOnClickListener {
            binding.radioLinkAja.isChecked = true
            if (binding.radioLinkAja.isChecked) {
                binding.radioLinkAja.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.green)
                binding.radioCash.buttonTintList =
                    ContextCompat.getColorStateList(this, R.color.dark)
            }
        }

        // set customer identity
        binding.tvName.text = StringBuilder().append("Nama : ").append(sharedPref.get(USER_NAME))
        binding.tvPhoneNumber.text =
            StringBuilder().append("No HP : ").append(sharedPref.get(USER_PHONE))
        binding.tvAddress.text =
            StringBuilder().append("Alamat : ").append(sharedPref.get(USER_ADDRESS))
        binding.bottomCart.btnSubmitCheckout.text = getString(R.string.proses)
        binding.bottomCart.tvTitle.text = getString(R.string.total_tagihan)

        // fetch data
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getCartRequestMenu(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {}
                    Status.SUCCESS -> {
                        it.data?.let { items ->
                             adapter.addData(items)

                            items.map { cr ->
                                totalHarga += (cr.product?.price!!.times(cr.qty!!.toLong()))
                            }
                            paymentSummary()
                        }
                    }
                    Status.ERROR -> {}
                }
            }
        }

        // process
        binding.rgPayment.setOnCheckedChangeListener { _, checkedId ->
            payment = when (checkedId) {
                R.id.radio_cash -> "cash"
                R.id.radio_link_aja -> "linkAja"
                else -> ""
            }
        }

        binding.bottomCart.btnSubmitCheckout.setOnClickListener {
            when (payment) {
                "cash" -> {
                    CustomDialogFragment(
                        getString(R.string.perhatian),
                        getString(R.string.checkout_disclaimer)
                    ) { doCheckoutCash() }.show(supportFragmentManager, "cutomDialog")
                }
                "linkAja" -> {
                    Toast.makeText(this, "linkAja belum bisa ya:)", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(this, "Harap pilih metode pembayaran!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun paymentSummary() {
        // set payment summary
        binding.tvTotalPrice.text = Helpers.changeToRupiah(totalHarga.toDouble())
        binding.tvBiayaLayanan.text = Helpers.changeToRupiah(biayaLayanan.toDouble())

        // total tagihan
        val totalTagihan = totalHarga + biayaLayanan
        binding.bottomCart.tvTotalPrice.text =
            Helpers.changeToRupiah(totalTagihan.toDouble())
    }

    private fun doCheckoutCash() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.checkoutRequestMenu(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.bottomCart.btnSubmitCheckout.isEnabled = false
                        binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                            ContextCompat.getColor(
                                this,
                                R.color.divider
                            )
                        )
                    }
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            finish()
                            startActivity(
                                Intent(this, SuccessCheckoutActivity::class.java)
                                    .putExtra(SuccessCheckoutActivity.EXTRA_REQUEST, data)
                                    .putExtra(SuccessCheckoutActivity.EXTRA_TYPE, "request")
                            )
                            Toast.makeText(this, "Berhasil checkout pesanan!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    Status.ERROR -> {
                        startActivity(Intent(this, HostActivity::class.java).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            putExtra(HostActivity.EXTRA_MOVE_CART, true)
                        })
                        Toast.makeText(this, "Gagal checkout pesanan!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_request_checkout)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<CartRequest> {
        override fun initComponent(itemView: View, data: CartRequest, itemIndex: Int) {
            val price = data.product?.price?.times(data.qty!!.toLong())
            itemView.tv_product_name.text = data.product?.name
            itemView.tv_qty.text = StringBuilder().append(data.qty).append(" produk")
            itemView.tv_price.text = Helpers.changeToRupiah(price!!.toDouble())
        }

        override fun onItemClicked(itemView: View, data: CartRequest, itemIndex: Int) { }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                startActivity(Intent(this, HostActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    putExtra(HostActivity.EXTRA_MOVE_CART, true)
                })
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}