package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.product

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_give_feedback.*
import kotlinx.android.synthetic.main.bottom_sheet_give_feedback.view.*
import java.util.*

class BottomSheetGiveFeedback(
    activity: Activity,
    private val trxId: String,
    private val listener: BottomSheetListener

) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    private var rating = 0
    private var comment1 = ""
    private var comment2 = ""
    private var comment3 = ""
    private var comment4 = ""
    private var comment5 = ""
    private var commentValid = false

    // option
    private var option1 = false
    private var option2 = false
    private var option3 = false
    private var option4 = false
    private var option5 = false

    fun showBottomSheet() {
        val view = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_give_feedback, feedback_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        // rating
        setRating(view)

        // comment helper
        setComment(view)

        view.et_comment.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                commentValid = p0.toString().trim().isNotEmpty()
                inputCheck(view)
            }
        })

        // submit
        view.btn_feedback.setOnClickListener {
            if (rating == 0)
                Toast.makeText(context, "Kasih rating dulu yaa", Toast.LENGTH_SHORT).show()
            else {
                val review = view.et_comment.text.toString().trim()
                listener.giveFeedback(trxId, rating, review, this)
            }
        }
    }

    private fun setRating(view: View) {
        view.star_1.setOnClickListener {
            rating = 1

            // set text
            view.tv_rating.text = context.getString(R.string.kurang_puas)

            // set star
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_1)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_2)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_3)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_4)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_5)
        }

        view.star_2.setOnClickListener {
            rating = 2

            // set text
            view.tv_rating.text = context.getString(R.string.cukup)

            // set star
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_1)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_2)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_3)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_4)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_5)
        }

        view.star_3.setOnClickListener {
            rating = 3

            // set text
            view.tv_rating.text = context.getString(R.string.lumayan)

            // set star
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_1)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_2)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_3)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_4)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_5)
        }

        view.star_4.setOnClickListener {
            rating = 4

            // set text
            view.tv_rating.text = context.getString(R.string.puas)

            // set star
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_1)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_2)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_3)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_4)
            Glide.with(context)
                .load(R.drawable.ic_star_off)
                .into(view.star_5)
        }

        view.star_5.setOnClickListener {
            rating = 5

            // set text
            view.tv_rating.text = context.getString(R.string.puas_banget)

            // set star
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_1)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_2)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_3)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_4)
            Glide.with(context)
                .load(R.drawable.ic_star_on)
                .into(view.star_5)
        }
    }

    private fun setComment(view: View) {
        view.option_1.setOnClickListener {
            // set option
            option1 = !option1
            if (option1) {
                view.option_1.strokeWidth = 0
                view.tv_option_1.setTextColor(ContextCompat.getColor(context, R.color.white))
                view.option_1.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green))
                comment1 = context.getString(R.string.harganya_pas)
            } else {
                view.option_1.strokeWidth = 2
                view.option_1.strokeColor = ContextCompat.getColor(context, R.color.grey)
                view.tv_option_1.setTextColor(ContextCompat.getColor(context, R.color.grey))
                view.option_1.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                comment1 = ""
            }

            // set text
            val review = StringBuilder().apply {
                append(comment1)
                append(comment2.toLowerCase(Locale.ROOT))
                append(comment3.toLowerCase(Locale.ROOT))
                append(comment4.toLowerCase(Locale.ROOT))
                append(comment5.toLowerCase(Locale.ROOT))
            }
            view.et_comment.setText(review.trim())

            // set enable button
            commentValid = review.trim().isNotEmpty()
            inputCheck(view)
        }

        view.option_2.setOnClickListener {
            // set option
            option2 = !option2
            if (option2) {
                view.option_2.strokeWidth = 0
                view.tv_option_2.setTextColor(ContextCompat.getColor(context, R.color.white))
                view.option_2.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green))
                comment2 = " ${context.getString(R.string.makanannya_enak)}"
            } else {
                view.option_2.strokeWidth = 2
                view.option_2.strokeColor = ContextCompat.getColor(context, R.color.grey)
                view.tv_option_2.setTextColor(ContextCompat.getColor(context, R.color.grey))
                view.option_2.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                comment2 = ""
            }

            // set text
            val review = StringBuilder().apply {
                append(comment1)
                append(comment2.toLowerCase(Locale.ROOT))
                append(comment3.toLowerCase(Locale.ROOT))
                append(comment4.toLowerCase(Locale.ROOT))
                append(comment5.toLowerCase(Locale.ROOT))
            }
            view.et_comment.setText(review.trim())

            // set enable button
            commentValid = review.trim().isNotEmpty()
            inputCheck(view)
        }

        view.option_3.setOnClickListener {
            // set option
            option3 = !option3
            if (option3) {
                view.option_3.strokeWidth = 0
                view.tv_option_3.setTextColor(ContextCompat.getColor(context, R.color.white))
                view.option_3.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green))
                comment3 = " ${context.getString(R.string.produknya_sesuai)}"
            } else {
                view.option_3.strokeWidth = 2
                view.option_3.strokeColor = ContextCompat.getColor(context, R.color.grey)
                view.tv_option_3.setTextColor(ContextCompat.getColor(context, R.color.grey))
                view.option_3.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                comment3 = ""
            }

            // set text
            val review = StringBuilder().apply {
                append(comment1)
                append(comment2.toLowerCase(Locale.ROOT))
                append(comment3.toLowerCase(Locale.ROOT))
                append(comment4.toLowerCase(Locale.ROOT))
                append(comment5.toLowerCase(Locale.ROOT))
            }
            view.et_comment.setText(review.trim())

            // set enable button
            commentValid = review.trim().isNotEmpty()
            inputCheck(view)
        }

        view.option_4.setOnClickListener {
            // set option
            option4 = !option4
            if (option4) {
                view.option_4.strokeWidth = 0
                view.tv_option_4.setTextColor(ContextCompat.getColor(context, R.color.white))
                view.option_4.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green))
                comment4 = " ${context.getString(R.string.pelayanannya_mantap)}"
            } else {
                view.option_4.strokeWidth = 2
                view.option_4.strokeColor = ContextCompat.getColor(context, R.color.grey)
                view.tv_option_4.setTextColor(ContextCompat.getColor(context, R.color.grey))
                view.option_4.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                comment4 = ""
            }

            // set text
            val review = StringBuilder().apply {
                append(comment1)
                append(comment2.toLowerCase(Locale.ROOT))
                append(comment3.toLowerCase(Locale.ROOT))
                append(comment4.toLowerCase(Locale.ROOT))
                append(comment5.toLowerCase(Locale.ROOT))
            }
            view.et_comment.setText(review.trim())

            // set enable button
            commentValid = review.trim().isNotEmpty()
            inputCheck(view)
        }

        view.option_5.setOnClickListener {
            // set option
            option5 = !option5
            if (option5) {
                view.option_5.strokeWidth = 0
                view.tv_option_5.setTextColor(ContextCompat.getColor(context, R.color.white))
                view.option_5.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green))
                comment5 = " ${context.getString(R.string.sangat_direkomendasikan)}"
            } else {
                view.option_5.strokeWidth = 2
                view.option_5.strokeColor = ContextCompat.getColor(context, R.color.grey)
                view.tv_option_5.setTextColor(ContextCompat.getColor(context, R.color.grey))
                view.option_5.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                comment5 = ""
            }

            // set text
            val review = StringBuilder().apply {
                append(comment1)
                append(comment2.toLowerCase(Locale.ROOT))
                append(comment3.toLowerCase(Locale.ROOT))
                append(comment4.toLowerCase(Locale.ROOT))
                append(comment5.toLowerCase(Locale.ROOT))
            }
            view.et_comment.setText(review.trim())

            // set enable button
            commentValid = review.trim().isNotEmpty()
            inputCheck(view)
        }
    }

    private fun inputCheck(view: View) {
        if (commentValid) {
            view.btn_feedback.isEnabled = true
            view.btn_feedback.setBackgroundColor(ContextCompat.getColor(context, R.color.green))
        } else {
            view.btn_feedback.isEnabled = false
            view.btn_feedback.setBackgroundColor(ContextCompat.getColor(context, R.color.divider))
        }
    }
}