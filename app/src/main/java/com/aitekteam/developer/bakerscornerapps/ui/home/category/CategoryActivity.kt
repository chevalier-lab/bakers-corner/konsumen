package com.aitekteam.developer.bakerscornerapps.ui.home.category

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityCategoryBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.adapter.TabAdapter
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import org.koin.android.viewmodel.ext.android.viewModel

class CategoryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCategoryBinding
    private val viewModel: CategoryViewModel by viewModel()

    // Utils
    private lateinit var sharedPrefs: SharedPrefsUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        sharedPrefs = SharedPrefsUtil()
        sharedPrefs.start(this, AUTH_TOKEN)

        val extra = intent.extras
        if (extra != null) {
            val index = extra.getInt(EXTRA_INDEX)
            initUI(index)
        }

    }

    private fun initUI(index: Int) {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getCategories(token, 0, "", "ASC").observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.loading.visibility = View.VISIBLE
                        binding.layoutError.visibility = View.GONE
                        binding.layoutMain.visibility = View.GONE
                    }
                    Status.SUCCESS -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.layoutMain.visibility = View.VISIBLE

                        val adapter = TabAdapter(supportFragmentManager)
                        binding.vpCategory.adapter = adapter
                        binding.tabLayoutCategory.setupWithViewPager(binding.vpCategory)

                        if (it.data?.isNotEmpty()!!) {
                            it.data?.mapIndexed { _, data ->
                                adapter.add(
                                    CategoryListFragment.newInstance(data.id!!),
                                    data.category.toString()
                                )
                                binding.tabLayoutCategory.getTabAt(index)?.select()
                            }
                        } else {
                            binding.layoutEmpty.visibility = View.VISIBLE
                            binding.layoutError.visibility = View.GONE
                            binding.vpCategory.visibility = View.GONE
                        }
                    }
                    Status.ERROR -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_INDEX = "extra_index"
    }
}