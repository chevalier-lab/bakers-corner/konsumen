package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.product

import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductItem
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityDetailTransactionProductBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.TransactionActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.changeToRupiah
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.convertToDateTime
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.aitekteam.developer.bakerscornerapps.utils.helper.SnackbarUtil
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.item_product_transaction_detail.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class DetailTransactionProductActivity : AppCompatActivity(), BottomSheetListener {
    private lateinit var binding: ActivityDetailTransactionProductBinding
    private val viewModel: DetailTransactionProductViewModel by viewModel()

    // utils
    private var condition: String? = null
    private var totalPriceProduct: Long = 0
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusableAdapter<ProductItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTransactionProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvProduct)

        // get extras
        val extra = intent.extras
        if (extra != null) {
            val trxId = extra.getString(EXTRA_ID)
            if (trxId != null) initUI(trxId)
            condition = extra.getString(EXTRA_CONDITION)
        }
    }

    private fun initUI(trxId: String) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getDetailTransaction(token, trxId).observe(this, {
                when (it.status) {
                    Status.LOADING -> {
                        binding.loading.visibility = View.VISIBLE
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                    }
                    Status.SUCCESS -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.layoutMain.visibility = View.VISIBLE

                        it.data?.let { data ->
                            // set status
                            when (data.status) {
                                0 -> {
                                    binding.btnCancel.visibility = View.VISIBLE
                                    binding.tvStatus.text = getString(R.string.menunggu_konfirmasi)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.grey_dark
                                        )
                                    )
                                }
                                2 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.diproses)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.priceColor
                                        )
                                    )
                                }
                                3 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.siap_untuk_ambil)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.colorPrimary
                                        )
                                    )
                                }
                                4 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.selesai)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.green
                                        )
                                    )

                                    if (data.isAlreadyFeedback == 0)
                                        binding.btnFeedback.visibility = View.VISIBLE
                                    else if (data.isAlreadyFeedback == 1)
                                        binding.btnFeedback.visibility = View.GONE
                                }
                                5 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.dibatalkan)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.red
                                        )
                                    )
                                }
                            }

                            // set total price
                            data.items?.map { product ->
                                totalPriceProduct += product.subTotal!!
                            }

                            // set utils
                            adapter.addData(data.items!!)
                            binding.tvOrderCode.text = data.code
                            binding.tvDate.text = convertToDateTime(data.date!!)
                            binding.tvTotalPrice.text = changeToRupiah(totalPriceProduct.toDouble())
                            binding.tvServiceFee.text = changeToRupiah(0.0)
                            binding.tvDiscount.text = StringBuilder().append("- ")
                                .append(changeToRupiah(data.discount?.toDouble()!!))
                            binding.tvTotalPay.text = changeToRupiah(data.total?.toDouble()!!)
                            binding.tvPaymentMethode.text = data.method
                        }
                    }
                    Status.ERROR -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                    }
                }
            })

            // cancel order
            binding.btnCancel.setOnClickListener { cancelTransaction(token, trxId) }

            // give feedback
            binding.btnFeedback.setOnClickListener {
                BottomSheetGiveFeedback(this, trxId, this).showBottomSheet()
            }
        }
    }

    private fun cancelTransaction(token: String, trxId: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.cancel_trx_disclaimer)
        ) {
            viewModel.cancelTransaction(token, trxId).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        Toast.makeText(this, "Berhasil membatalkan pesanan!", Toast.LENGTH_SHORT)
                            .show()
                        startActivity(Intent(this, TransactionActivity::class.java))
                        finish()
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }.show(supportFragmentManager, "cutomDialog")
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_product_transaction_detail)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<ProductItem> {
        override fun initComponent(itemView: View, data: ProductItem, itemIndex: Int) {
            itemView.tv_product_name.text = data.product?.name
            itemView.tv_qty.text = StringBuilder().append(data.qty).append(" produk")
            itemView.tv_total_price.text = changeToRupiah(data.subTotal?.toDouble()!!)

            Glide.with(this@DetailTransactionProductActivity)
                .load(data.product?.thumbnail?.url)
                .into(itemView.image_product)

            // set discount
            if (data.product?.discount == "0") {
                itemView.tv_price_before.visibility = View.GONE
                itemView.tv_price.text = changeToRupiah(data.product?.price?.toDouble()!!)
            }
            else {
                itemView.tv_price_before.visibility = View.VISIBLE

                // set new price
                val oldPrice = data.product!!.price!!
                val discount = oldPrice * data.product!!.discount!!.toLong() / 100
                val newPrice = oldPrice - discount

                // set view
                itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tv_price_before.text = changeToRupiah(oldPrice.toDouble())
                itemView.tv_price.text = changeToRupiah(newPrice.toDouble())
            }
        }

        override fun onItemClicked(itemView: View, data: ProductItem, itemIndex: Int) {
            startActivity(
                Intent(
                    this@DetailTransactionProductActivity,
                    DetailProductActivity::class.java
                ).putExtra(DetailProductActivity.EXTRA_ID, data.product?.id)
            )
        }
    }

    override fun giveFeedback(trxId: String, rating: Int, comment: String, bottomSheet: BottomSheetDialog) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.feedback_disclaimer)
        ) {
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                bottomSheet.dismiss()
                val rawData = JsonObject().apply {
                    addProperty("rating", rating)
                    addProperty("review", comment)
                }

                viewModel.addReview(token, trxId, rawData).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            binding.btnFeedback.visibility = View.GONE
                            SnackbarUtil.show(
                                window.decorView.rootView,
                                "Berhasil menambahkan feedback",
                                ContextCompat.getColor(this, R.color.colorPrimary),
                                ContextCompat.getColor(this, R.color.dark),
                                ContextCompat.getColor(this, R.color.white)
                            )
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Gagal menambahkan feedback :(", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "cutomDialog")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (condition != null)
                    when (condition) {
                        "back" -> {
                            startActivity(Intent(this, TransactionActivity::class.java))
                            finish()
                        }
                        "current" -> startActivity(Intent(this, HostActivity::class.java).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            putExtra(HostActivity.EXTRA_MOVE_CART, true)
                        })
                    }

                else finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (condition != null)
            when (condition) {
                "back" -> {
                    startActivity(Intent(this, TransactionActivity::class.java))
                    finish()
                }
                "current" -> startActivity(Intent(this, HostActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    putExtra(HostActivity.EXTRA_MOVE_CART, true)
                })
            }
        else finish()
    }

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_CONDITION = "extra_condition"
    }
}