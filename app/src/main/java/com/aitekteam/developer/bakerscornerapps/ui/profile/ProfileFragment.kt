package com.aitekteam.developer.bakerscornerapps.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentProfileBinding
import com.aitekteam.developer.bakerscornerapps.service.NotificationService
import com.aitekteam.developer.bakerscornerapps.ui.auth.login.AuthActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.list.AnnouncementActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.list.FeedbackActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.manage.KelolaAkunActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.request.RequestMenuActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.terms.TermsAndConditionActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.TransactionActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist.WishlistActivity
import com.aitekteam.developer.bakerscornerapps.ui.service.chat.ChatActivity
import com.aitekteam.developer.bakerscornerapps.ui.service.notification.NotificationActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ADDRESS
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_EMAIL
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ID
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_MEDIA
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_NAME
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_PHONE
import com.aitekteam.developer.bakerscornerapps.utils.helper.CleanDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_host.*
import org.koin.android.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ProfileViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtil
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        sharedPrefs = SharedPrefsUtil()
        sharedPrefs.start(requireActivity(), AUTH_TOKEN)

        activity?.et_search?.setText("")
        activity?.search_bar?.visibility = View.GONE
        activity?.layout_filter?.visibility = View.GONE
        activity?.toolbar?.visibility = View.VISIBLE

        setHasOptionsMenu(true)
        activity?.toolbar_title?.text = getString(R.string.profile)

        // init UI
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun getProfile () {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(viewLifecycleOwner, {
                when (it.status) {
                    Status.LOADING -> {}
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            sharedPrefs.setString(AUTH_TOKEN, data.token!!)
                            sharedPrefs.setString(USER_ID, data.id!!)
                            sharedPrefs.setString(USER_NAME, data.name!!)
                            sharedPrefs.setString(USER_EMAIL, data.email!!)
                            sharedPrefs.setString(USER_PHONE, data.phone_number!!)
                            sharedPrefs.setString(USER_ADDRESS, data.address!!)
                            sharedPrefs.setString(USER_MEDIA, data.media?.url!!)

                            Glide.with(requireActivity())
                                .load(data.media?.url!!)
                                .into(binding.ivProfileImage)
                        }
                    }
                    Status.ERROR -> {}
                }
            })
        }
    }

    private fun initUI() {
        // getting data
        getProfile()

        // set profile
        binding.tvName.text = sharedPrefs.get(USER_NAME)
        binding.tvEmail.text = sharedPrefs.get(USER_EMAIL)

        Glide.with(requireActivity())
            .load(sharedPrefs.get(USER_MEDIA))
            .into(binding.ivProfileImage)

        binding.btnRequestMenu.setOnClickListener {
            startActivity(Intent(requireContext(), RequestMenuActivity::class.java))
        }
        binding.btnKelolaAkun.setOnClickListener {
            startActivity(Intent(requireContext(), KelolaAkunActivity::class.java))
        }
        binding.btnAnnouncement.setOnClickListener {
            startActivity(Intent(requireContext(), AnnouncementActivity::class.java))
        }

        binding.btnFeedback.setOnClickListener {
            startActivity(Intent(requireContext(), FeedbackActivity::class.java))
        }
        binding.btnTransaction.setOnClickListener {
            startActivity(Intent(requireContext(), TransactionActivity::class.java))
        }
        binding.btnWishlist.setOnClickListener {
            startActivity(Intent(requireContext(), WishlistActivity::class.java))
        }

        binding.btnTerms.setOnClickListener {
            startActivity(Intent(requireContext(), TermsAndConditionActivity::class.java))
        }

        binding.btnAbout.setOnClickListener {
            CleanDialogFragment(
                getString(R.string.tentang_aplikasi)
            ).show(parentFragmentManager, "cutomDialog")
        }
        binding.btnSignOut.setOnClickListener { signOut() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.chat -> {
                startActivity(Intent(requireContext(), ChatActivity::class.java))
                true
            }
            R.id.notification -> {
                startActivity(Intent(requireContext(), NotificationActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun signOut() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.logout_disclaimer)
        ) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

            mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
            FirebaseAuth.getInstance().signOut()
            mGoogleSignInClient.signOut()

            // clear sharedprefs
            sharedPrefs.clear()

            // stop service
            val intentService = Intent(requireContext(), NotificationService::class.java)
            requireContext().stopService(intentService)

            val intent = Intent(requireContext(), AuthActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            }
            startActivity(intent)
        }.show(parentFragmentManager, "cutomDialog")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}