package com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.detail

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityDetailAnnouncementBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants.ANNOUNCEMENT_DETAIL_URL

class DetailAnnouncementActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailAnnouncementBinding

    // utils
    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailAnnouncementBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // get parcelable data
        val extra = intent.extras
        if (extra != null) {
            val data = extra.getParcelable<Announcement>(EXTRA_DATA)
            data?.let { initUI(it) }
        }
    }

    private fun initUI(data: Announcement) {
        webView = binding.wvAnnouncement
        webView.apply {
            settings.loadsImagesAutomatically = true
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true

            // zoom support
            settings.supportZoom()
            settings.builtInZoomControls = true
            settings.displayZoomControls = false

            // scroll support
            scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            webViewClient = WebViewClient()
            loadUrl("${ANNOUNCEMENT_DETAIL_URL}/${data.id}")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}