package com.aitekteam.developer.bakerscornerapps.ui.feed

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import okio.IOException
import retrofit2.HttpException

class FeedPagingSource(
    private val token: String,
    private val search: String,
    private val direction: String,
    private val is_price_order: Boolean?,
    private val is_discount_order: Boolean?,
    private val productService: ProductService
) : PagingSource<Int, Product>() {

    override fun getRefreshKey(state: PagingState<Int, Product>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = productService.filterProductsPaging(
            token,
            currentPage,
            search,
            direction,
            is_price_order,
            is_discount_order
        )
        val responseList = mutableListOf<Product>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}