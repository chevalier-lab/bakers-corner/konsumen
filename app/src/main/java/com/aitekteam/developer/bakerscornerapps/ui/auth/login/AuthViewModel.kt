package com.aitekteam.developer.bakerscornerapps.ui.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.Auth
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.AuthUseCase
import okhttp3.RequestBody

class AuthViewModel(
    private val authUseCase: AuthUseCase,
) : ViewModel() {

    fun login(email: RequestBody, password: RequestBody): LiveData<Resource<Auth>> =
        authUseCase.login(email, password).asLiveData()
}