package com.aitekteam.developer.bakerscornerapps.ui.cart.request

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentCartRequestMenuBinding
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.request.CheckoutRequestActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_cart_request.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class CartRequestMenuFragment : Fragment() {
    private var _binding: FragmentCartRequestMenuBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CartRequestViewModel by viewModel()

    // utils
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusableAdapter<CartRequest>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartRequestMenuBinding.inflate(inflater, container, false)

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(requireActivity(), AUTH_TOKEN)
        adapter = ReusableAdapter(requireContext())

        // setup adapter
        setupAdapter(binding.rvCart)

        // init ui
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getCartRequest(token).observe(viewLifecycleOwner, {
                when (it.status) {
                    Status.LOADING -> {
                        binding.rvCart.visibility = View.GONE
                        binding.bottomCart.layoutBottom.visibility = View.GONE
                        binding.shimmerCart.rootLayout.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.rvCart.visibility = View.VISIBLE
                        binding.bottomCart.layoutBottom.visibility = View.VISIBLE
                        binding.shimmerCart.rootLayout.visibility = View.GONE

                        it.data?.let { items ->
                            if (items.isEmpty()) {
                                binding.rvCart.visibility = View.GONE
                                binding.layoutEmpty.visibility = View.VISIBLE
                            } else
                                adapter.addData(items)

                            var totalPrice: Long = 0
                            items.map { cr ->
                                totalPrice += (cr.product?.price!!.times(cr.qty!!.toLong()))
                            }
                            binding.bottomCart.tvTotalPrice.text =
                                Helpers.changeToRupiah(totalPrice.toDouble())

                            if (items.isEmpty()) {
                                binding.bottomCart.btnSubmitCheckout.isEnabled = false
                                binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                    ContextCompat.getColor(
                                        requireContext(),
                                        R.color.divider
                                    )
                                )
                            } else {
                                binding.bottomCart.btnSubmitCheckout.isEnabled = true
                                binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                    ContextCompat.getColor(
                                        requireContext(),
                                        R.color.green
                                    )
                                )
                            }
                        }
                    }
                    Status.ERROR -> {
                        binding.rvCart.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        binding.bottomCart.layoutBottom.visibility = View.GONE
                        binding.shimmerCart.rootLayout.visibility = View.GONE
                    }
                }
            })

            binding.bottomCart.btnSubmitCheckout.setOnClickListener {
                startActivity(Intent(requireContext(), CheckoutRequestActivity::class.java))
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_cart_request)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<CartRequest> {
        override fun initComponent(itemView: View, data: CartRequest, itemIndex: Int) {
            val price = data.product?.price?.times(data.product?.qty!!.toLong())
            itemView.tv_product_name.text = data.product?.name
            itemView.tv_price.text = Helpers.changeToRupiah(price!!.toDouble())
            itemView.tv_qty.text = data.product?.qty

            // set button action
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                itemView.btn_delete.setOnClickListener {
                    processDelete(token, data.id!!)
                }

                itemView.btn_min.setOnClickListener {
                    if (data.qty!!.toInt() > 1) {
                        val currentQty = data.qty!!.toInt() - 1
                        val rawData = JsonObject().apply {
                            addProperty("product_qty", currentQty)
                        }
                        viewModel.updateCartRequest(token, data.id!!, rawData)
                            .observe(viewLifecycleOwner, observer.cart)
                    } else processDelete(token, data.id!!)
                }

                itemView.btn_plus.setOnClickListener {
                    val currentQty = data.qty!!.toInt() + 1
                    val rawData = JsonObject().apply {
                        addProperty("product_qty", currentQty)
                    }
                    viewModel.updateCartRequest(token, data.id!!, rawData)
                        .observe(viewLifecycleOwner, observer.cart)
                }

            }
        }

        override fun onItemClicked(itemView: View, data: CartRequest, itemIndex: Int) {}

    }

    private fun processDelete(token: String, id: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.delete_cart_disclaimer)
        ) {
            viewModel.deleteCartRequest(token, id)
                .observe(viewLifecycleOwner, observer.cart)
        }.show(parentFragmentManager, "cutomDialog")
    }

    private val observer = object {
        val cart = Observer<Resource<CartRequest>> {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    sharedPref.get(AUTH_TOKEN)?.let { token ->
                        viewModel.getCartRequest(token).observe(viewLifecycleOwner, { res ->
                            when (res.status) {
                                Status.LOADING -> {
                                }
                                Status.SUCCESS -> {
                                    res.data?.let { items ->
                                        if (items.isEmpty()) {
                                            binding.rvCart.visibility = View.GONE
                                            binding.layoutEmpty.visibility = View.VISIBLE
                                        } else
                                            adapter.addData(items)

                                        var totalPrice: Long = 0
                                        items.map { cr ->
                                            totalPrice += (cr.product?.price!!.times(cr.qty!!.toLong()))
                                        }
                                        binding.bottomCart.tvTotalPrice.text =
                                            Helpers.changeToRupiah(totalPrice.toDouble())

                                        if (items.isEmpty()) {
                                            binding.bottomCart.btnSubmitCheckout.isEnabled = false
                                            binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                                ContextCompat.getColor(
                                                    requireContext(),
                                                    R.color.divider
                                                )
                                            )
                                        } else {
                                            binding.bottomCart.btnSubmitCheckout.isEnabled = true
                                            binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                                ContextCompat.getColor(
                                                    requireContext(),
                                                    R.color.green
                                                )
                                            )
                                        }
                                    }
                                }
                                Status.ERROR -> {
                                    Toast.makeText(
                                        requireContext(),
                                        getString(R.string.network_error),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        })
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.network_error),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}