package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.request

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequest
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentTransactionRequestBinding
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.BottomSheetTransaction
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.request.DetailTransactionRequestActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import kotlinx.android.synthetic.main.item_request_transaction.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class TransactionRequestFragment : Fragment(), BottomSheetListener {
    private var _binding: FragmentTransactionRequestBinding? = null
    private val binding get() = _binding!!
    private val viewModel: TransactionRequestViewModel by viewModel()

    // utils
    private var hideFilter = true
    private var updateJob: Job? = null
    private var currentStatus: Int? = null
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusablePagingAdapter<TransactionRequest>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTransactionRequestBinding.inflate(inflater, container, false)

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(requireActivity(), AUTH_TOKEN)
        adapter = ReusablePagingAdapter(requireContext())

        // setup adapter
        setupAdapter(binding.rvTransaction)

        // init ui
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                viewModel.getListTransaction(token, "", "DESC", currentStatus).collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvTransaction.visibility = View.GONE
                        binding.shimmerTrx.rootLayout.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.rvTransaction.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                            binding.shimmerTrx.rootLayout.visibility = View.GONE
                        }
                    } else {
                        // handle if data is exists
                        binding.rvTransaction.visibility = View.VISIBLE
                        binding.shimmerTrx.rootLayout.visibility = View.GONE

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvTransaction.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                            binding.shimmerTrx.rootLayout.visibility = View.GONE
                        }
                    }
                }
            }

            // search
            binding.etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(p0: Editable?) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (p0?.trim() == "") {
                        adapter.submitData(lifecycle, PagingData.empty())
                        adapter.notifyDataSetChanged()
                    } else {
                        adapter.submitData(lifecycle, PagingData.empty())
                        adapter.notifyDataSetChanged()

                        updateJob?.cancel()
                        updateJob = lifecycleScope.launch {
                            viewModel.getListTransaction(token, p0.toString(), "DESC", currentStatus).collect {
                                adapter.submitData(it)
                            }
                        }
                    }
                }
            })

            // filter toggle
            binding.btnFilter.setOnClickListener {
                hideFilter = !hideFilter

                if (hideFilter)
                    binding.layoutFilter.visibility = View.GONE
                else
                    binding.layoutFilter.visibility = View.VISIBLE
            }

            binding.layoutFilter.setOnClickListener {
                BottomSheetTransaction(
                    requireActivity(),
                    currentStatus,
                    this
                ).showBottomSheet()
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_request_transaction)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<TransactionRequest> {
        override fun initComponent(itemView: View, data: TransactionRequest, itemIndex: Int) {
            itemView.tv_date.text = Helpers.convertToDate(data.date!!)
            itemView.tv_total_price.text = Helpers.changeToRupiah(data.total?.toDouble()!!)
            itemView.tv_product_name.text = data.item?.product?.name
            itemView.tv_qty.text = StringBuilder().append(data.item?.qty).append(" produk")

            when (data.status) {
                0 -> {
                    itemView.tv_status.text = getString(R.string.menunggu_konfirmasi)
                    itemView.tv_status.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.dark
                        )
                    )
                    itemView.card_status.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.grey_light
                        )
                    )
                }
                2 -> {
                    itemView.tv_status.text = getString(R.string.diproses)
                    itemView.tv_status.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.priceColor
                        )
                    )
                    itemView.card_status.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.priceColorTransparent
                        )
                    )
                }
                3 -> {
                    itemView.tv_status.text = getString(R.string.siap_ambil)
                    itemView.tv_status.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorPrimary
                        )
                    )
                    itemView.card_status.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorPrimaryTransparent
                        )
                    )
                }
                4 -> {
                    itemView.tv_status.text = getString(R.string.selesai)
                    itemView.tv_status.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.green
                        )
                    )
                    itemView.card_status.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.greenTransparent
                        )
                    )
                }
                5 -> {
                    itemView.tv_status.text = getString(R.string.dibatalkan)
                    itemView.tv_status.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.red
                        )
                    )
                    itemView.card_status.setCardBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.redTransparent
                        )
                    )
                }
            }
        }

        override fun onItemClicked(itemView: View, data: TransactionRequest, itemIndex: Int) {
            startActivity(
                Intent(requireContext(), DetailTransactionRequestActivity::class.java).putExtra(
                    DetailTransactionRequestActivity.EXTRA_ID,
                    data.id
                )
            )
        }

    }

    override fun selectStatus(status: Int?) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                val search = binding.etSearch.text.toString()
                viewModel.getListTransaction(token, search, "DESC", status).collect {
                    adapter.notifyDataSetChanged()
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvTransaction.visibility = View.GONE
                        binding.shimmerTrx.rootLayout.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.rvTransaction.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                            binding.shimmerTrx.rootLayout.visibility = View.GONE
                        }
                    } else {
                        // handle if data is exists
                        binding.rvTransaction.visibility = View.VISIBLE
                        binding.shimmerTrx.rootLayout.visibility = View.GONE

                        // status check
                        when (status) {
                            0 -> {
                                currentStatus = 0
                                binding.tvStatus.text = getString(R.string.menunggu_konfirmasi)
                            }
                            2 -> {
                                currentStatus = 2
                                binding.tvStatus.text = getString(R.string.diproses)
                            }
                            3 -> {
                                currentStatus = 3
                                binding.tvStatus.text = getString(R.string.siap_untuk_ambil)
                            }
                            4 -> {
                                currentStatus = 4
                                binding.tvStatus.text = getString(R.string.berhasil)
                            }
                            5 -> {
                                currentStatus = 5
                                binding.tvStatus.text = getString(R.string.tidak_berhasil)
                            }
                            null -> {
                                currentStatus = null
                                binding.tvStatus.text = getString(R.string.semua_status)
                            }
                        }

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvTransaction.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                            binding.shimmerTrx.rootLayout.visibility = View.GONE
                        }
                    }
                }
            }
       }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}