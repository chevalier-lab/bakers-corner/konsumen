package com.aitekteam.developer.bakerscornerapps.ui.profile.request

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase
import com.google.gson.JsonObject

class RequestMenuViewModel(
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun addRequestMenu(token: String, jsonObject: JsonObject): LiveData<Resource<CartRequest>> =
        transactionUseCase.addRequestMenu(token, jsonObject).asLiveData()
}