package com.aitekteam.developer.bakerscornerapps.utils.helper

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.text.NumberFormat
import java.util.*

object Helpers {

    fun changeToRupiah(uang: Double): String {
        val localeID =  Locale("in", "ID")
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        return numberFormat.format(uang).toString()
    }

    fun convertToDateTime(dateTime: String): String {
        val arrTime = dateTime.split(" ")
        val arrDate = arrTime[0].split("-")
        val arrHour = arrTime[1].split(":")

        return StringBuilder()
            .append("${arrDate[2]} ")
            .append("${getMonth(arrDate[1])} ")
            .append("${arrDate[0]}, ")
            .append("${arrHour[0]}:${arrHour[1]} ")
            .append("WIB")
            .toString()
    }

    fun convertToDate(dateTime: String): String {
        val arrTime = dateTime.split(" ")
        val arrDate = arrTime[0].split("-")

        return StringBuilder()
            .append("${arrDate[2]} ")
            .append("${getMonth(arrDate[1])} ")
            .append(arrDate[0])
            .toString()
    }

    private fun getMonth(month: String): String {
        return when (month) {
            "01" -> "Januari"
            "02" -> "Februari"
            "03" -> "Maret"
            "04" -> "April"
            "05" -> "Mei"
            "06" -> "Juni"
            "07" -> "Juli"
            "08" -> "Agustus"
            "09" -> "September"
            "10" -> "Oktober"
            "11" -> "November"
            "12" -> "Desember"
            else -> "error"
        }
    }

    private val formatAccumulated: (String) -> String = {
        val splitDate = it.split("-")
        val day = splitDate[2]

        when (splitDate[1]) {
            "01" -> "Jan $day"
            "02" -> "Feb $day"
            "03" -> "Mar $day"
            "04" -> "Apr $day"
            "05" -> "May $day"
            "06" -> "Jun $day"
            "07" -> "Jul $day"
            "08" -> "Aug $day"
            "09" -> "Sep $day"
            "10" -> "Okt $day"
            "11" -> "Nov $day"
            "12" -> "Dec $day"
            else -> ""
        }
    }

    fun String.dateFormat(): String {
        val splitDateTime = this.split(" ")
        val splitDate = splitDateTime[0].split("-")
        val splitTime = splitDateTime[1].split(":")

        return StringBuilder().apply {
            append("${splitTime[0]}:${splitTime[1]}, ")
            append(formatAccumulated(splitDateTime[0]))
            append(" ${splitDate[0]}")
        }.toString()
    }

//    fun isOnline(context: Context): Boolean {
//        val connectivityManager =
//            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            val capabilities =
//                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
//
//            if (capabilities != null) {
//                when {
//                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
//                        Timber.i("NetworkCapabilities.TRANSPORT_CELLULAR")
//                        return true
//                    }
//                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
//                        Timber.i("NetworkCapabilities.TRANSPORT_WIFI")
//                        return true
//                    }
//                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
//                        Timber.i("NetworkCapabilities.TRANSPORT_ETHERNET")
//                        return true
//                    }
//                }
//            }
//        }
//        return false
//    }

    fun isOnline(ctx: Context): Boolean {
        val connectionManager: ConnectivityManager =
            ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectionManager.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting === true
    }
}