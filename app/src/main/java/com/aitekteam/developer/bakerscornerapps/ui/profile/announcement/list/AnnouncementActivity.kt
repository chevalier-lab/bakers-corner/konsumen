package com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.list

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityAnnouncementBinding
import com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.detail.DetailAnnouncementActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import kotlinx.android.synthetic.main.item_notification.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class AnnouncementActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAnnouncementBinding
    private val viewModel: AnnouncementViewModel by viewModel()

    // utils
    private var updateJob: Job? = null
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusablePagingAdapter<Announcement>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAnnouncementBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvAnnouncement)

        // init UI
        initUI()
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                viewModel.getAnnouncements(token).collect {
                    adapter.submitData(it)
                }
            }
        }

        with(adapter) {
            // loading state
            addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading) {
                    binding.layoutEmpty.visibility = View.GONE
                    binding.layoutError.visibility = View.GONE
                    binding.rvAnnouncement.visibility = View.GONE
                    binding.shimmerAnnouncement.rootLayout.visibility = View.VISIBLE
                } else if (loadState.append.endOfPaginationReached) {
                    // handle if data is empty
                    if (adapter.itemCount < 1) {
                        binding.rvAnnouncement.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.VISIBLE
                        binding.shimmerAnnouncement.rootLayout.visibility = View.GONE
                    }
                } else {
                    // handle if data is exists
                    binding.layoutError.visibility = View.GONE
                    binding.rvAnnouncement.visibility = View.VISIBLE
                    binding.shimmerAnnouncement.rootLayout.visibility = View.GONE

                    // get error
                    val error = when {
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                        else -> null
                    }

                    error?.let {
                        binding.layoutEmpty.visibility = View.GONE
                        binding.rvAnnouncement.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        binding.shimmerAnnouncement.rootLayout.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_notification)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Announcement> {
        override fun initComponent(itemView: View, data: Announcement, itemIndex: Int) {
            itemView.tv_title.text = data.title
            itemView.tv_content.text = data.subtitle
            itemView.tv_time.text = Helpers.convertToDateTime(data.date!!)
        }

        override fun onItemClicked(itemView: View, data: Announcement, itemIndex: Int) {
            startActivity(
                Intent(this@AnnouncementActivity, DetailAnnouncementActivity::class.java)
                    .putExtra(DetailAnnouncementActivity.EXTRA_DATA, data)
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}