package com.aitekteam.developer.bakerscornerapps.utils.helper

import android.content.Context
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object DialogUtil {

    fun showDialog(context: Context, title: String,
                   message: String, callback: () -> Unit) {
        MaterialAlertDialogBuilder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ya") { dialog, _ ->
                callback()
                dialog.dismiss()
            }
            .setNegativeButton("Batal") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(false)
            .show()
    }

    fun showDialogPositiveOnly(context: Context, title: String,
                               message: String, callback: () -> Unit) {
        MaterialAlertDialogBuilder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ya") { dialog, _ ->
                callback()
                dialog.dismiss()
            }
            .setCancelable(false)
            .show()
    }

    fun showDialogNoTitle(context: Context, layout: Int) {
        MaterialAlertDialogBuilder(context)
            .setView(layout)
            .show()
    }

}