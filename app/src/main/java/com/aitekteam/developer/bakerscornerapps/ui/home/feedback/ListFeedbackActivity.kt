package com.aitekteam.developer.bakerscornerapps.ui.home.feedback

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityListFeedbackBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.convertToDate
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_feedback.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class ListFeedbackActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListFeedbackBinding
    private val viewModel: ListFeedbackViewModel by viewModel()

    // utils
    private var updateJob: Job? = null
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusablePagingAdapter<Feedback>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListFeedbackBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvFeedback)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            val productId = extra.getString(EXTRA_ID)
            productId?.let { initUI(it) }
        }
    }

    private fun initUI(productId: String) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                viewModel.getFeedback(token, productId, "", "DESC").collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvFeedback.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.layoutShimmer.visibility = View.VISIBLE
                    } else {
                        // handle if data is exists
                        binding.rvFeedback.visibility = View.VISIBLE
                        binding.layoutShimmer.visibility = View.GONE

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvFeedback.visibility = View.GONE
                            binding.layoutShimmer.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_feedback)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Feedback> {
        override fun initComponent(itemView: View, data: Feedback, itemIndex: Int) {
            itemView.tv_user_name.text = data.user?.name
            itemView.tv_date.text = convertToDate(data.date!!)
            itemView.tv_review.text = data.comment

            // set rating
            when (data.rating) {
                1 -> Glide.with(this@ListFeedbackActivity)
                    .load(R.drawable.ic_star_on)
                    .into(itemView.star_1)
                2 -> {
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_1)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_2)
                }
                3 -> {
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_1)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_2)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_3)
                }
                4 -> {
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_1)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_2)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_3)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_4)
                }
                5 -> {
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_1)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_2)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_3)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_4)
                    Glide.with(this@ListFeedbackActivity)
                        .load(R.drawable.ic_star_on)
                        .into(itemView.star_5)
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Feedback, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_ID = "extra_id"
    }
}