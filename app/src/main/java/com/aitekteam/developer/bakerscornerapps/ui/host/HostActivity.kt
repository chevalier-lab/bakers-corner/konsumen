package com.aitekteam.developer.bakerscornerapps.ui.host

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.aitekteam.developer.bakerscornerapps.MyApplication
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityHostBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_EMAIL
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ID
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_NAME
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import io.socket.client.Socket
import org.koin.android.viewmodel.ext.android.viewModel

class HostActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHostBinding
    private val viewModel: HostViewModel by viewModel()

    private lateinit var sharedPrefs: SharedPrefsUtil

    private var mSocket: Socket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPrefs = SharedPrefsUtil()
        sharedPrefs.start(this, AUTH_TOKEN)

        // init socket
        val app: MyApplication = application as MyApplication
        mSocket = app.getSocket()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        val navView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment)
        navView.setupWithNavController(navController)

        // get parcelable data
        val extra = intent.extras
        if (extra != null) {
            val moveCart = extra.getBoolean(EXTRA_MOVE_CART, false)
            val moveProfile = extra.getBoolean(EXTRA_MOVE_PROFILE, false)

            if (moveCart)
                navController.navigate(R.id.navigation_cart)
            else if (moveProfile)
                navController.navigate(R.id.navigation_profile)
        }

        // fetch profile
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {}
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            sharedPrefs.setString(AUTH_TOKEN, data.token!!)
                            sharedPrefs.setString(USER_ID, data.id!!)
                            sharedPrefs.setString(USER_NAME, data.name!!)
                            sharedPrefs.setString(USER_EMAIL, data.email!!)
                        }
                    }
                    Status.ERROR -> {}
                }
            }
        }
    }

    companion object {
        const val EXTRA_MOVE_CART = "extra_move_cart"
        const val EXTRA_MOVE_PROFILE = "extra_move_profile"
    }
}