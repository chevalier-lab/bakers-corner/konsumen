package com.aitekteam.developer.bakerscornerapps.ui.home.detail

import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityDetailProductBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.feedback.ListFeedbackActivity
import com.aitekteam.developer.bakerscornerapps.ui.home.recommended.RecommendedProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist.WishlistActivity
import com.aitekteam.developer.bakerscornerapps.ui.service.chat.ChatActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.convertToDate
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import kotlinx.android.synthetic.main.item_recommended_product.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class DetailProductActivity : AppCompatActivity(), BottomSheetListener {
    private lateinit var binding: ActivityDetailProductBinding
    private val viewModel: DetailProductViewModel by viewModel()

    // utils
    private var isFavorite = false
    private var condition: String? = null
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var recommendedProductAdapter: ReusableAdapter<Product>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        recommendedProductAdapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvRecomendedProduct)

        // get extras
        val extra = intent.extras
        if (extra != null) {
            val productId = extra.getString(EXTRA_ID)
            if (productId != null) initUI(productId)
            condition = extra.getString(EXTRA_CONDITION)
        }
    }

    private fun initUI(productId: String) {
        // fetch data
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.detailProduct(token, productId).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        visibleUI(true)
                    }
                    Status.SUCCESS -> {
                        visibleUI(false)
                        it.data?.let { data ->
                            // slider
                            val slides = mutableListOf<SlideModel>()
                            slides.add(SlideModel(data.thumbnail?.url))

                            data.image?.map { media ->
                                slides.add(SlideModel(media.url))
                            }
                            binding.imageSlider.apply {
                                setImageList(slides, ScaleTypes.CENTER_CROP)
                            }

                            // discount
                            if (data.discount == "0")
                                binding.tvPrice.text =
                                    Helpers.changeToRupiah(data.price!!.toDouble())
                            else {
                                binding.layoutDiscount.visibility = View.VISIBLE

                                // set new price
                                val oldPrice = data.price!!
                                val discount = oldPrice * data.discount!!.toLong() / 100
                                val newPrice = oldPrice - discount

                                // set view
                                binding.tvPriceBefore.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                                binding.tvDiscount.text =
                                    StringBuilder().append("${data.discount}%")
                                binding.tvPriceBefore.text =
                                    Helpers.changeToRupiah(oldPrice.toDouble())
                                binding.tvPrice.text = Helpers.changeToRupiah(newPrice.toDouble())
                            }

                            // utils
                            binding.toolbarTitle.text = data.name
                            binding.tvProductName.text = data.name
                            binding.tvDescription.text = data.description
                            binding.tvCategory.text = data.category?.category

                            // status
                            if (data.visible == "0") {
                                binding.tvStatus.text = getString(R.string.habis)
                                binding.tvStatus.setTextColor(
                                    ContextCompat.getColor(
                                        this@DetailProductActivity,
                                        R.color.red
                                    )
                                )
                            } else {
                                binding.tvStatus.text = getString(R.string.ready)
                                binding.tvStatus.setTextColor(
                                    ContextCompat.getColor(
                                        this@DetailProductActivity,
                                        R.color.green
                                    )
                                )
                            }

                            binding.layoutButton.btnBuyCart.setOnClickListener {
                                if (data.visible == "0")
                                    Toast.makeText(
                                        this,
                                        "${data.name} lagi tidak tersedia!", Toast.LENGTH_SHORT
                                    ).show()
                                else
                                    BottomSheetAddCart(this, productId, this).showBottomSheet()
                            }

                            binding.layoutButton.btnBuyNow.setOnClickListener {
                                if (data.visible == "0")
                                    Toast.makeText(
                                        this,
                                        "${data.name} lagi tidak tersedia!", Toast.LENGTH_SHORT
                                    ).show()
                                else
                                    viewModel.addToCartProduct(token, productId, 1)
                                        .observe(this, observer.addToCart(true))
                            }

                            binding.layoutButton.btnChat.setOnClickListener {
                                startActivity(
                                    Intent(this, ChatActivity::class.java).putExtra(
                                        ChatActivity.EXTRA_ID,
                                        "${data.name} ready ga?"
                                    )
                                )
                            }
                        }
                    }
                    Status.ERROR -> {
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutShimmer.visibility = View.GONE
                        binding.layoutButton.rootLayout.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        Timber.e("error => ${it.message}")
                    }
                }
            }

            viewModel.recommendedProduct(token, 0, "", "ASC")
                .observe(this, observer.recommendedProduct)

            viewModel.getWishlist(token, productId).observe(this, observer.favoriteCheck(productId))

            viewModel.getFeedback(token, productId, 0, "", "DESC")
                .observe(this, observer.feedback)

            binding.btnFavorite.setOnClickListener {
                if (isFavorite) {
                    viewModel.deleteWishlist(token, productId)
                        .observe(this, observer.deleteWishlist)
                } else {
                    viewModel.addToWishlist(token, productId).observe(this, observer.addWishlist)
                }
            }

            binding.btnShowFeedback.setOnClickListener {
                startActivity(
                    Intent(this, ListFeedbackActivity::class.java).putExtra(
                        ListFeedbackActivity.EXTRA_ID,
                        productId
                    )
                )
            }

            binding.btnShowRecommended.setOnClickListener {
                startActivity(Intent(this, RecommendedProductActivity::class.java))
            }
        }
    }

    private val observer = object {
        val recommendedProduct = Observer<Resource<ProductsResponse>> {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { res ->
                        res.data?.let { items ->
                            val randItems = items.shuffled(Random(System.currentTimeMillis()))
                            recommendedProductAdapter.addData(randItems)
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutButton.rootLayout.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.e("error => ${it.message}")
                }
            }
        }

        fun favoriteCheck(productId: String) = Observer<Resource<Wishlist>> {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        if ((data.product?.id == productId)) {
                            isFavorite = !isFavorite
                            renderFavorite(isFavorite)
                        }
                    }
                }
                Status.ERROR -> {
                    Timber.e("error => ${it.message}")
                }
            }
        }

        val addWishlist = Observer<Resource<Wishlist>> {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    isFavorite = !isFavorite
                    renderFavorite(isFavorite)
                    Toast.makeText(
                        this@DetailProductActivity,
                        "Berhasil memasukkan produk ke wishlist!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                Status.ERROR -> {
                    Timber.e("error => ${it.message}")
                    Toast.makeText(
                        this@DetailProductActivity,
                        "Gagal memasukkan produk ke wishlist:(",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        val deleteWishlist = Observer<Resource<Wishlist>> {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    isFavorite = !isFavorite
                    renderFavorite(isFavorite)
                    Toast.makeText(
                        this@DetailProductActivity,
                        "Berhasil hapus favorite!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                Status.ERROR -> {
                    Timber.e("error => ${it.message}")
                    Toast.makeText(
                        this@DetailProductActivity,
                        "Gagal hapus favorite:(",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        fun addToCart(navigate: Boolean) = Observer<Resource<CartProduct>> {
            when (it.status) {
                Status.LOADING -> {
                    binding.layoutButton.btnBuyNow.isEnabled = false
                    binding.layoutButton.btnBuyCart.isEnabled = false
                }
                Status.SUCCESS -> {
                    binding.layoutButton.btnBuyNow.isEnabled = true
                    binding.layoutButton.btnBuyCart.isEnabled = true
                    if (navigate)
                        startActivity(
                            Intent(
                                this@DetailProductActivity,
                                HostActivity::class.java
                            ).putExtra(
                                HostActivity.EXTRA_MOVE_CART,
                                true
                            )
                        )
                    Toast.makeText(
                        this@DetailProductActivity,
                        "Berhasil menambahkan produk ke keranjang!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                Status.ERROR -> {
                    binding.layoutButton.btnBuyNow.isEnabled = true
                    binding.layoutButton.btnBuyCart.isEnabled = true
                    Toast.makeText(
                        this@DetailProductActivity,
                        "Gagal menambahkan produk ke keranjang:(",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        val feedback = Observer<Resource<List<Feedback>>> {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        if (data.isEmpty())
                            binding.layoutFeedback.visibility = View.GONE
                        else {
                            binding.layoutFeedback.visibility = View.VISIBLE

                            // set utils
                            binding.itemFeedback.tvUserName.text = data[0].user?.name
                            binding.itemFeedback.tvDate.text = convertToDate(data[0].date!!)
                            binding.itemFeedback.tvReview.text = data[0].comment

                            // set rating
                            when (data[0].rating) {
                                1 -> Glide.with(this@DetailProductActivity)
                                    .load(R.drawable.ic_star_on)
                                    .into(binding.itemFeedback.star1)
                                2 -> {
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star1)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star2)
                                }
                                3 -> {
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star1)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star2)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star3)
                                }
                                4 -> {
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star1)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star2)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star3)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star4)
                                }
                                5 -> {
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star1)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star2)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star3)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star4)
                                    Glide.with(this@DetailProductActivity)
                                        .load(R.drawable.ic_star_on)
                                        .into(binding.itemFeedback.star5)
                                }
                                else -> {
                                }
                            }
                        }
                    }
                }
                Status.ERROR -> {
                    Timber.e("error => ${it.message}")
                }
            }
        }
    }

    private fun renderFavorite(favorite: Boolean) {
        if (favorite)
            binding.imageFavorite.setImageResource(R.drawable.ic_loved)
        else
            binding.imageFavorite.setImageResource(R.drawable.ic_love)
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.layoutMain.visibility = View.GONE
            binding.layoutShimmer.visibility = View.VISIBLE
            binding.layoutButton.rootLayout.visibility = View.GONE
        } else {
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutShimmer.visibility = View.GONE
            binding.layoutButton.rootLayout.visibility = View.VISIBLE
        }
    }

    override fun addToCart(qty: Int, productId: String) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.addToCartProduct(token, productId, qty)
                .observe(this, observer.addToCart(false))
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        recommendedProductAdapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_recommended_product)
            .isHorizontalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Product> {
        override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
            // set utils
            itemView.tv_nama_produk_recommended.text = data.name

            // set gambar
            Glide.with(this@DetailProductActivity)
                .load(data.thumbnail?.url)
                .into(itemView.image_product_recommended)

            // set status makanan
            val setStatus = fun(text: String, color: Int) {
                itemView.tv_status_recommended.text = StringBuilder().append(text)
                itemView.status_recommended.setBackgroundResource(color)
            }

            when (data.visible) {
                "0" -> setStatus("Habis", R.drawable.status_habis)
                "1" -> setStatus("Ready", R.drawable.status_ready)
            }

            // set discount
            if (data.discount == "0") {
                itemView.tv_harga_recommended.text = Helpers.changeToRupiah(data.price!!.toDouble())
            } else {
                // set new price
                val oldPrice = data.price!!
                val discount = oldPrice * data.discount!!.toLong() / 100
                val newPrice = oldPrice - discount

                // set view
                itemView.tv_price_before_recommended.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tv_discount_recommended.text = StringBuilder().append("${data.discount}% OFF")
                itemView.tv_price_before_recommended.text = Helpers.changeToRupiah(oldPrice.toDouble())
                itemView.tv_harga_recommended.text = Helpers.changeToRupiah(newPrice.toDouble())
            }
        }

        override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
            startActivity(
                Intent(this@DetailProductActivity, DetailProductActivity::class.java).putExtra(
                    EXTRA_ID,
                    data.id
                )
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cart, menu)
        menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_cart)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (condition != null)
                    startActivity(Intent(this, WishlistActivity::class.java))
                finish()
                true
            }
            R.id.favorite -> {
                startActivity(
                    Intent(
                        this,
                        HostActivity::class.java
                    ).putExtra(HostActivity.EXTRA_MOVE_CART, true)
                )
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (condition != null)
            startActivity(Intent(this, WishlistActivity::class.java))
        finish()
    }

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_CONDITION = "extra_condition"
    }
}