package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.request

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequest
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.TransactionService
import okio.IOException
import retrofit2.HttpException

class TransactionRequestPagingSource(
    private val token: String,
    private val search: String,
    private val direction: String,
    private val status: Int?,
    private val transactionService: TransactionService
) : PagingSource<Int, TransactionRequest>() {
    override fun getRefreshKey(state: PagingState<Int, TransactionRequest>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TransactionRequest> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = transactionService.getListTransactionRequestPaging(
            token,
            currentPage,
            search,
            direction,
            status
        )
        val responseList = mutableListOf<TransactionRequest>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}