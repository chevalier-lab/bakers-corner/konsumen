package com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityWishlistBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import com.paginate.Paginate
import kotlinx.android.synthetic.main.item_wishlist.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class WishlistActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWishlistBinding
    private val viewModel: WishlistViewModel by viewModel()

    // utils
    private var page = 0
    private var totalData = 0
    private var loading = false
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusableAdapter<Wishlist>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWishlistBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvWishlist)

        // init UI
        initUI()

        // handle paging
        Paginate.with(binding.rvWishlist, pagingCallback)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .build()
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getWishlist(token, page, "DESC").observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.layoutError.visibility = View.GONE
                        binding.rvWishlist.visibility = View.GONE
                        binding.shimmerWishlist.rootLayout.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.layoutError.visibility = View.GONE
                        binding.rvWishlist.visibility = View.VISIBLE
                        binding.shimmerWishlist.rootLayout.visibility = View.GONE
                        it.data?.let { res ->
                            // set total data
                            totalData = res.meta?.size?.total!!

                            res.data?.let { items ->
                                if (items.isNotEmpty())
                                    adapter.addData(items)
                                else {
                                    binding.rvWishlist.visibility = View.GONE
                                    binding.layoutEmpty.visibility = View.VISIBLE
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        binding.rvWishlist.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        binding.shimmerWishlist.rootLayout.visibility = View.GONE
                        Toast.makeText(this, getString(R.string.network_error), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
    }

    private val pagingCallback = object : Paginate.Callbacks {
        override fun onLoadMore() {
            page++
            loading = true
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                viewModel.getWishlist(token, page, "DESC").observe(this@WishlistActivity) {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            it.data?.let { res ->
                                res.data?.let { items ->
                                    if (items.isNotEmpty())
                                        adapter.addMoreData(items)
                                }

                                loading = false
                            }
                        }
                        Status.ERROR -> {
                        }
                    }
                }
            }
        }

        override fun isLoading(): Boolean {
            return loading
        }

        override fun hasLoadedAllItems(): Boolean {
            Timber.i("paging total => ${adapter.itemCount} $totalData")
            return adapter.itemCount == totalData
        }

    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_wishlist)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Wishlist> {
        override fun initComponent(itemView: View, data: Wishlist, itemIndex: Int) {
            // set utils
            itemView.tv_nama_produk.text = data.product?.name
            itemView.tv_harga.text = Helpers.changeToRupiah(data.product?.price!!.toDouble())

            // set gambar
            Glide.with(this@WishlistActivity)
                .load(data.product?.thumbnail?.url)
                .into(itemView.image_product)

            when (data.product?.visible) {
                "0" -> {
                    itemView.btn_habis.visibility = View.VISIBLE
                    itemView.btn_add_cart.visibility = View.GONE
                }
                "1" -> {
                    itemView.btn_habis.visibility = View.GONE
                    itemView.btn_add_cart.visibility = View.VISIBLE
                }
            }

            itemView.btn_habis.setOnClickListener {
                Toast.makeText(
                    this@WishlistActivity,
                    "Produk ini sedang tidak tersedia",
                    Toast.LENGTH_SHORT
                ).show()
            }

            itemView.btn_delete.setOnClickListener {
                CustomDialogFragment(
                    getString(R.string.perhatian),
                    getString(R.string.unfavorite_disclaimer)
                ) {
                    sharedPref.get(AUTH_TOKEN)?.let { token ->
                        viewModel.deleteWishlist(token, data.product?.id!!)
                            .observe(this@WishlistActivity, { res ->
                                when (res.status) {
                                    Status.LOADING -> {
                                    }
                                    Status.SUCCESS -> {
                                        totalData -= 1
                                        adapter.deleteData(data)

                                        if (adapter.itemCount == 0) {
                                            binding.rvWishlist.visibility = View.GONE
                                            binding.layoutEmpty.visibility = View.VISIBLE
                                        }
                                        Toast.makeText(
                                            this@WishlistActivity,
                                            "Berhasil hapus favorit!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                    Status.ERROR -> {
                                        Toast.makeText(
                                            this@WishlistActivity,
                                            "Gagal hapus favorit!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            })
                    }
                }.show(supportFragmentManager, "cutomDialog")

            }

            itemView.btn_add_cart.setOnClickListener {
                sharedPref.get(AUTH_TOKEN)?.let { token ->
                    viewModel.addToCartProduct(token, data.product?.id!!, 1)
                        .observe(this@WishlistActivity, { res ->
                            when (res.status) {
                                Status.LOADING -> {
                                }
                                Status.SUCCESS -> {
                                    startActivity(
                                        Intent(
                                            this@WishlistActivity,
                                            HostActivity::class.java
                                        ).putExtra(
                                            HostActivity.EXTRA_MOVE_CART,
                                            true
                                        )
                                    )
                                    Toast.makeText(
                                        this@WishlistActivity,
                                        "Berhasil menambahkan produk ke keranjang!",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                                Status.ERROR -> {
                                    Toast.makeText(
                                        this@WishlistActivity,
                                        "Gagal menambahkan produk ke keranjang:(",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        })
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Wishlist, itemIndex: Int) {
            startActivity(
                Intent(this@WishlistActivity, DetailProductActivity::class.java).apply {
                    putExtra(DetailProductActivity.EXTRA_ID, data.product?.id)
                    putExtra(DetailProductActivity.EXTRA_CONDITION, "wishlist")
                }
            )
            finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}