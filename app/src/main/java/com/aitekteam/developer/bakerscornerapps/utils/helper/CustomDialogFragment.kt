package com.aitekteam.developer.bakerscornerapps.utils.helper

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentCustomDialogBinding

class CustomDialogFragment(
    private val title: String,
    private val content: String,
    private val callback: () -> Unit
) : DialogFragment() {
    private var _binding: FragmentCustomDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCustomDialogBinding.inflate(layoutInflater, container, false)

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        binding.tvTitle.text = title
        binding.tvContent.text = content

        binding.btnPositive.setOnClickListener {
            callback()
            dismiss()
        }

        binding.btnNegative.setOnClickListener { dismiss() }

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}