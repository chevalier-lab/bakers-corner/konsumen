package com.aitekteam.developer.bakerscornerapps.utils.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.product.TransactionProductFragment
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.request.TransactionRequestFragment

class SectionTransactionAdapter(fm: FragmentManager, lifecycle: Lifecycle): FragmentStateAdapter(fm, lifecycle) {

    private val fragments: List<Fragment> = listOf(
        TransactionProductFragment(),
        TransactionRequestFragment()
    )

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}