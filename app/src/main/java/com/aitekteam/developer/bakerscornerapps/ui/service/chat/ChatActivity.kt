package com.aitekteam.developer.bakerscornerapps.ui.service.chat

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.MyApplication
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatListener
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityChatBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.CHAT_ID
import com.aitekteam.developer.bakerscornerapps.utils.Constants.CHAT_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ID
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.isOnline
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import io.socket.client.Socket
import kotlinx.android.synthetic.main.item_message.view.*
import kotlinx.android.synthetic.main.item_notification.view.tv_content
import kotlinx.android.synthetic.main.item_notification.view.tv_time
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class ChatActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChatBinding

    private val viewModel: ChatViewModel by viewModel()

    // utils
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusableAdapter<ChatListener>

    //Socket.io
    private var mSocket: Socket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        // Socket instance
        val app: MyApplication = application as MyApplication
        mSocket = app.getSocket()

        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)

        joinSocket()
        listenerSocket()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        adapter = ReusableAdapter(this)
        setupAdapter(binding.rvChat)

        init()

        binding.rvChat.scrollToPosition(adapter.itemCount - 1)
        binding.btnSend.setOnClickListener {
            sendMessage()
        }

        // get extras
        val extra = intent.extras
        if (extra != null) {
            val trxId = extra.getString(EXTRA_ID)
            if (trxId != null) binding.etMessage.setText(trxId)
        }
    }

    private fun joinSocket() {
        sharedPref.get(USER_ID).let {
            mSocket?.on("infoRoom") { args ->
                this.runOnUiThread(Runnable {
                    val data = args[0] as JSONObject
                    try {
                        Timber.i("socket_info => $data")
                        sharedPref.setString(CHAT_ID, data.getString("id"))
                        sharedPref.setString(CHAT_TOKEN, data.getString("chat_token"))
                    } catch (e: JSONException) {
                        return@Runnable
                    }
                })
            }

            mSocket?.emit("joinRoom", it)
        }
    }

    private fun reInitView() {
        if (ChatStorage.getHistoryChat().isEmpty()) {
            binding.rvChat.visibility = View.GONE
            binding.containerNoData.visibility = View.VISIBLE
        } else {
            binding.containerNoData.visibility = View.GONE
            binding.rvChat.visibility = View.VISIBLE
            binding.rvChat.scrollToPosition(adapter.itemCount - 1)
        }
    }

    private fun init() {
//      Get History Chat
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            sharedPref.get(USER_ID)?.let { id ->
                viewModel.getHistoryChat(token, id).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            it.data?.let { data ->
                                val arr: MutableList<ChatListener> = mutableListOf()
                                data.forEach { ch ->
                                    val isUser = ch.idMUsers.toString() == id
                                    arr.add(
                                        ChatListener(
                                            "",
                                            ch.content,
                                            ch.createdAt?.dateFormat(),
                                            isUser
                                        )
                                    )
                                }
                                ChatStorage.assignHistoryChat(arr)
                            }

                            if (ChatStorage.getHistoryChat().isNotEmpty()) {
                                adapter.addData(ChatStorage.getHistoryChat())
                            }

                            reInitView()
                        }
                        Status.ERROR -> {
                            ChatStorage.clearHistoryChat()
                            reInitView()
                        }
                    }
                }
            }
        }
    }

    private fun listenerSocket() {
        mSocket?.on(Socket.EVENT_CONNECT) {
        }
        mSocket?.on(Socket.EVENT_CONNECT_ERROR) {
        }
        mSocket?.on(Socket.EVENT_DISCONNECT) {
        }

//      Listener Message Coming
        mSocket?.on("message") { args ->
            this.runOnUiThread(Runnable {
                val data = args[0] as JSONObject

                val msg: String
                val isUser: Boolean
                val time: String

                try {
                    msg = data.getString("msg")
                    time = data.getString("time")
                    isUser = data.getBoolean("isUser")

                    val payload = ChatListener("", msg, time?.dateFormat(), isUser)

                    ChatStorage.addHistoryChat(payload)
                    adapter.addSingleData(payload)

                    reInitView()
                } catch (e: JSONException) {
                    return@Runnable
                }
            })
        }
    }

    private fun sendMessage() {
        val message = binding.etMessage.text.toString().trim { it <= ' ' }
        if (TextUtils.isEmpty(message)) {
            return
        }

        if (!isOnline(this)) {
            Toast.makeText(
                this,
                "Pastikan Kamu Terhubung ke Internet.",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        binding.etMessage.setText("")

//      post data to Database
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            sharedPref.get(USER_ID)?.let { user_id ->
                sharedPref.get(CHAT_ID)?.let { chat_id ->
                    sharedPref.get(CHAT_TOKEN)?.let { chat_token ->
                        val payload = JSONObject().apply {
                            put("content", message)
                            put("id_t_chat_user", chat_id)
                            put("id_m_user", user_id)
                        }

                        val payloadString = payload.toString()

                        val requestBody: RequestBody = MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("raw", payloadString)
                            .build()

                        viewModel.postChat(token, requestBody).observe(this) {
                            when (it.status) {
                                Status.LOADING -> {
                                }
                                Status.SUCCESS -> {
                                    it.data?.let { _ ->
                                        // post data to socket
                                        val rawData = JSONObject().apply {
                                            put("token", chat_token)
                                            put("msg", message)
                                            put("isUser", true)
                                        }
                                        mSocket?.emit("chatMessage", rawData)
                                    }
                                }
                                Status.ERROR -> {
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_message)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<ChatListener> {
        override fun initComponent(itemView: View, data: ChatListener, itemIndex: Int) {
            if (data.isUser == false) {
                itemView.chat_admin.visibility = View.VISIBLE
                itemView.chat_user.visibility = View.GONE

                itemView.tv_content_admin.text = data.text
                itemView.tv_time_admin.text = data.time
            } else {
                itemView.chat_admin.visibility = View.GONE
                itemView.chat_user.visibility = View.VISIBLE

                itemView.tv_content.text = data.text
                itemView.tv_time.text = data.time
            }
        }

        override fun onItemClicked(itemView: View, data: ChatListener, itemIndex: Int) {}
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_ID = "extra_id"
    }
}