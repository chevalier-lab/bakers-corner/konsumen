package com.aitekteam.developer.bakerscornerapps.ui.auth.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.Auth
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.AuthUseCase
import com.google.gson.JsonObject

class RegisterViewModel(
    private val authUseCase: AuthUseCase
) : ViewModel() {

    fun register(jsonObject: JsonObject): LiveData<Resource<Auth>> =
        authUseCase.register(jsonObject).asLiveData()
}