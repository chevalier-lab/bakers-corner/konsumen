package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductDetail
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase
import com.google.gson.JsonObject

class DetailTransactionProductViewModel(
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun getDetailTransaction(
        token: String,
        id: String
    ): LiveData<Resource<TransactionProductDetail>> =
        transactionUseCase.getDetailTransaction(token, id).asLiveData()

    fun cancelTransaction(token: String, id: String): LiveData<Resource<TransactionProduct>> =
        transactionUseCase.cancelTransaction(token, id).asLiveData()

    fun addReview(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): LiveData<Resource<TransactionProductDetail>> =
        transactionUseCase.addFeedback(token, id, jsonObject).asLiveData()
}