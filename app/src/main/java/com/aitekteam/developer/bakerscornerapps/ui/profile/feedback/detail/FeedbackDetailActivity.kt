package com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.detail

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import androidx.lifecycle.observe
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityFeedbackDetailBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.convertToDateTime
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_my_feedback.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class FeedbackDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFeedbackDetailBinding
    private val viewModel: FeedbackDetailViewModel by viewModel()

    // utils
    private lateinit var sharedPref: SharedPrefsUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFeedbackDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, Constants.AUTH_TOKEN)

        // get extras
        val extra = intent.extras
        if (extra != null) {
            val trxId = extra.getString(EXTRA_ID)
            if (trxId != null) initUI(trxId)
        }
    }

    private fun initUI (id: String) {
        // fetch data
        sharedPref.get(Constants.AUTH_TOKEN)?.let { token ->
            viewModel.getMyFeedbackDetail(token, id).observe(this){
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            binding.tvNamaProduk.text = data.product?.name
                            binding.tvComment.text = data.comment
                            binding.tvTanggal.text = data.date?.let { date -> convertToDateTime(date) }
                            binding.tvUsername.text = data.user?.name

                            Glide.with(this@FeedbackDetailActivity)
                                .load(data.product?.thumbnail?.url)
                                .into(binding.imageProduct)

                            when (data.rating) {
                                1 -> {
                                    assignImage(R.drawable.ic_star_on, binding.star1)
                                    assignImage(R.drawable.ic_star_off, binding.star2)
                                    assignImage(R.drawable.ic_star_off, binding.star3)
                                    assignImage(R.drawable.ic_star_off, binding.star4)
                                    assignImage(R.drawable.ic_star_off, binding.star5)
                                }
                                2 -> {
                                    assignImage(R.drawable.ic_star_on, binding.star1)
                                    assignImage(R.drawable.ic_star_on, binding.star2)
                                    assignImage(R.drawable.ic_star_off, binding.star3)
                                    assignImage(R.drawable.ic_star_off, binding.star4)
                                    assignImage(R.drawable.ic_star_off, binding.star5)
                                }
                                3 -> {
                                    assignImage(R.drawable.ic_star_on, binding.star1)
                                    assignImage(R.drawable.ic_star_on, binding.star2)
                                    assignImage(R.drawable.ic_star_on, binding.star3)
                                    assignImage(R.drawable.ic_star_off, binding.star4)
                                    assignImage(R.drawable.ic_star_off, binding.star5)
                                }
                                4 -> {
                                    assignImage(R.drawable.ic_star_on, binding.star1)
                                    assignImage(R.drawable.ic_star_on, binding.star2)
                                    assignImage(R.drawable.ic_star_on, binding.star3)
                                    assignImage(R.drawable.ic_star_on, binding.star4)
                                    assignImage(R.drawable.ic_star_off, binding.star5)
                                }
                                5 -> {
                                    assignImage(R.drawable.ic_star_on, binding.star1)
                                    assignImage(R.drawable.ic_star_on, binding.star2)
                                    assignImage(R.drawable.ic_star_on, binding.star3)
                                    assignImage(R.drawable.ic_star_on, binding.star4)
                                    assignImage(R.drawable.ic_star_on, binding.star5)
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        Timber.e("error => ${it.message}")
                    }
                }
            }
        }
    }

    private fun assignImage (load: Int, container: ImageView) {
        Glide.with(this)
            .load(load)
            .into(container)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_ID = "extra_id"
    }
}