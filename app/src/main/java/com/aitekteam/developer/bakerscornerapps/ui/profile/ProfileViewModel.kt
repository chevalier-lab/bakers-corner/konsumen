package com.aitekteam.developer.bakerscornerapps.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.MasterUseCase

class ProfileViewModel(
    private val masterUseCase: MasterUseCase
) : ViewModel() {
    fun getProfile(token: String): LiveData<Resource<User>> =
        masterUseCase.getProfile(token).asLiveData()
}