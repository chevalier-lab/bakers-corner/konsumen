package com.aitekteam.developer.bakerscornerapps.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.getSystemService
import com.aitekteam.developer.bakerscornerapps.MyApplication
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatListener
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.notification.TransactionNotification
import com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.detail.DetailAnnouncementActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.product.DetailTransactionProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.request.DetailTransactionRequestActivity
import com.aitekteam.developer.bakerscornerapps.ui.service.chat.ChatActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ID
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.dateFormat
import com.google.gson.Gson
import io.socket.client.Socket
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber

class NotificationService : Service() {

    // utils
    private val channelName = "Notification Service"
    private val transactionChannelId = "transaction"
    private val announcementChannelId = "announcement"
    private val chatChannelId = "chat"
    private var mSocket: Socket? = null
    private var sharePref: SharedPreferences? = null

    override fun onCreate() {
        super.onCreate()

        //Socket instance
        val app: MyApplication = application as MyApplication
        mSocket = app.getSocket()
        mSocket?.connect()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = applicationContext.getSystemService<NotificationManager>()
            val channel = NotificationChannel("foreground", "Bakers Corner Service", NotificationManager.IMPORTANCE_DEFAULT)
            manager?.createNotificationChannel(channel)

            // create builder
            val notificationBuilder = NotificationCompat.Builder(applicationContext, "foreground")
                .setContentText("Aplikasi sedang berjalan, anda bisa menyembunyikan ini di pengaturan")
                .setSmallIcon(R.drawable.logo_bakers)
                .setAutoCancel(true)
                .setNotificationSilent()
                .build()

            startForeground(3, notificationBuilder)
        }

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        sharePref = getSharedPreferences(AUTH_TOKEN, Context.MODE_PRIVATE)

        // listen transaction
        mSocket?.on("notification/transaction") { args ->
            sharePref?.getString(USER_ID, null).let { userId ->
                val data = args[0] as JSONObject
                val trxData = Gson().fromJson(data.toString(), TransactionNotification::class.java)

                if (userId == trxData.idUser)
                    when (trxData.type) {
                        2 -> displayTransactionNotification(trxData, "lagi diproses")
                        3 -> displayTransactionNotification(trxData, "siap untuk diambil")
                        4 -> displayTransactionNotification(trxData, "telah selesai, yuk kasih feedback :)")
                        5 -> displayTransactionNotification(trxData, "ditolak :(")
                    }
            }
        }

        // listen announcement
        mSocket?.on("notification/announcement") { args ->
            val data = args[0] as JSONObject
            val announcementData = Gson().fromJson(data.toString(), Announcement::class.java)
            displayAnnouncementNotification(announcementData)
        }

        // listen chat
        mSocket?.on("message") { args ->
            val data = args[0] as JSONObject
            val msg = data.getString("msg")
            val time = data.getString("time")
            val isUser = data.getBoolean("isUser")
            if (!isUser) {
                val payload = ChatListener("Admin Baker's Corner", msg, time.dateFormat(), isUser)
                displayChatNotification(payload)
            }
        }

        // listen chat join room
        mSocket?.on("infoRoom") { args ->
            val data = args[0] as JSONObject
            try {
                sharePref?.let {
                    with (sharePref!!.edit()) {
                        putString(Constants.CHAT_ID, data.getString("id"))
                        apply()
                    }

                    with (sharePref!!.edit()) {
                        putString(Constants.CHAT_TOKEN, data.getString("chat_token"))
                        apply()
                    }
                }
            } catch (e: JSONException) {
                Timber.i(e.toString())
            }
        }

        sharePref?.getString(USER_ID, null).let { userId ->
            mSocket?.emit("joinRoom", userId)
        }

        return START_NOT_STICKY
    }

    private fun displayAnnouncementNotification(announcement: Announcement) {
        // pending intent
        val intent = Intent(applicationContext, DetailAnnouncementActivity::class.java).apply {
            putExtra(DetailAnnouncementActivity.EXTRA_DATA, announcement)
        }
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // notification manager
        val manager = applicationContext.getSystemService<NotificationManager>()

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, announcementChannelId)
            .setContentTitle("Ada pengumuman baru nih!")
            .setContentText(announcement.subtitle)
            .setSmallIcon(R.drawable.logo_bakers)
            .setContentIntent(pendingIntent)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(announcementChannelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            manager?.createNotificationChannel(channel)
        }

        val notification = notificationBuilder.build()
        manager?.notify(1, notification)
    }

    private fun displayTransactionNotification(data: TransactionNotification, condition: String) {
        lateinit var intent: Intent

        if (data.orderType == "product")
            intent = Intent(applicationContext, DetailTransactionProductActivity::class.java).apply {
                putExtra(DetailTransactionProductActivity.EXTRA_ID, data.trxId)
            }
        else if (data.orderType == "request")
            intent = Intent(applicationContext, DetailTransactionRequestActivity::class.java).apply {
                putExtra(DetailTransactionRequestActivity.EXTRA_ID, data.trxId)
            }

        // pending intent
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // notification manager
        val manager = applicationContext.getSystemService<NotificationManager>()

        // create a builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, transactionChannelId)
            .setContentTitle("Ada kabar baru nih!")
            .setContentText("Pesanan kamu $condition")
            .setSmallIcon(R.drawable.logo_bakers)
            .setContentIntent(pendingIntent)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(transactionChannelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            manager?.createNotificationChannel(channel)
        }

        val notification = notificationBuilder.build()
        manager?.notify(2, notification)
    }

    private fun displayChatNotification(chat: ChatListener) {
        // pending intent
        val intent = Intent(applicationContext, ChatActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // notification manager
        val manager = applicationContext.getSystemService<NotificationManager>()

        // create builder
        val notificationBuilder = NotificationCompat.Builder(applicationContext, chatChannelId)
            .setContentTitle(chat.username)
            .setContentText(chat.text)
            .setSmallIcon(R.drawable.logo_bakers)
            .setContentIntent(pendingIntent)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(chatChannelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            manager?.createNotificationChannel(channel)
        }

        val notification = notificationBuilder.build()
        manager?.notify(4, notification)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("service status => destroyed")
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        val broadcastIntent = Intent()
        broadcastIntent.apply {
            action = "StartService"
            setClass(this@NotificationService, ReceiverService::class.java)
        }
        sendBroadcast(broadcastIntent)

        Timber.i("service status => removed, starting")

    }
}