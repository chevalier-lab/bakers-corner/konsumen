package com.aitekteam.developer.bakerscornerapps.ui.feed

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import com.aitekteam.developer.bakerscornerapps.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_feed.view.*
import kotlinx.android.synthetic.main.bottom_sheet_transaction.*
import kotlinx.android.synthetic.main.bottom_sheet_transaction.view.btn_submit
import kotlinx.android.synthetic.main.bottom_sheet_transaction.view.radio_all
import kotlinx.android.synthetic.main.bottom_sheet_transaction.view.select_all

class BottomSheetFeed(
    activity: Activity,
    private val status: Int?,
    private val listener: FeedFragment
): BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {
    private var newStatus: Int? = null

    fun showBottomSheet() {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bottom_sheet_feed, transaction_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        when (status) {
            1 -> {
                newStatus = 1
                view.radio_harga_termurah.isChecked = true
            }
            2 -> {
                newStatus = 2
                view.radio_harga_termahal.isChecked = true
            }
            3 -> {
                newStatus = 3
                view.radio_sedang_promo.isChecked = true
            }
            null -> {
                newStatus = null
                view.radio_all.isChecked = true
            }
        }

        selectStatus(view)

        view.btn_submit.setOnClickListener {
            listener.selectStatus(newStatus)
            dismiss()
        }
    }

    private fun selectStatus(view: View) {
        // select status
        view.select_all.setOnClickListener {
            newStatus = null
            view.radio_all.isChecked = true
        }

        view.select_harga_termurah.setOnClickListener {
            newStatus = 1
            view.radio_harga_termurah.isChecked = true
        }

        view.select_harga_termahal.setOnClickListener {
            newStatus = 2
            view.radio_harga_termahal.isChecked = true
        }

        view.select_sedang_promo.setOnClickListener {
            newStatus = 3
            view.radio_sedang_promo.isChecked = true
        }
    }
}