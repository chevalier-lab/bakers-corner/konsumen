package com.aitekteam.developer.bakerscornerapps.ui.home.feedback

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.TransactionService
import okio.IOException
import retrofit2.HttpException

class FeedbackListPagingSource(
    private val token: String,
    private val search: String,
    private val productId: String,
    private val direction: String,
    private val transactionService: TransactionService
) : PagingSource<Int, Feedback>() {

    override fun getRefreshKey(state: PagingState<Int, Feedback>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Feedback> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response =
            transactionService.getFeedbackPaging(token, productId, currentPage, search, direction)
        val responseList = mutableListOf<Feedback>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}