package com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.list

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityFeedbackBinding
import com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.detail.FeedbackDetailActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.convertToDateTime
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_my_feedback.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class FeedbackActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFeedbackBinding
    private val viewModel: FeedbackViewModel by viewModel()

    // utils
    private var updateJob: Job? = null
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusablePagingAdapter<MyFeedback>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFeedbackBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, Constants.AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvFeedback)

        // init UI
        initUI()
    }

    private fun initUI() {
        sharedPref.get(Constants.AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                viewModel.getMyFeedback(token).collect {
                    adapter.submitData(it)
                }
            }
        }

        with(adapter) {
            // loading state
            addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading) {
                    binding.rvFeedback.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.VISIBLE
                } else if (loadState.append.endOfPaginationReached) {
                    // handle if data is empty
                    if (adapter.itemCount < 1) {
                        binding.rvFeedback.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.VISIBLE
                        binding.layoutShimmer.visibility = View.GONE
                    }
                } else {
                    // handle if data is exists
                    binding.rvFeedback.visibility = View.VISIBLE
                    binding.layoutShimmer.visibility = View.GONE

                    // get error
                    val error = when {
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                        else -> null
                    }

                    error?.let {
                        binding.rvFeedback.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        binding.layoutShimmer.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun isEmptyData(isEmpty: Boolean) {
        if (isEmpty) {
            binding.rvFeedback.visibility = View.GONE
            binding.layoutEmpty.visibility = View.VISIBLE
        } else {
            binding.layoutEmpty.visibility = View.GONE
            binding.rvFeedback.visibility = View.VISIBLE
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_my_feedback)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<MyFeedback> {
        override fun initComponent(itemView: View, data: MyFeedback, itemIndex: Int) {
            itemView.tv_comment.text = data.comment
            itemView.tv_tanggal.text = data.date?.let { convertToDateTime(it) }

            when (data.rating) {
                1 -> {
                    assignImage(R.drawable.ic_star_on, itemView.star_1)
                    assignImage(R.drawable.ic_star_off, itemView.star_2)
                    assignImage(R.drawable.ic_star_off, itemView.star_3)
                    assignImage(R.drawable.ic_star_off, itemView.star_4)
                    assignImage(R.drawable.ic_star_off, itemView.star_5)
                }
                2 -> {
                    assignImage(R.drawable.ic_star_on, itemView.star_1)
                    assignImage(R.drawable.ic_star_on, itemView.star_2)
                    assignImage(R.drawable.ic_star_off, itemView.star_3)
                    assignImage(R.drawable.ic_star_off, itemView.star_4)
                    assignImage(R.drawable.ic_star_off, itemView.star_5)
                }
                3 -> {
                    assignImage(R.drawable.ic_star_on, itemView.star_1)
                    assignImage(R.drawable.ic_star_on, itemView.star_2)
                    assignImage(R.drawable.ic_star_on, itemView.star_3)
                    assignImage(R.drawable.ic_star_off, itemView.star_4)
                    assignImage(R.drawable.ic_star_off, itemView.star_5)
                }
                4 -> {
                    assignImage(R.drawable.ic_star_on, itemView.star_1)
                    assignImage(R.drawable.ic_star_on, itemView.star_2)
                    assignImage(R.drawable.ic_star_on, itemView.star_3)
                    assignImage(R.drawable.ic_star_on, itemView.star_4)
                    assignImage(R.drawable.ic_star_off, itemView.star_5)
                }
                5 -> {
                    assignImage(R.drawable.ic_star_on, itemView.star_1)
                    assignImage(R.drawable.ic_star_on, itemView.star_2)
                    assignImage(R.drawable.ic_star_on, itemView.star_3)
                    assignImage(R.drawable.ic_star_on, itemView.star_4)
                    assignImage(R.drawable.ic_star_on, itemView.star_5)
                }
            }
        }

        override fun onItemClicked(itemView: View, data: MyFeedback, itemIndex: Int) {
            startActivity(
                Intent(this@FeedbackActivity, FeedbackDetailActivity::class.java).putExtra(
                    FeedbackDetailActivity.EXTRA_ID,
                    data.id
                )
            )
        }
    }

    private fun assignImage(load: Int, container: ImageView) {
        Glide.with(this@FeedbackActivity)
            .load(load)
            .into(container)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}