package com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.AppService
import retrofit2.HttpException
import java.io.IOException

class AnnouncementPagingSource(
    private val token: String,
    private val appService: AppService
) : PagingSource<Int, Announcement>() {

    override fun getRefreshKey(state: PagingState<Int, Announcement>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Announcement> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = appService.getAnnouncementsPaging(
            token,
            currentPage
        )
        val responseList = mutableListOf<Announcement>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}