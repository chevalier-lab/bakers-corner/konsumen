package com.aitekteam.developer.bakerscornerapps.ui.cart.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase

class CartProductViewModel(
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun getCartProducts(
        token: String,
        page: Int,
        direction: String
    ): LiveData<Resource<ListCartProductResponse>> =
        transactionUseCase.getCartProducts(token, page, direction).asLiveData()

    fun updateCartProduct(token: String, id: String, qty: Int): LiveData<Resource<CartProduct>> =
        transactionUseCase.updateCartProduct(token, id, qty).asLiveData()

    fun deleteCartProduct(token: String, id: String): LiveData<Resource<CartProduct>> =
        transactionUseCase.deleteCartProduct(token, id).asLiveData()
}