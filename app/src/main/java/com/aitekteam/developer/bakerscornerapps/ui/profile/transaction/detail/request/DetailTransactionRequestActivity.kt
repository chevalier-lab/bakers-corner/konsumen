package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.request

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestItem
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityDetailTransactionRequestBinding
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.TransactionActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import kotlinx.android.synthetic.main.item_request_transaction_detail.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class DetailTransactionRequestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailTransactionRequestBinding
    private val viewModel: DetailTransactionRequestViewModel by viewModel()

    // utils
    private var condition: String? = null
    private var totalPriceProduct: Long = 0
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusableAdapter<RequestItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTransactionRequestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvRequest)

        // get extras
        val extra = intent.extras
        if (extra != null) {
            val trxId = extra.getString(EXTRA_ID)
            if (trxId != null) initUI(trxId)
            condition = extra.getString(EXTRA_CONDITION)
        }
    }

    private fun initUI(trxId: String) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getDetailTransaction(token, trxId).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.loading.visibility = View.VISIBLE
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                    }
                    Status.SUCCESS -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.layoutMain.visibility = View.VISIBLE

                        it.data?.let { data ->
                            // set status
                            when (data.status) {
                                0 -> {
                                    binding.btnCancel.visibility = View.VISIBLE
                                    binding.tvStatus.text = getString(R.string.menunggu_konfirmasi)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.grey_dark
                                        )
                                    )
                                }
                                2 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.diproses)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.priceColor
                                        )
                                    )
                                }
                                3 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.siap_untuk_ambil)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.colorPrimary
                                        )
                                    )
                                }
                                4 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.selesai)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.green
                                        )
                                    )
                                }
                                5 -> {
                                    binding.btnCancel.visibility = View.GONE
                                    binding.tvStatus.text = getString(R.string.dibatalkan)
                                    binding.tvStatus.setTextColor(
                                        ContextCompat.getColor(
                                            this,
                                            R.color.red
                                        )
                                    )
                                }
                            }

                            // set total price
                            data.items?.map { product ->
                                totalPriceProduct += product.subTotal!!
                            }

                            // set utils
                            adapter.addData(data.items!!)
                            binding.tvOrderCode.text = data.code
                            binding.tvDate.text = Helpers.convertToDateTime(data.date!!)
                            binding.tvTotalPrice.text =
                                Helpers.changeToRupiah(totalPriceProduct.toDouble())
                            binding.tvServiceFee.text = Helpers.changeToRupiah(0.0)
                            binding.tvTotalPay.text =
                                Helpers.changeToRupiah(data.total?.toDouble()!!)
                        }
                    }
                    Status.ERROR -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        Timber.e("error => ${it.message}")
                    }
                }
            }

            binding.btnCancel.setOnClickListener { cancelTransaction(token, trxId) }
        }
    }

    private fun cancelTransaction(token: String, trxId: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.cancel_trx_disclaimer)
        ) {
            viewModel.cancelTransaction(token, trxId).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        Toast.makeText(this, "Berhasil membatalkan pesanan!", Toast.LENGTH_SHORT)
                            .show()
                        startActivity(Intent(this, TransactionActivity::class.java))
                        finish()
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }.show(supportFragmentManager, "cutomDialog")
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_request_transaction_detail)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<RequestItem> {
        override fun initComponent(itemView: View, data: RequestItem, itemIndex: Int) {
            itemView.tv_product_name.text = data.product?.name
            itemView.tv_qty.text = StringBuilder().append(data.qty).append(" produk")
            itemView.tv_price.text = Helpers.changeToRupiah(data.product?.price?.toDouble()!!)
            itemView.tv_total_price.text = Helpers.changeToRupiah(data.subTotal?.toDouble()!!)
        }

        override fun onItemClicked(itemView: View, data: RequestItem, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (condition != null)
                    startActivity(Intent(this, HostActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        putExtra(HostActivity.EXTRA_MOVE_CART, true)
                    })
                else finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (condition != null)
            startActivity(Intent(this, HostActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                putExtra(HostActivity.EXTRA_MOVE_CART, true)
            })
        else finish()
    }

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_CONDITION = "extra_condition"
    }
}