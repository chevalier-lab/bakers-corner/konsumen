package com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.product

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_check_voucher.*
import kotlinx.android.synthetic.main.bottom_sheet_check_voucher.view.*
import java.util.*

class BottomSheetVoucher(
    private var activity: Activity
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    private var voucherValid = false

    fun showBottomSheet() {
        val view = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_check_voucher, voucher_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        view.et_voucher.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                voucherValid = p0.toString().trim().isNotEmpty()
                inputCheck(view)
            }
        })

        view.btn_submit.setOnClickListener {
            val code = view.et_voucher.text.toString().toUpperCase(Locale.ROOT)
            val listener = activity as BottomSheetListener
            listener.checkVoucher(code)
            dismiss()
        }
    }

    private fun inputCheck(view: View) {
        if (voucherValid) {
            view.btn_submit.isEnabled = true
            view.btn_submit.setBackgroundColor(ContextCompat.getColor(context, R.color.green))
        } else {
            view.btn_submit.isEnabled = false
            view.btn_submit.setBackgroundColor(ContextCompat.getColor(context, R.color.divider))
        }
    }
}