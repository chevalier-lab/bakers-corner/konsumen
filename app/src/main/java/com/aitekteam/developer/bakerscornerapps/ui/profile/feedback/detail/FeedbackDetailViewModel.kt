package com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedbackDetail
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ProductUseCase

class FeedbackDetailViewModel(
    private val productUseCase: ProductUseCase
) : ViewModel() {
    fun getMyFeedbackDetail(token: String, id: String): LiveData<Resource<MyFeedbackDetail>> =
        productUseCase.getMyFeedbackDetail(token, id).asLiveData()
}