package com.aitekteam.developer.bakerscornerapps.ui.service.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.NotificationResponse
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ServiceUseCase

class NotificationViewModel(
    private val serviceUseCase: ServiceUseCase
) : ViewModel() {

    fun getNotifications(
        token: String,
        page: Int,
        direction: String
    ): LiveData<Resource<NotificationResponse>> =
        serviceUseCase.getNotifications(token, page, direction).asLiveData()
}