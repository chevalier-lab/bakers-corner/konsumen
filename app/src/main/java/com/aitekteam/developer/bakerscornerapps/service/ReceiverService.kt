package com.aitekteam.developer.bakerscornerapps.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import timber.log.Timber

class ReceiverService : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.i("Broadcast status : diterima")

        // start service
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context?.startForegroundService(Intent(context, NotificationService::class.java))
        } else {
            context?.startService(Intent(context, NotificationService::class.java))
        }
    }
}