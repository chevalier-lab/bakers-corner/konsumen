package com.aitekteam.developer.bakerscornerapps.ui.profile.request

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityRequestMenuBinding
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.aitekteam.developer.bakerscornerapps.utils.helper.SnackbarUtil
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel

class RequestMenuActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRequestMenuBinding
    private val viewModel: RequestMenuViewModel by viewModel()

    // utils
    private var qtyValid = false
    private var nameValid = false
    private var priceValid = false
    private var descriptionValid = false
    private lateinit var sharedPref: SharedPrefsUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRequestMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)

        // init UI
        initUI()
    }

    private fun initUI() {
        // validate input
        validateInput()

        binding.btnSubmit.setOnClickListener {
            CustomDialogFragment(
                getString(R.string.perhatian),
                getString(R.string.register_disclaimer)
            ) { processData(it) }.show(supportFragmentManager, "cutomDialog")
        }
    }

    private fun validateInput() {
        binding.etName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                nameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etDescription.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                descriptionValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })
        binding.etQty.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                qtyValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })
        binding.etPrice.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                priceValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })
    }

    private fun inputCheck() {
        if (nameValid && descriptionValid && qtyValid && priceValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary2))
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.divider))
        }
    }

    private fun processData(view: View) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            val rawData = JsonObject().apply {
                addProperty("product_name", binding.etName.text.toString())
                addProperty("product_description", binding.etDescription.text.toString())
                addProperty("product_price", binding.etPrice.text.toString().toLong())
                addProperty("product_qty", binding.etQty.text.toString().toLong())
            }

            viewModel.addRequestMenu(token, rawData).observe(this, {
                when (it.status) {
                    Status.LOADING -> {
                        binding.btnSubmit.visibility = View.GONE
                        binding.loading.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        // visible button
                        binding.btnSubmit.visibility = View.VISIBLE
                        binding.loading.visibility = View.GONE

                        // clear text
                        binding.etQty.setText("")
                        binding.etName.setText("")
                        binding.etPrice.setText("")
                        binding.etDescription.setText("")

                        // snackbar
                        SnackbarUtil.withAction(
                            view, getString(R.string.success_request),
                            ContextCompat.getColor(this, R.color.colorPrimary),
                            ContextCompat.getColor(this, R.color.dark),
                            ContextCompat.getColor(this, R.color.white)
                        ) {
                            startActivity(
                                Intent(this, HostActivity::class.java).putExtra(
                                    HostActivity.EXTRA_MOVE_CART,
                                    true
                                )
                            )
                            finish()
                        }
                    }
                    Status.ERROR -> {
                        binding.btnSubmit.visibility = View.VISIBLE
                        binding.loading.visibility = View.GONE
                        SnackbarUtil.withoutAction(
                            view, getString(R.string.fail_request),
                            ContextCompat.getColor(this, R.color.dark),
                            ContextCompat.getColor(this, R.color.white)
                        )
                    }
                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}