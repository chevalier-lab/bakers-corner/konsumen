package com.aitekteam.developer.bakerscornerapps.ui.profile.manage

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityKelolaAkunBinding
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.io.File

class KelolaAkunActivity : AppCompatActivity() {
    private lateinit var binding: ActivityKelolaAkunBinding
    private val viewModel: KelolaAkunViewModel by viewModel()

    companion object {
        private const val GALLERY_IMAGE_REQ_CODE = 102
    }
    private var imgProfileFile: File? = null

    // Utils
    private lateinit var sharedPrefs: SharedPrefsUtil
    private var emailValid = false
    private var fullNameValid = false
    private var phoneNumberValid = false
    private var addressValid = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKelolaAkunBinding.inflate(layoutInflater)

        sharedPrefs = SharedPrefsUtil()
        sharedPrefs.start(this, Constants.AUTH_TOKEN)

        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        initUI()
    }

    private fun initUI () {
        validateInput()

        binding.etEmail.setText(sharedPrefs.get(Constants.USER_EMAIL))
        binding.etFullName.setText(sharedPrefs.get(Constants.USER_NAME))
        binding.etPhoneNumber.setText(sharedPrefs.get(Constants.USER_PHONE))
        binding.etAddress.setText(sharedPrefs.get(Constants.USER_ADDRESS))
        Glide.with(this)
            .load(sharedPrefs.get(Constants.USER_MEDIA))
            .into(binding.ivProfileImage)

        binding.btnSubmit.setOnClickListener {
            CustomDialogFragment(
                getString(R.string.perhatian),
                getString(R.string.update_profile_disclaimer)
            ) {
                if (imgProfileFile !== null) saveDataWithUpload()
                else saveData(null)
            }.show(supportFragmentManager, "cutomDialog")
        }
        binding.btnChooseFoto.setOnClickListener {
            pickGalleryImage()
        }
    }

    private fun validateInput() {
        binding.etFullName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                fullNameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
                binding.tvName.text = binding.etFullName.text.toString()
            }
        })

        binding.etPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                phoneNumberValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })


        binding.etEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                emailValid = p0.toString().trim().isNotEmpty()
                inputCheck()
                binding.tvEmail.text = binding.etEmail.text.toString()
            }
        })

        binding.etAddress.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                addressValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })
    }

    private fun inputCheck() {
        if (emailValid && fullNameValid && phoneNumberValid && addressValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary2))
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.divider))
        }
    }

    private fun pickGalleryImage() {
        ImagePicker.with(this)
            .cropSquare()
            .galleryOnly()
            .galleryMimeTypes(
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            .maxResultSize(1080, 1920)
            .start(GALLERY_IMAGE_REQ_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.i(data.toString())
        when (resultCode) {
            Activity.RESULT_OK -> {
                // File object will not be null for RESULT_OK
                val file = ImagePicker.getFile(data)!!
                when (requestCode) {
                    GALLERY_IMAGE_REQ_CODE -> {
                        imgProfileFile = file
                        Glide.with(this)
                            .load(file)
                            .into(binding.ivProfileImage)
                    }
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
//                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun saveDataWithUpload () {
        if (imgProfileFile !== null) {
            val image = imgProfileFile!!.asRequestBody()
            val dataImage = MultipartBody.Part.createFormData("photo",  imgProfileFile?.name,  image)

            sharedPrefs.get(Constants.AUTH_TOKEN)?.let { token ->
                viewModel.uploadPhoto(token, dataImage).observe(this, {
                    when (it.status) {
                        Status.LOADING -> {
                            binding.btnSubmit.isEnabled = false
                        }
                        Status.SUCCESS -> {
                            it.data?.let { media ->
                                saveData(media.id)
                            }
                        }
                        Status.ERROR -> {
                            binding.btnSubmit.isEnabled = true
                            Snackbar.make(window.decorView.rootView, "Gagal upload image!", Snackbar.LENGTH_SHORT).show()
                        }
                    }
                })
            }
        }
    }
    private fun saveData (media_id: String?) {
        val payload = JsonObject().apply {
            addProperty("email", binding.etEmail.text.toString())
            addProperty("full_name", binding.etFullName.text.toString())
            addProperty("phone_number", binding.etPhoneNumber.text.toString())
            addProperty("address", binding.etAddress.text.toString())
            if (media_id !== null) addProperty("id_m_medias", Integer.parseInt(media_id.toString()))
        }

        sharedPrefs.get(Constants.AUTH_TOKEN)?.let { token ->
            viewModel.saveDataAkun(token, payload).observe(this, {
                Timber.i(it.toString())
                when (it.status) {
                    Status.LOADING -> {
                        binding.btnSubmit.visibility = View.GONE
                    }
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            sharedPrefs.setString(Constants.AUTH_TOKEN, data.token!!)
                            sharedPrefs.setString(Constants.USER_NAME, data.name!!)
                            sharedPrefs.setString(Constants.USER_EMAIL, data.email!!)
                            sharedPrefs.setString(Constants.USER_PHONE, data.phone_number!!)
                            sharedPrefs.setString(Constants.USER_ADDRESS, data.address!!)
                            sharedPrefs.setString(Constants.USER_MEDIA, data.media?.url!!)
                        }
                        startActivity(
                            Intent(this, HostActivity::class.java).putExtra(
                                HostActivity.EXTRA_MOVE_PROFILE,
                                true
                            )
                        )
                    }
                    Status.ERROR -> {
                        binding.btnSubmit.visibility = View.VISIBLE
                        Snackbar.make(
                            window.decorView.rootView,
                            "Gagal registrasi!",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}