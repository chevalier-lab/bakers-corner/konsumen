package com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.success

import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckout
import com.aitekteam.developer.bakerscornerapps.databinding.ActivitySuccessCheckoutBinding
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.product.DetailTransactionProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.request.DetailTransactionRequestActivity
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers.changeToRupiah

class SuccessCheckoutActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySuccessCheckoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySuccessCheckoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        val extra = intent.extras
        if (extra != null) {
            when (extra.getString(EXTRA_TYPE)) {
                "product" -> {
                    val dataProduct = extra.getParcelable<ProductCheckout>(EXTRA_PRODUCT)
                    dataProduct?.let { initUIProduct(it) }
                }
                "request" -> {
                    val dataRequest = extra.getParcelable<RequestCheckout>(EXTRA_REQUEST)
                    dataRequest?.let { initUIRequest(it) }
                }
            }
        }
    }

    private fun initUIProduct(data: ProductCheckout) {
        binding.tvTotalPrice.text = changeToRupiah(data.total!!.toDouble())

        binding.btnDetail.setOnClickListener {
            finish()
            startActivity(
                Intent(this, DetailTransactionProductActivity::class.java)
                    .putExtra(DetailTransactionProductActivity.EXTRA_ID, data.id)
                    .putExtra(DetailTransactionProductActivity.EXTRA_CONDITION, "current")
            )
        }
    }

    private fun initUIRequest(data: RequestCheckout) {
        binding.tvTotalPrice.text = changeToRupiah(data.total!!.toDouble())

        binding.btnDetail.setOnClickListener {
            finish()
            startActivity(
                Intent(this, DetailTransactionRequestActivity::class.java)
                    .putExtra(DetailTransactionRequestActivity.EXTRA_ID, data.id)
                    .putExtra(DetailTransactionRequestActivity.EXTRA_CONDITION, "current")
            )
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this, HostActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            putExtra(HostActivity.EXTRA_MOVE_CART, true)
        })
    }

    companion object {
        const val EXTRA_TYPE = "extra_type"
        const val EXTRA_PRODUCT = "extra_product"
        const val EXTRA_REQUEST = "extra_request"
    }
}