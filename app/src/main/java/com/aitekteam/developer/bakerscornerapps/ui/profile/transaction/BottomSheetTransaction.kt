package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_transaction.*
import kotlinx.android.synthetic.main.bottom_sheet_transaction.view.*

class BottomSheetTransaction(
    activity: Activity,
    private val status: Int?,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    private var newStatus: Int? = null

    fun showBottomSheet() {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bottom_sheet_transaction, transaction_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        when (status) {
            0 -> {
                newStatus = 0
                view.radio_wait_confirm.isChecked = true
            }
            2 -> {
                newStatus = 2
                view.radio_process.isChecked = true
            }
            3 -> {
                newStatus = 3
                view.radio_ready_taken.isChecked = true
            }
            4 -> {
                newStatus = 4
                view.radio_success.isChecked = true
            }
            5 -> {
                newStatus = 5
                view.radio_failed.isChecked = true
            }
            null -> {
                newStatus = null
                view.radio_all.isChecked = true
            }
        }

        selectStatus(view)

        view.btn_submit.setOnClickListener {
            listener.selectStatus(newStatus)
            dismiss()
        }
    }

    private fun selectStatus(view: View) {
        // select status
        view.select_all.setOnClickListener {
            newStatus = null
            view.radio_all.isChecked = true
        }

        view.select_wait_confirm.setOnClickListener {
            newStatus = 0
            view.radio_wait_confirm.isChecked = true
        }

        view.select_process.setOnClickListener {
            newStatus = 2
            view.radio_process.isChecked = true
        }

        view.select_ready_taken.setOnClickListener {
            newStatus = 3
            view.radio_ready_taken.isChecked = true
        }

        view.select_success.setOnClickListener {
            newStatus = 4
            view.radio_success.isChecked = true
        }

        view.select_failed.setOnClickListener {
            newStatus = 5
            view.radio_failed.isChecked = true
        }
    }
}