package com.aitekteam.developer.bakerscornerapps.ui.home.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ProductUseCase
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase

class DetailProductViewModel(
    private val productUseCase: ProductUseCase,
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun detailProduct(token: String, id: String): LiveData<Resource<Product>> =
        productUseCase.getProductDetail(token, id).asLiveData()

    fun recommendedProduct(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): LiveData<Resource<ProductsResponse>> =
        productUseCase.recomendedProducts(token, page, search, direction).asLiveData()

    fun getWishlist(
        token: String,
        id: String
    ): LiveData<Resource<Wishlist>> =
        productUseCase.getOnceWishlist(token, id).asLiveData()

    fun addToWishlist(token: String, id: String): LiveData<Resource<Wishlist>> =
        productUseCase.addToWishlist(token, id).asLiveData()

    fun deleteWishlist(token: String, id: String): LiveData<Resource<Wishlist>> =
        productUseCase.deleteWishlist(token, id).asLiveData()

    fun addToCartProduct(token: String, id: String, qty: Int): LiveData<Resource<CartProduct>> =
        transactionUseCase.addToCartProduct(token, id, qty).asLiveData()

    fun getFeedback(
        token: String,
        id: String,
        page: Int,
        search: String,
        direction: String
    ): LiveData<Resource<List<Feedback>>> =
        transactionUseCase.getFeedback(token, id, page, search, direction).asLiveData()

}