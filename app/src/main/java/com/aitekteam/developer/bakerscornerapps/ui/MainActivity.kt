package com.aitekteam.developer.bakerscornerapps.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.ui.auth.login.AuthActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        object : Thread() {
            override fun run() {
                try {
                    sleep(3000)
                    startActivity(Intent(this@MainActivity, AuthActivity::class.java))
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }.start()
    }
}