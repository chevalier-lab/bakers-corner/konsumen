package com.aitekteam.developer.bakerscornerapps.ui.service.notification

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.Notification
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.DiffUtilAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityNotificationBinding
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.paginate.Paginate
import kotlinx.android.synthetic.main.item_notification.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class NotificationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNotificationBinding
    private val viewModel: NotificationViewModel by viewModel()

    // utils
    private var page = 0
    private var totalData = 0
    private var loading = false
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: DiffUtilAdapter<Notification>
    private lateinit var tempDataList: MutableList<Notification>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        tempDataList = mutableListOf()
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = DiffUtilAdapter(this)

        // setup adapter
        setupAdapter(binding.rvNotification)

        // init UI
        initUI()

        // handle paging
        Paginate.with(binding.rvNotification, pagingCallback)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .build()
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getNotifications(token, page, "DESC").observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.layoutError.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.rvNotification.visibility = View.GONE
                        binding.shimmerNotification.rootLayout.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.layoutError.visibility = View.GONE
                        binding.shimmerNotification.rootLayout.visibility = View.GONE

                        it.data?.let { res ->
                            // set total data
                            totalData = res.meta?.size?.total!!

                            res.data?.let { items ->
                                if (items.isEmpty()) {
                                    binding.rvNotification.visibility = View.GONE
                                    binding.layoutEmpty.visibility = View.VISIBLE
                                } else {
                                    tempDataList.addAll(items)
                                    adapter.setData(items)
                                    binding.layoutEmpty.visibility = View.GONE
                                    binding.rvNotification.visibility = View.VISIBLE
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        binding.layoutError.visibility = View.VISIBLE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.rvNotification.visibility = View.GONE
                        binding.shimmerNotification.rootLayout.visibility = View.GONE
                        Timber.e("error => ${it.message}")
                    }
                }
            }
        }
    }

    private val pagingCallback = object : Paginate.Callbacks {
        override fun onLoadMore() {
            page++
            loading = true
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                viewModel.getNotifications(token, page, "DESC").observe(this@NotificationActivity) {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            it.data?.let { res ->
                                res.data?.let { items ->
                                    if (items.isNotEmpty())
                                        tempDataList.addAll(items)
                                        adapter.setData(tempDataList)
                                }

                                loading = false
                            }
                        }
                        Status.ERROR -> {
                        }
                    }
                }
            }
        }

        override fun isLoading(): Boolean {
            return loading
        }

        override fun hasLoadedAllItems(): Boolean {
            return adapter.itemCount == totalData
        }

    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_notification)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Notification> {
        override fun initComponent(itemView: View, data: Notification, itemIndex: Int) {
            itemView.tv_title.text = data.title
            itemView.tv_content.text = data.content
            itemView.tv_time.text = Helpers.convertToDateTime(data.date!!)
        }

        override fun onItemClicked(itemView: View, data: Notification, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}