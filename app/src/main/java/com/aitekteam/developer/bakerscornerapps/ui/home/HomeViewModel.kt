package com.aitekteam.developer.bakerscornerapps.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.Banner
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.Setting
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ProductDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.MasterUseCase
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ProductUseCase
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ServiceUseCase
import com.google.gson.JsonObject

class HomeViewModel(
    private val masterUseCase: MasterUseCase,
    private val serviceUseCase: ServiceUseCase,
    private val productUseCase: ProductUseCase
) : ViewModel() {

    fun getCategories(token: String): LiveData<Resource<List<Category>>> =
        masterUseCase.getCategories(token).asLiveData()

    fun getBanners(token: String): LiveData<Resource<List<Banner>>> =
        serviceUseCase.getBanners(token).asLiveData()

    fun activeAnnouncement(token: String): LiveData<Resource<Announcement>> =
        serviceUseCase.activeAnnouncements(token).asLiveData()

    fun getSetting(token: String): LiveData<Resource<Setting>> =
        serviceUseCase.getSetting(token).asLiveData()

    fun changeVersionCode(token: String, jsonObject: JsonObject): LiveData<Resource<Setting>> =
        serviceUseCase.changeVersionCode(token, jsonObject).asLiveData()

    fun getproducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): LiveData<Resource<List<ProductDomain>>> =
        productUseCase.getProducts(token, page, search, direction).asLiveData()

    fun recomendedProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): LiveData<Resource<ProductsResponse>> =
        productUseCase.recomendedProducts(token, page, search, direction).asLiveData()

    fun getFeedbackCheck(token: String): LiveData<Resource<String?>> =
        productUseCase.getFeedbackCheck(token).asLiveData()
}