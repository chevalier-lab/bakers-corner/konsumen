package com.aitekteam.developer.bakerscornerapps.ui.cart

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentCartBinding
import com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist.WishlistActivity
import com.aitekteam.developer.bakerscornerapps.utils.adapter.SectionCartAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_host.*

class CartFragment : Fragment() {
    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(layoutInflater, container, false)

        // toolbar
        setHasOptionsMenu(true)
        activity?.et_search?.setText("")
        activity?.toolbar_title?.text = getString(R.string.cart)
        activity?.search_bar?.visibility = View.GONE
        activity?.layout_filter?.visibility = View.GONE
        activity?.toolbar?.visibility = View.VISIBLE

        val adapter = SectionCartAdapter(parentFragmentManager, lifecycle)
        binding.viewPager.apply {
            this.adapter = adapter
//            this.setPageTransformer(DepthPageTransformer())
        }

        val title = listOf("Produk", "Request Menu")
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, pos ->
            tab.text = title[pos]
        }.attach()

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_cart, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.favorite -> {
                startActivity(Intent(requireContext(), WishlistActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}