package com.aitekteam.developer.bakerscornerapps.utils.helper

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

class SharedPrefsUtil {
    private var sharePref: SharedPreferences? = null

    fun start(activity: Activity, PREFS: String) {
        sharePref = activity.getSharedPreferences(PREFS, Context.MODE_PRIVATE)
    }

    fun setString(PREFS: String, value: String) {
        this.sharePref?.let {
            with(it.edit()) {
                putString(PREFS, value)
                apply()
            }
        }
    }

    fun get(PREFS: String): String? {
        if (sharePref != null) return sharePref!!.getString(PREFS, null)
        return null
    }

    fun clear() {
        this.sharePref?.edit()?.clear()?.apply()
    }

    fun destroy() { this.sharePref = null }
}