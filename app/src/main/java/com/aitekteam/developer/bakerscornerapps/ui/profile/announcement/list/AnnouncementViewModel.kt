package com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.AppService
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ServiceUseCase
import com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.list.FeedbackPagingSource
import kotlinx.coroutines.flow.Flow

class AnnouncementViewModel(
    private val appService: AppService
) : ViewModel() {

    fun getAnnouncements(token: String): Flow<PagingData<Announcement>> = Pager(PagingConfig(12)) {
        AnnouncementPagingSource(token, appService)
    }.flow.cachedIn(viewModelScope)
}