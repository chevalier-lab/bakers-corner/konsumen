package com.aitekteam.developer.bakerscornerapps.ui.home.detail

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_add_cart.*
import kotlinx.android.synthetic.main.bottom_sheet_add_cart.view.*

class BottomSheetAddCart(
    activity: Activity,
    private val productId: String,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    private var qty = 1

    fun showBottomSheet() {
        val view = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_add_cart, add_cart_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        // set default qty
        view.tv_qty.text = qty.toString()

        // min button
        view.btn_min.setOnClickListener {
            qty -= 1

            if (qty == 0) qty = 1
            if (qty == 1) {
                Glide.with(view)
                    .load(R.drawable.ic_min_disable)
                    .into(view.btn_min)
            }
            else {
                Glide.with(view)
                    .load(R.drawable.ic_min)
                    .into(view.btn_min)
            }

            view.tv_qty.text = qty.toString()
        }

        // plus button
        view.btn_plus.setOnClickListener {
            qty += 1

            if (qty > 1) {
                Glide.with(view)
                    .load(R.drawable.ic_min)
                    .into(view.btn_min)
            }
            else {
                Glide.with(view)
                    .load(R.drawable.ic_min_disable)
                    .into(view.btn_min)
            }

            view.tv_qty.text = qty.toString()
        }

        view.btn_submit.setOnClickListener {
            listener.addToCart(qty, productId)
            dismiss()
        }
    }

}