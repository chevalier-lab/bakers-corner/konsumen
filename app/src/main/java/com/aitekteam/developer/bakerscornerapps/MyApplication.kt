package com.aitekteam.developer.bakerscornerapps

import android.app.Application
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.aitekteam.developer.bakerscornerapps.core.di.dataSourceModule
import com.aitekteam.developer.bakerscornerapps.core.di.databaseModule
import com.aitekteam.developer.bakerscornerapps.core.di.networkModule
import com.aitekteam.developer.bakerscornerapps.core.di.repositoryModule
import com.aitekteam.developer.bakerscornerapps.di.useCaseModule
import com.aitekteam.developer.bakerscornerapps.di.viewModelModule
import com.aitekteam.developer.bakerscornerapps.service.NotificationWorker
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber
import java.net.URISyntaxException

class MyApplication : Application() {
    private var mSocket: Socket? = null
    private var socketServer: String = "http://213.190.4.40:2500"
    private val applicationScope = CoroutineScope(Dispatchers.Default)

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@MyApplication)
            modules(
                listOf(
                    databaseModule,
                    networkModule,
                    dataSourceModule,
                    repositoryModule,
                    useCaseModule,
                    viewModelModule
                )
            )
        }

        applicationScope.launch { setupWorker() }

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())

        connectSocket()
    }

    private fun connectSocket() {
        try {
            mSocket = IO.socket(socketServer)
            mSocket?.connect()
            val options = IO.Options()
            options.reconnection = true
            options.forceNew = true
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    fun getSocket(): Socket? {
        return mSocket
    }

    fun disconnectSocket() {
        mSocket?.disconnect()
    }

    private fun setupWorker() {
        // create task
        val task = OneTimeWorkRequest.Builder(NotificationWorker::class.java).build()
        val workManager = WorkManager.getInstance(applicationContext)
        workManager.enqueue(task)
    }
}