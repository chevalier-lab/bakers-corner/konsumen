package com.aitekteam.developer.bakerscornerapps.ui.home

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import com.aitekteam.developer.bakerscornerapps.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_feedback_home.*
import kotlinx.android.synthetic.main.bottom_sheet_feedback_home.view.*

class BottomSheetFeedback(
    activity: Activity,
    private val listener: HomeFragment
): BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    fun showBottomSheet(data: String?) {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bottom_sheet_feedback_home, transaction_container)
        setContentView(view)
        show()

        if (data == null)
            view.tv_count_review.visibility = View.GONE
        else {
            view.tv_count_review.text = StringBuilder().apply {
                append("Ada ")
                append(data)
                append(" transaksi yang belum kamu ulas nih")
            }
        }

        view.btn_to_ulasan.setOnClickListener {
            cancel()
            listener.toFeedback()
        }

        view.btn_swipe_sheet.setOnClickListener {
            dismiss()
        }
    }
}