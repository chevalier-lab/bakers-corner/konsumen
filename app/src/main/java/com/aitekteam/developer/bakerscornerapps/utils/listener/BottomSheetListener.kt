package com.aitekteam.developer.bakerscornerapps.utils.listener

import com.google.android.material.bottomsheet.BottomSheetDialog

interface BottomSheetListener {

    fun checkVoucher(code: String) {}

    fun selectStatus(status: Int?) {}

    fun addToCart(qty: Int, productId: String) {}

    fun giveFeedback(trxId: String, rating: Int, comment: String, bottomSheet: BottomSheetDialog) {}
}