package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.request

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequest
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.TransactionService
import kotlinx.coroutines.flow.Flow

class TransactionRequestViewModel(
    private val transactionService: TransactionService
) : ViewModel() {

    fun getListTransaction(
        token: String,
        search: String,
        direction: String,
        status: Int?
    ): Flow<PagingData<TransactionRequest>> = Pager(PagingConfig(12)) {
        TransactionRequestPagingSource(token, search, direction, status, transactionService)
    }.flow.cachedIn(viewModelScope)
}