package com.aitekteam.developer.bakerscornerapps.ui.profile.manage

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.MasterUseCase
import com.google.gson.JsonObject
import okhttp3.MultipartBody

class KelolaAkunViewModel (
    private val masterUseCase: MasterUseCase
) : ViewModel() {
    fun uploadPhoto(token: String, photo: MultipartBody.Part): LiveData<Resource<Media>> =
        masterUseCase.createMedia(token, photo).asLiveData()

    fun saveDataAkun(token: String, jsonObject: JsonObject): LiveData<Resource<User>> =
        masterUseCase.updateProfile(token, jsonObject).asLiveData()
}