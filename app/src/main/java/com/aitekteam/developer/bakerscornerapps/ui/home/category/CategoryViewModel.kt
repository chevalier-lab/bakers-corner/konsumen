package com.aitekteam.developer.bakerscornerapps.ui.home.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.MasterUseCase
import kotlinx.coroutines.flow.Flow

class CategoryViewModel(
    private val masterUseCase: MasterUseCase,
    private val productService: ProductService
) : ViewModel() {

    fun getCategories(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): LiveData<Resource<List<Category>>> =
        masterUseCase.getCategoriesFilter(token, page, search, direction).asLiveData()

    fun getProductsCategory(token: String, categoryId: String): Flow<PagingData<Product>> = Pager(
        PagingConfig(12)
    ) {
        CategoryPagingSource(token, categoryId, productService)
    }.flow.cachedIn(viewModelScope)
}