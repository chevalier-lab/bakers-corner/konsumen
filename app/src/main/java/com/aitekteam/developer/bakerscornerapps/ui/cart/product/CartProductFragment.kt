package com.aitekteam.developer.bakerscornerapps.ui.cart.product

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.DiffUtilAdapter
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentCartProductBinding
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.product.CheckoutProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import com.paginate.Paginate
import kotlinx.android.synthetic.main.item_cart_product.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class CartProductFragment : Fragment() {
    private var _binding: FragmentCartProductBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CartProductViewModel by viewModel()

    // utils
    private var page = 0
    private var totalData = 0
    private var loading = false
    private var totalPriceCart: Long = 0
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: DiffUtilAdapter<CartProduct>
    private lateinit var tempDataList: MutableList<CartProduct>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartProductBinding.inflate(inflater, container, false)

        // init utils
        tempDataList = mutableListOf()
        sharedPref = SharedPrefsUtil()
        sharedPref.start(requireActivity(), AUTH_TOKEN)
        adapter = DiffUtilAdapter(requireContext())

        // setup adapter
        setupAdapter(binding.rvCart)

        // init UI
        initUI()

        // handle paging
        Paginate.with(binding.rvCart, pagingCallback)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .build()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getCartProducts(token, page, "DESC").observe(viewLifecycleOwner) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.rvCart.visibility = View.GONE
                        binding.bottomCart.layoutBottom.visibility = View.GONE
                        binding.shimmerCart.rootLayout.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.rvCart.visibility = View.VISIBLE
                        binding.bottomCart.layoutBottom.visibility = View.VISIBLE
                        binding.shimmerCart.rootLayout.visibility = View.GONE

                        it.data?.let { res ->
                            // set total data
                            totalData = res.meta?.size?.total!!

                            res.data?.let { items ->
                                if (items.isEmpty()) {
                                    binding.rvCart.visibility = View.GONE
                                    binding.layoutEmpty.visibility = View.VISIBLE
                                } else {
                                    tempDataList.addAll(items)
                                    adapter.setData(items)
                                }

                                var totalPrice: Long = 0
                                items.map { cp ->
                                    // check discount
                                    if (cp.product?.discount == "0")
                                        totalPrice += (cp.product?.price!!.times(cp.qty!!.toLong()))
                                    else {
                                        // set new price
                                        val oldPrice = cp.product!!.price!!
                                        val discount = oldPrice * cp.product!!.discount!!.toLong() / 100
                                        val newPrice = oldPrice - discount

                                        // set total price
                                        totalPrice += newPrice.times(cp.qty!!.toLong())
                                    }
                                }

                                totalPriceCart = totalPrice
                                binding.bottomCart.tvTotalPrice.text =
                                    Helpers.changeToRupiah(totalPrice.toDouble())

                                if (items.isEmpty()) {
                                    binding.bottomCart.btnSubmitCheckout.isEnabled = false
                                    binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                        ContextCompat.getColor(
                                            requireContext(),
                                            R.color.divider
                                        )
                                    )
                                } else {
                                    binding.bottomCart.btnSubmitCheckout.isEnabled = true
                                    binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                        ContextCompat.getColor(
                                            requireContext(),
                                            R.color.green
                                        )
                                    )
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        binding.rvCart.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                        binding.bottomCart.layoutBottom.visibility = View.GONE
                        binding.shimmerCart.rootLayout.visibility = View.GONE
                    }
                }
            }
        }

        binding.bottomCart.btnSubmitCheckout.setOnClickListener {
            startActivity(Intent(requireContext(), CheckoutProductActivity::class.java))
        }
    }

    private val pagingCallback = object : Paginate.Callbacks {
        override fun onLoadMore() {
            page++
            loading = true
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                viewModel.getCartProducts(token, page, "DESC").observe(viewLifecycleOwner) {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            it.data?.let { res ->
                                res.data?.let { items ->
                                    if (items.isNotEmpty()) {
                                        tempDataList.addAll(items)
                                        adapter.setData(tempDataList)

                                        var totalPrice: Long = 0
                                        items.map { cp ->
                                            // check discount
                                            if (cp.product?.discount == "0")
                                                totalPrice += (cp.product?.price!!.times(cp.qty!!.toLong()))
                                            else {
                                                // set new price
                                                val oldPrice = cp.product!!.price!!
                                                val discount = oldPrice * cp.product!!.discount!!.toLong() / 100
                                                val newPrice = oldPrice - discount

                                                // set total price
                                                totalPrice += newPrice.times(cp.qty!!.toLong())
                                            }
                                        }

                                        totalPriceCart += totalPrice
                                        binding.bottomCart.tvTotalPrice.text =
                                            Helpers.changeToRupiah(totalPriceCart.toDouble())
                                    }
                                }

                                loading = false
                            }
                        }
                        Status.ERROR -> {
                        }
                    }
                }
            }
        }

        override fun isLoading(): Boolean {
            return loading
        }

        override fun hasLoadedAllItems(): Boolean {
            return adapter.itemCount == totalData
        }

    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_cart_product)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<CartProduct> {
        override fun initComponent(itemView: View, data: CartProduct, itemIndex: Int) {
            // set utils
            val price = data.product?.price?.times(data.qty!!.toLong())
            itemView.tv_product_name.text = data.product?.name
            itemView.tv_qty.text = data.qty

            Glide.with(this@CartProductFragment)
                .load(data.product?.thumbnail?.url)
                .into(itemView.image_product)

            // set discount
            if (data.product!!.discount == "0") {
                itemView.tv_price_before.visibility = View.GONE
                itemView.tv_price.text = Helpers.changeToRupiah(price!!.toDouble())
            }
            else {
                itemView.tv_price_before.visibility = View.VISIBLE

                // set new price
                val oldPrice = data.product!!.price!!
                val discount = oldPrice * data.product!!.discount!!.toLong() / 100
                val newPrice = oldPrice - discount

                // set displayed price
                val priceAfter = newPrice.times(data.qty!!.toLong())
                val priceBefore = oldPrice.times(data.qty!!.toLong())

                // set view
                itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tv_price_before.text = Helpers.changeToRupiah(priceBefore.toDouble())
                itemView.tv_price.text = Helpers.changeToRupiah(priceAfter.toDouble())
            }

            // set button action
            sharedPref.get(AUTH_TOKEN)?.let { token ->
                itemView.btn_delete.setOnClickListener {
                    processDelete(token, data.id!!)
                }

                itemView.btn_min.setOnClickListener {
                    if (data.qty!!.toInt() > 1) {
                        val currentQty = data.qty!!.toInt() - 1
                        viewModel.updateCartProduct(token, data.id!!, currentQty)
                            .observe(viewLifecycleOwner, observer.cart)
                    } else processDelete(token, data.id!!)
                }

                itemView.btn_plus.setOnClickListener {
                    val currentQty = data.qty!!.toInt() + 1
                    viewModel.updateCartProduct(token, data.id!!, currentQty)
                        .observe(viewLifecycleOwner, observer.cart)
                }
            }
        }

        override fun onItemClicked(itemView: View, data: CartProduct, itemIndex: Int) {
            startActivity(
                Intent(requireContext(), DetailProductActivity::class.java).putExtra(
                    DetailProductActivity.EXTRA_ID,
                    data.product?.id
                )
            )
        }

    }

    private fun processDelete(token: String, id: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.delete_cart_disclaimer)
        ) {
            viewModel.deleteCartProduct(token, id)
                .observe(viewLifecycleOwner, observer.cart)
        }.show(parentFragmentManager, "cutomDialog")
    }

    private val observer = object {
        val cart = Observer<Resource<CartProduct>> {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    sharedPref.get(AUTH_TOKEN)?.let { token ->
                        // reset page & total data
                        page = 0
                        totalData = 0
                        totalPriceCart = 0

                        viewModel.getCartProducts(token, page, "DESC")
                            .observe(viewLifecycleOwner) { res ->
                                when (res.status) {
                                    Status.LOADING -> {
                                    }
                                    Status.SUCCESS -> {
                                        res.data?.let { resData ->
                                            // set total data
                                            totalData = resData.meta?.size?.total!!

                                            resData.data?.let { items ->
                                                if (items.isEmpty()) {
                                                    binding.rvCart.visibility = View.GONE
                                                    binding.layoutEmpty.visibility = View.VISIBLE
                                                } else {
                                                    tempDataList.clear()
                                                    tempDataList.addAll(items)
                                                    setupAdapter(binding.rvCart)
                                                    adapter.setData(tempDataList)
                                                }

                                                var totalPrice: Long = 0
                                                items.map { cp ->
                                                    // check discount
                                                    if (cp.product?.discount == "0")
                                                        totalPrice += (cp.product?.price!!.times(cp.qty!!.toLong()))
                                                    else {
                                                        // set new price
                                                        val oldPrice = cp.product!!.price!!
                                                        val discount = oldPrice * cp.product!!.discount!!.toLong() / 100
                                                        val newPrice = oldPrice - discount

                                                        // set total price
                                                        totalPrice += newPrice.times(cp.qty!!.toLong())
                                                    }
                                                }

                                                totalPriceCart = totalPrice
                                                binding.bottomCart.tvTotalPrice.text =
                                                    Helpers.changeToRupiah(totalPrice.toDouble())

                                                if (items.isEmpty()) {
                                                    binding.bottomCart.btnSubmitCheckout.isEnabled =
                                                        false
                                                    binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                                        ContextCompat.getColor(
                                                            requireContext(),
                                                            R.color.divider
                                                        )
                                                    )
                                                } else {
                                                    binding.bottomCart.btnSubmitCheckout.isEnabled =
                                                        true
                                                    binding.bottomCart.btnSubmitCheckout.setBackgroundColor(
                                                        ContextCompat.getColor(
                                                            requireContext(),
                                                            R.color.green
                                                        )
                                                    )
                                                }
                                            }
                                        }
                                    }
                                    Status.ERROR -> {
                                    }
                                }
                            }
                    }
                }
                Status.ERROR -> {
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}