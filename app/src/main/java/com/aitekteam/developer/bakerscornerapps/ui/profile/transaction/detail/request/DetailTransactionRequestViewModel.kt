package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.request

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestDetail
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase

class DetailTransactionRequestViewModel(
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun getDetailTransaction(token: String, id: String): LiveData<Resource<TransactionRequestDetail>> =
        transactionUseCase.getDetailTransactionRequest(token, id).asLiveData()

    fun cancelTransaction(token: String, id: String): LiveData<Resource<TransactionProduct>> =
        transactionUseCase.cancelTransactionRequest(token, id).asLiveData()
}