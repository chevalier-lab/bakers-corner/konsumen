package com.aitekteam.developer.bakerscornerapps.ui.service.chat

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatListener

object ChatStorage {
    private var historyChat: MutableList<ChatListener> = mutableListOf()

    fun addHistoryChat(data: ChatListener) {
        historyChat.add(data)
    }
    fun assignHistoryChat(data: MutableList<ChatListener>) {
        historyChat = data
    }
    fun getHistoryChat(): List<ChatListener> {
        return historyChat.toList()
    }
    fun clearHistoryChat() {
        historyChat = mutableListOf()
    }
}