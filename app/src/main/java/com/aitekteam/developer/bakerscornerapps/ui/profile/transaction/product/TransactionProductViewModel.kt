package com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.product

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.TransactionService
import kotlinx.coroutines.flow.Flow

class TransactionProductViewModel(
    private val transactionService: TransactionService
) : ViewModel() {

    fun getListTransaction(
        token: String,
        search: String,
        direction: String,
        status: Int?
    ): Flow<PagingData<TransactionProduct>> = Pager(PagingConfig(12)) {
        TransactionProductPagingSource(token, search, direction, status, transactionService)
    }.flow.cachedIn(viewModelScope)
}