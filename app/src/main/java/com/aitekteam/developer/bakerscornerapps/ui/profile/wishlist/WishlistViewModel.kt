package com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ProductUseCase
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase

class WishlistViewModel(
    private val productUseCase: ProductUseCase,
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun getWishlist(token: String, page: Int, direction: String): LiveData<Resource<WishlistResponse>> =
        productUseCase.getWishlist(token, page, direction).asLiveData()

    fun deleteWishlist(token: String, id: String): LiveData<Resource<Wishlist>> =
        productUseCase.deleteWishlist(token, id).asLiveData()

    fun addToCartProduct(token: String, id: String, qty: Int): LiveData<Resource<CartProduct>> =
        transactionUseCase.addToCartProduct(token, id, qty).asLiveData()
}