package com.aitekteam.developer.bakerscornerapps.ui.auth.login

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityAuthBinding
import com.aitekteam.developer.bakerscornerapps.service.ReceiverService
import com.aitekteam.developer.bakerscornerapps.ui.auth.register.RegisterActivity
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ADDRESS
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_EMAIL
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ID
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_NAME
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_PHONE
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.koin.android.viewmodel.ext.android.viewModel

class AuthActivity : AppCompatActivity() {

    private val RC_SIGN_IN: Int = 1
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var binding: ActivityAuthBinding
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mGoogleSignInOptions: GoogleSignInOptions

    // view model
    private val viewModel: AuthViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        // init variables
        firebaseAuth = FirebaseAuth.getInstance()
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)

        // setup auth
        configureGoogleSignIn()

        // button
        binding.btnLogin.setOnClickListener { signIn() }
    }

    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null && sharedPref.get(AUTH_TOKEN) != null) {
            startActivity(getLaunchHome(this))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            binding.btnLogin.showLoading()
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                binding.btnLogin.hideLoading()
                Snackbar.make(
                    window.decorView.rootView,
                    "Google sign in failed!",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    // proses autentikasi
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful)
                userCheck(firebaseAuth.currentUser!!)
            else
                Snackbar.make(
                    window.decorView.rootView,
                    "Google sign in failed!",
                    Snackbar.LENGTH_SHORT
                ).show()

        }
    }

    private fun userCheck(user: FirebaseUser) {
        val email = user.email!!.toRequestBody("text/plain".toMediaTypeOrNull())
        val password = user.uid.toRequestBody("text/plain".toMediaTypeOrNull())

        viewModel.login(email, password).observe(this) { res ->
            when (res.status) {
                Status.LOADING -> {
                    binding.btnLogin.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    res.data?.let { auth ->
                        if (auth.email != null) {
                            auth.id?.let { id ->
                                sharedPref.setString(USER_ID, id)
                            }
                            sharedPref.setString(USER_ID, auth.id!!)
                            sharedPref.setString(AUTH_TOKEN, auth.token!!)
                            sharedPref.setString(USER_NAME, auth.name!!)
                            sharedPref.setString(USER_EMAIL, auth.email!!)
                            sharedPref.setString(USER_PHONE, auth.phone_number!!)
                            sharedPref.setString(USER_ADDRESS, auth.address!!)
                            startActivity(getLaunchHome(this))

                            // send broadcast
                            val broadcastIntent = Intent()
                            broadcastIntent.apply {
                                action = "StartService"
                                setClass(this@AuthActivity, ReceiverService::class.java)
                            }
                            sendBroadcast(broadcastIntent)
                        } else {
                            startActivity(getLaunchRegister(this))
                        }
                        finish()
                    }
                }
                Status.ERROR -> {
                    Snackbar.make(
                        window.decorView.rootView,
                        "Gagal login!",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    binding.btnLogin.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    companion object {
        fun getLaunchHome(from: Context) = Intent(from, HostActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        fun getLaunchRegister(from: Context) = Intent(from, RegisterActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}