package com.aitekteam.developer.bakerscornerapps.ui.home.feedback

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.TransactionService
import kotlinx.coroutines.flow.Flow

class ListFeedbackViewModel(
    private val transactionService: TransactionService
) : ViewModel() {

    fun getFeedback(
        token: String,
        id: String,
        search: String,
        direction: String
    ) : Flow<PagingData<Feedback>> = Pager(PagingConfig(12)) {
        FeedbackListPagingSource(token, search, id, direction, transactionService)
    }.flow.cachedIn(viewModelScope)
}