package com.aitekteam.developer.bakerscornerapps.ui.home.recommended

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityRecommendedProductBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class RecommendedProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRecommendedProductBinding
    private val viewModel: RecommendedViewModel by viewModel()

    // utils
    private var updateJob: Job? = null
    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var adapter: ReusablePagingAdapter<Product>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecommendedProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_btn)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvRecommended)

        // init UI
        initUI()
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                viewModel.recomendedProducts(token, "", "ASC").collect {
                    adapter.submitData(it)
                }
            }
        }

        with(adapter) {
            // loading state
            addLoadStateListener { loadState ->
                // handle loading
                if (loadState.refresh is LoadState.Loading) {
                    binding.layoutEmpty.visibility = View.GONE
                    binding.layoutError.visibility = View.GONE
                    binding.rvRecommended.visibility = View.GONE
                    binding.shimmerProduct.rootLayout.visibility = View.VISIBLE
                } else if (loadState.append.endOfPaginationReached) {
                    // handle if data is empty
                    if (adapter.itemCount < 1) {
                        binding.layoutEmpty.visibility = View.VISIBLE
                        binding.rvRecommended.visibility = View.GONE
                        binding.shimmerProduct.rootLayout.visibility = View.GONE
                    }
                } else {
                    // handle if data is exists
                    binding.layoutEmpty.visibility = View.GONE
                    binding.rvRecommended.visibility = View.VISIBLE
                    binding.shimmerProduct.rootLayout.visibility = View.GONE

                    // get error
                    val error = when {
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                        else -> null
                    }

                    error?.let {
                        binding.layoutEmpty.visibility = View.GONE
                        binding.rvRecommended.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_product)
            .isStaggeredGridView(2)
            .buildStagged(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Product> {
        override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
            // set utils
            itemView.tv_nama_produk.text = data.name

            // set image
            Glide.with(this@RecommendedProductActivity)
                .load(data.thumbnail?.url)
                .into(itemView.image_product)

            // set status makanan
            val setStatus = fun(text: String, color: Int) {
                itemView.tv_status.text = StringBuilder().append(text)
                itemView.status.setBackgroundResource(color)
            }

            when (data.visible) {
                "0" -> setStatus("Habis", R.drawable.status_habis)
                "1" -> setStatus("Ready", R.drawable.status_ready)
            }

            // set discount
            if (data.discount == "0") {
                itemView.layout_discount.visibility = View.GONE
                itemView.tv_harga.text = Helpers.changeToRupiah(data.price!!.toDouble())
            } else {
                itemView.layout_discount.visibility = View.VISIBLE

                // set new price
                val oldPrice = data.price!!
                val discount = oldPrice * data.discount!!.toLong() / 100
                val newPrice = oldPrice - discount

                // set view
                itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tv_discount.text = StringBuilder().append("${data.discount}% OFF")
                itemView.tv_price_before.text = Helpers.changeToRupiah(oldPrice.toDouble())
                itemView.tv_harga.text = Helpers.changeToRupiah(newPrice.toDouble())
            }
        }

        override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
            startActivity(
                Intent(this@RecommendedProductActivity, DetailProductActivity::class.java).putExtra(
                    DetailProductActivity.EXTRA_ID,
                    data.id
                )
            )
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recommended, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.search)?.actionView as SearchView

        with(searchView) {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            queryHint = resources.getString(R.string.search_product)

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    if (p0 != null) {
                        adapter.submitData(lifecycle, PagingData.empty())
                        sharedPref.get(AUTH_TOKEN)?.let { token ->
                            updateJob?.cancel()
                            updateJob = lifecycleScope.launch {
                                viewModel.recomendedProducts(token, p0, "ASC").collect {
                                    adapter.submitData(it)
                                }
                            }
                        }
                    }
                    return true
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    if (p0 != null) {
                        adapter.submitData(lifecycle, PagingData.empty())
                        sharedPref.get(AUTH_TOKEN)?.let { token ->
                            updateJob?.cancel()
                            updateJob = lifecycleScope.launch {
                                viewModel.recomendedProducts(token, p0, "ASC").collect {
                                    adapter.submitData(it)
                                }
                            }
                        }

                    }
                    return true
                }
            })
        }

        menu.findItem(R.id.search)
            .setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
                    adapter.submitData(lifecycle, PagingData.empty())
                    return true
                }

                override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
                    initUI()
                    return true
                }

            })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

