package com.aitekteam.developer.bakerscornerapps.ui.service.chat

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.Chat
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ChatHistoryDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ServiceUseCase
import okhttp3.RequestBody

class ChatViewModel(
    private val serviceUseCase: ServiceUseCase
) : ViewModel() {
    fun postChat(token: String, jsonObject: RequestBody): LiveData<Resource<Chat>> =
        serviceUseCase.postChat(token, jsonObject).asLiveData()

    fun getHistoryChat(token: String, id: String): LiveData<Resource<List<ChatHistoryDomain>>> =
        serviceUseCase.getHistoryChat(token, id).asLiveData()
}