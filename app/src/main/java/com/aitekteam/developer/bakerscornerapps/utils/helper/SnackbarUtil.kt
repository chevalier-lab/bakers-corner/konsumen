package com.aitekteam.developer.bakerscornerapps.utils.helper

import android.view.View
import com.google.android.material.snackbar.Snackbar

@Suppress("SpellCheckingInspection")
object SnackbarUtil {

    fun show(view: View, message: String, actionTextColor: Int, textColor: Int, backgroundColor: Int) {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .setActionTextColor(actionTextColor)
            .setTextColor(textColor)
            .setBackgroundTint(backgroundColor)

        snackbar.setAction("Tutup") {
            snackbar.dismiss()
        }.show()
    }

    fun withAction(view: View, message: String, actionTextColor: Int,
                   textColor: Int, backgroundColor: Int, callback: () -> Unit) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("Lihat") {
                callback()
            }
            .setActionTextColor(actionTextColor)
            .setTextColor(textColor)
            .setBackgroundTint(backgroundColor)
            .show()
    }

    fun withoutAction(view: View, message: String, textColor: Int, backgroundColor: Int) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .setTextColor(textColor)
            .setBackgroundTint(backgroundColor)
            .show()
    }

}