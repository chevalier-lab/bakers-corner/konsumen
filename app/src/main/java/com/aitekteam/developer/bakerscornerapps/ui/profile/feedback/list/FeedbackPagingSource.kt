package com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import retrofit2.HttpException
import java.io.IOException

class FeedbackPagingSource(
    private val token: String,
    private val productService: ProductService
) : PagingSource<Int, MyFeedback>() {

    override fun getRefreshKey(state: PagingState<Int, MyFeedback>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MyFeedback> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = productService.getMyFeedbackPaging(
            token,
            currentPage
        )
        val responseList = mutableListOf<MyFeedback>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}