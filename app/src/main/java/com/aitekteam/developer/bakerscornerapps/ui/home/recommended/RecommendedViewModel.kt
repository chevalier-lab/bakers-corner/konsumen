package com.aitekteam.developer.bakerscornerapps.ui.home.recommended

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import kotlinx.coroutines.flow.Flow

class RecommendedViewModel(
    private val productService: ProductService
) : ViewModel() {

    fun recomendedProducts(
        token: String,
        search: String,
        direction: String
    ): Flow<PagingData<Product>> = Pager(PagingConfig(12)) {
        RecommendedPagingSource(token, search, direction, productService)
    }.flow.cachedIn(viewModelScope)
}