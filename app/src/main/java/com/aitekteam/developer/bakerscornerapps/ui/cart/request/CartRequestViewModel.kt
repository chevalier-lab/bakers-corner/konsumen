package com.aitekteam.developer.bakerscornerapps.ui.cart.request

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase
import com.google.gson.JsonObject

class CartRequestViewModel(
    private val transactionUseCase: TransactionUseCase
) : ViewModel() {

    fun getCartRequest(token: String): LiveData<Resource<List<CartRequest>>> =
        transactionUseCase.getRequestMenu(token).asLiveData()

    fun updateCartRequest(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): LiveData<Resource<CartRequest>> =
        transactionUseCase.updateRequestMenu(token, id, jsonObject).asLiveData()

    fun deleteCartRequest(token: String, id: String): LiveData<Resource<CartRequest>> =
        transactionUseCase.deleteRequestMenu(token, id).asLiveData()
}