package com.aitekteam.developer.bakerscornerapps.ui.home.category

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentCategoryListBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class CategoryListFragment : Fragment() {
    private var _binding: FragmentCategoryListBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CategoryViewModel by viewModel()

    // utils
    private var category = 0
    private var updateJob: Job? = null
    private lateinit var sharedPrefs: SharedPrefsUtil
    private lateinit var adapter: ReusablePagingAdapter<Product>

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCategoryListBinding.inflate(inflater, container, false)
        category = requireArguments().getInt(CATEGORY_ID, 0)
        sharedPrefs = SharedPrefsUtil()
        sharedPrefs.start(requireActivity(), AUTH_TOKEN)

        // init adapter
        adapter = ReusablePagingAdapter(requireContext())

        // setup adapter
        setupAdapter(binding.rvCategoryDetail)

        // init UI
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getProductsCategory(token, category.toString()).collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.layoutEmpty.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.rvCategoryDetail.visibility = View.GONE
                        binding.shimmerProduct.rootLayout.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.layoutEmpty.visibility = View.VISIBLE
                            binding.rvCategoryDetail.visibility = View.GONE
                            binding.shimmerProduct.rootLayout.visibility = View.GONE
                        }
                    } else {
                        // handle if data is exists
                        binding.layoutError.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.rvCategoryDetail.visibility = View.VISIBLE
                        binding.shimmerProduct.rootLayout.visibility = View.GONE

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.layoutEmpty.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                            binding.rvCategoryDetail.visibility = View.GONE
                            binding.shimmerProduct.rootLayout.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_product)
            .isStaggeredGridView(2)
            .buildStagged(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Product> {
        override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
            // set utils
            itemView.tv_nama_produk.text = data.name

            // set image
            Glide.with(requireContext())
                .load(data.thumbnail?.url)
                .into(itemView.image_product)

            // set status makanan
            val setStatus = fun(text: String, color: Int) {
                itemView.tv_status.text = StringBuilder().append(text)
                itemView.status.setBackgroundResource(color)
            }

            when (data.visible) {
                "0" -> setStatus("Habis", R.drawable.status_habis)
                "1" -> setStatus("Ready", R.drawable.status_ready)
            }

            // set discount
            if (data.discount == "0") {
                itemView.layout_discount.visibility = View.GONE
                itemView.tv_harga.text = Helpers.changeToRupiah(data.price!!.toDouble())
            } else {
                itemView.layout_discount.visibility = View.VISIBLE

                // set new price
                val oldPrice = data.price!!
                val discount = oldPrice * data.discount!!.toLong() / 100
                val newPrice = oldPrice - discount

                // set view
                itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tv_discount.text = StringBuilder().append("${data.discount}% OFF")
                itemView.tv_price_before.text = Helpers.changeToRupiah(oldPrice.toDouble())
                itemView.tv_harga.text = Helpers.changeToRupiah(newPrice.toDouble())
            }
        }

        override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
            startActivity(
                Intent(requireContext(), DetailProductActivity::class.java).putExtra(
                    DetailProductActivity.EXTRA_ID,
                    data.id
                )
            )
        }

    }

    companion object {
        const val CATEGORY_ID = "CATEGORY_ID"

        fun newInstance(page: Int): CategoryListFragment {
            val fragment = CategoryListFragment()
            val args = Bundle()
            args.putInt(CATEGORY_ID, page)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}