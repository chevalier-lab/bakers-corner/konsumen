package com.aitekteam.developer.bakerscornerapps.ui.feed

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.bakerscornerapps.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.bakerscornerapps.databinding.FragmentFeedBinding
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductActivity
import com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist.WishlistActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.aitekteam.developer.bakerscornerapps.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_host.*
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class FeedFragment : Fragment(), BottomSheetListener {
    private var _binding: FragmentFeedBinding? = null
    private val binding get() = _binding!!
    private val viewModel: FeedViewModel by viewModel()

    // utils
    private var hideFilter = false
    private var updateJob: Job? = null
    private var currentStatus: Int? = null

    // Filter
    private var direction: String = "ASC"
    private var isPriceOrder: Boolean? = null
    private var isDiscountOrder: Boolean? = null

    private lateinit var sharedPref: SharedPrefsUtil
    private lateinit var productAdapter: ReusablePagingAdapter<Product>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFeedBinding.inflate(layoutInflater, container, false)

        // toolbar
        setHasOptionsMenu(true)
        activity?.et_search?.setText("")
        activity?.toolbar?.visibility = View.GONE
        activity?.search_bar?.visibility = View.VISIBLE
        activity?.layout_filter?.visibility = View.VISIBLE
        activity?.toolbar_title?.text = getString(R.string.feed)
        activity?.tv_status?.text = getString(R.string.semua_produk)

        // init utils
        sharedPref = SharedPrefsUtil()
        sharedPref.start(requireActivity(), AUTH_TOKEN)
        productAdapter = ReusablePagingAdapter(requireContext())

        // setup adapter
        setupAdapter.product(binding.rvFeed)

        // init UI
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getProducts(token, "", direction, isPriceOrder, isDiscountOrder).collect {
                    productAdapter.submitData(it)
                }
            }

            with(productAdapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.shimmerProduct.rootLayout.visibility = View.VISIBLE
                        binding.containerNoData.visibility = View.GONE
                        binding.rvFeed.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (productAdapter.itemCount < 1) {
                            binding.rvFeed.visibility = View.GONE
                            binding.containerNoData.visibility = View.VISIBLE
                            binding.shimmerProduct.rootLayout.visibility = View.GONE
                        }
                    } else {
                        // handle if data is exists
                        binding.rvFeed.visibility = View.VISIBLE
                        binding.layoutError.visibility = View.GONE
                        binding.containerNoData.visibility = View.GONE
                        binding.shimmerProduct.rootLayout.visibility = View.GONE

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvFeed.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                            binding.containerNoData.visibility = View.GONE
                            binding.shimmerProduct.rootLayout.visibility = View.GONE
                        }
                    }
                }
            }


            // search
            activity?.et_search?.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(p0: Editable?) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (view != null) {
                        if (p0?.trim() == "") {
                            productAdapter.submitData(lifecycle, PagingData.empty())
                            productAdapter.notifyDataSetChanged()
                        } else {
                            productAdapter.submitData(lifecycle, PagingData.empty())
                            productAdapter.notifyDataSetChanged()

                            updateJob?.cancel()
                            updateJob = viewLifecycleOwner.lifecycleScope.launch {
                                viewModel.getProducts(
                                    token,
                                    p0.toString(),
                                    direction,
                                    isPriceOrder,
                                    isDiscountOrder
                                ).collect {
                                    productAdapter.submitData(it)
                                }
                            }

                        }
                    }
                }
            })

            // filter
            activity?.btn_filter?.setOnClickListener {
                hideFilter = !hideFilter

                if (hideFilter)
                    activity?.layout_filter?.visibility = View.GONE
                else
                    activity?.layout_filter?.visibility = View.VISIBLE
            }

            activity?.layout_filter?.setOnClickListener {
                BottomSheetFeed(
                    requireActivity(),
                    currentStatus,
                    this
                ).showBottomSheet()
            }
        }
    }

    private val setupAdapter = object {
        fun product(recyclerView: RecyclerView) {
            productAdapter.adapterCallback(adapterCallback.product)
                .setLayout(R.layout.item_product)
                .isStaggeredGridView(2)
                .buildStagged(recyclerView)
        }
    }

    private val adapterCallback = object {
        val product = object : AdapterCallback<Product> {
            override fun initComponent(itemView: View, data: Product, itemIndex: Int) {
                // set utils
                itemView.tv_nama_produk.text = data.name

                // set image
                Glide.with(requireContext())
                    .load(data.thumbnail?.url)
                    .into(itemView.image_product)

                // set status makanan
                val setStatus = fun(text: String, color: Int) {
                    itemView.tv_status.text = StringBuilder().append(text)
                    itemView.status.setBackgroundResource(color)
                }

                when (data.visible) {
                    "0" -> setStatus("Habis", R.drawable.status_habis)
                    "1" -> setStatus("Ready", R.drawable.status_ready)
                }

                // set discount
                if (data.discount == "0") {
                    itemView.layout_discount.visibility = View.GONE
                    itemView.tv_harga.text = Helpers.changeToRupiah(data.price!!.toDouble())
                } else {
                    itemView.layout_discount.visibility = View.VISIBLE

                    // set new price
                    val oldPrice = data.price!!
                    val discount = oldPrice * data.discount!!.toLong() / 100
                    val newPrice = oldPrice - discount

                    // set view
                    itemView.tv_price_before.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                    itemView.tv_discount.text = StringBuilder().append("${data.discount}% OFF")
                    itemView.tv_price_before.text = Helpers.changeToRupiah(oldPrice.toDouble())
                    itemView.tv_harga.text = Helpers.changeToRupiah(newPrice.toDouble())
                }

            }

            override fun onItemClicked(itemView: View, data: Product, itemIndex: Int) {
                startActivity(
                    Intent(requireContext(), DetailProductActivity::class.java).putExtra(
                        DetailProductActivity.EXTRA_ID,
                        data.id
                    )
                )
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_feed, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.favorite -> {
                startActivity(Intent(requireContext(), WishlistActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun selectStatus(status: Int?) {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            currentStatus = status
            val search = activity?.et_search?.text.toString()

            when (status) {
                1 -> {
                    activity?.tv_status?.text = getString(R.string.harga_termurah)
                    direction = "ASC"
                    isPriceOrder = true
                    isDiscountOrder = null
                }
                2 -> {
                    activity?.tv_status?.text = getString(R.string.harga_termahal)
                    direction = "DESC"
                    isPriceOrder = true
                    isDiscountOrder = null
                }
                3 -> {
                    activity?.tv_status?.text = getString(R.string.sedang_promo)
                    direction = "ASC"
                    isPriceOrder = null
                    isDiscountOrder = true
                }
                else -> {
                    activity?.tv_status?.text = getString(R.string.semua_produk)
                    direction = "ASC"
                    isPriceOrder = null
                    isDiscountOrder = null
                }
            }

            // clear data
            productAdapter.submitData(lifecycle, PagingData.empty())
            productAdapter.notifyDataSetChanged()

            updateJob?.cancel()
            updateJob = viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getProducts(token, search, direction, isPriceOrder, isDiscountOrder)
                    .collect {
                        productAdapter.submitData(it)
                    }
            }
        }
    }
}