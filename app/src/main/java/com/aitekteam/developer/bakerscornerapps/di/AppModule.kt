package com.aitekteam.developer.bakerscornerapps.di

import com.aitekteam.developer.bakerscornerapps.core.domain.interactor.*
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.*
import com.aitekteam.developer.bakerscornerapps.ui.auth.login.AuthViewModel
import com.aitekteam.developer.bakerscornerapps.ui.home.HomeViewModel
import com.aitekteam.developer.bakerscornerapps.ui.service.notification.NotificationViewModel
import com.aitekteam.developer.bakerscornerapps.ui.auth.register.RegisterViewModel
import com.aitekteam.developer.bakerscornerapps.ui.home.category.CategoryViewModel
import com.aitekteam.developer.bakerscornerapps.ui.cart.checkout.CheckoutViewModel
import com.aitekteam.developer.bakerscornerapps.ui.cart.product.CartProductViewModel
import com.aitekteam.developer.bakerscornerapps.ui.cart.request.CartRequestViewModel
import com.aitekteam.developer.bakerscornerapps.ui.feed.FeedViewModel
import com.aitekteam.developer.bakerscornerapps.ui.home.detail.DetailProductViewModel
import com.aitekteam.developer.bakerscornerapps.ui.home.feedback.ListFeedbackViewModel
import com.aitekteam.developer.bakerscornerapps.ui.home.recommended.RecommendedViewModel
import com.aitekteam.developer.bakerscornerapps.ui.host.HostViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.ProfileViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.announcement.list.AnnouncementViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.list.FeedbackViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.detail.FeedbackDetailViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.manage.KelolaAkunViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.request.RequestMenuViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.product.DetailTransactionProductViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.detail.request.DetailTransactionRequestViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.product.TransactionProductViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.transaction.request.TransactionRequestViewModel
import com.aitekteam.developer.bakerscornerapps.ui.profile.wishlist.WishlistViewModel
import com.aitekteam.developer.bakerscornerapps.ui.service.chat.ChatViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<AuthUseCase> { AuthInteractor(get()) }
    factory<MasterUseCase> { MasterInteractor(get()) }
    factory<ProductUseCase> { ProductInteractor(get()) }
    factory<ServiceUseCase> { ServiceInteractor(get()) }
    factory<TransactionUseCase> { TransactionInteractor(get()) }
}

val viewModelModule = module {
    viewModel { AuthViewModel(get()) }
    viewModel { FeedViewModel(get()) }
    viewModel { ChatViewModel(get()) }
    viewModel { HostViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { RegisterViewModel(get()) }
    viewModel { KelolaAkunViewModel(get()) }
    viewModel { CartProductViewModel(get()) }
    viewModel { CartRequestViewModel(get()) }
    viewModel { RecommendedViewModel(get()) }
    viewModel { RequestMenuViewModel(get()) }
    viewModel { NotificationViewModel(get()) }
    viewModel { AnnouncementViewModel(get()) }
    viewModel { ListFeedbackViewModel(get()) }
    viewModel { CheckoutViewModel(get(), get()) }
    viewModel { WishlistViewModel(get(), get()) }
    viewModel { CategoryViewModel(get(), get()) }
    viewModel { TransactionRequestViewModel(get()) }
    viewModel { TransactionProductViewModel(get()) }
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { DetailProductViewModel(get(), get()) }
    viewModel { DetailTransactionProductViewModel(get()) }
    viewModel { DetailTransactionRequestViewModel(get()) }
    viewModel { FeedbackViewModel(get()) }
    viewModel { FeedbackDetailViewModel(get()) }
}