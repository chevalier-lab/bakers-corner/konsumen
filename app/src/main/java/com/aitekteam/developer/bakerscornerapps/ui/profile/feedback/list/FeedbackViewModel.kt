package com.aitekteam.developer.bakerscornerapps.ui.profile.feedback.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import kotlinx.coroutines.flow.Flow

class FeedbackViewModel(
    private val productService: ProductService
) : ViewModel() {

    fun getMyFeedback(token: String): Flow<PagingData<MyFeedback>> = Pager(PagingConfig(12)) {
        FeedbackPagingSource(token, productService)
    }.flow.cachedIn(viewModelScope)
}