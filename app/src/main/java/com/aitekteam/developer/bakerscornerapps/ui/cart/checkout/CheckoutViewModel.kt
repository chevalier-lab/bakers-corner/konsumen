package com.aitekteam.developer.bakerscornerapps.ui.cart.checkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.Voucher
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ServiceUseCase
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase

class CheckoutViewModel(
    private val transactionUseCase: TransactionUseCase,
    private val serviceUseCase: ServiceUseCase
) : ViewModel() {

    fun getCartProducts(
        token: String,
        page: Int,
        direction: String
    ): LiveData<Resource<ListCartProductResponse>> =
        transactionUseCase.getCartProducts(token, page, direction).asLiveData()

    fun getCartRequestMenu(token: String): LiveData<Resource<List<CartRequest>>> =
        transactionUseCase.getRequestMenu(token).asLiveData()

    fun checkoutProduct(token: String, voucher: String): LiveData<Resource<ProductCheckout>> =
        transactionUseCase.checkoutProduct(token, voucher).asLiveData()

    fun checkVoucher(token: String, code: String): LiveData<Resource<Voucher>> =
        serviceUseCase.checkVoucher(token, code).asLiveData()

    fun checkoutRequestMenu(token: String): LiveData<Resource<RequestCheckout>> =
        transactionUseCase.checkoutRequestMenu(token).asLiveData()
}