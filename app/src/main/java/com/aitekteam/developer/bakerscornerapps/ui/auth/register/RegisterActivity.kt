package com.aitekteam.developer.bakerscornerapps.ui.auth.register

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aitekteam.developer.bakerscornerapps.R
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.databinding.ActivityRegisterBinding
import com.aitekteam.developer.bakerscornerapps.service.ReceiverService
import com.aitekteam.developer.bakerscornerapps.ui.host.HostActivity
import com.aitekteam.developer.bakerscornerapps.utils.Constants
import com.aitekteam.developer.bakerscornerapps.utils.Constants.AUTH_TOKEN
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_EMAIL
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_ID
import com.aitekteam.developer.bakerscornerapps.utils.Constants.USER_NAME
import com.aitekteam.developer.bakerscornerapps.utils.helper.CustomDialogFragment
import com.aitekteam.developer.bakerscornerapps.utils.helper.SharedPrefsUtil
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private val viewModel: RegisterViewModel by viewModel()

    // utils
    private var nameValid = false
    private var phoneValid = false
    private var addressValid = false
    private lateinit var user: FirebaseAuth
    private lateinit var sharedPref: SharedPrefsUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)

        // init utils
        user = FirebaseAuth.getInstance()
        sharedPref = SharedPrefsUtil()
        sharedPref.start(this, AUTH_TOKEN)

        // change background status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        // setup ui
        setupUI()

        setContentView(binding.root)
    }

    private fun setupUI() {
        validateInput()
        val dataUser = user.currentUser

        dataUser?.let {
            binding.etEmail.setText(it.email)
            binding.etFullName.setText(it.displayName)
        }

        binding.btnSubmit.setOnClickListener { showDialogToProcess() }
    }

    private fun validateInput() {
        binding.etFullName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}


            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                nameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}


            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                phoneValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etAddress.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}


            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                addressValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })
    }

    private fun inputCheck() {
        if (nameValid && phoneValid && addressValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.divider))
        }
    }

    private fun showDialogToProcess() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.register_disclaimer)
        ) { processData() }.show(supportFragmentManager, "cutomDialog")
    }

    private fun processData() {
        val dataRaw = JsonObject().apply {
            addProperty("email", user.currentUser?.email)
            addProperty("full_name", binding.etFullName.text.toString())
            addProperty("password", user.currentUser?.uid)
            addProperty("phone_number", binding.etPhoneNumber.text.toString())
            addProperty("address", binding.etAddress.text.toString())
        }

        viewModel.register(dataRaw).observe(this, {
            when (it.status) {
                Status.LOADING -> {
                    binding.btnSubmit.visibility = View.GONE
                    binding.loading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        data.id?.let { id ->
                            sharedPref.setString(USER_ID, id)
                        }
                        sharedPref.setString(AUTH_TOKEN, data.token!!)
                        sharedPref.setString(USER_NAME, data.name!!)
                        sharedPref.setString(USER_EMAIL, data.email!!)
                        sharedPref.setString(Constants.USER_PHONE, data.phone_number!!)
                        sharedPref.setString(Constants.USER_ADDRESS, data.address!!)
                    }
                    startActivity(getLaunchHome(this))

                    // send broadcast
                    val broadcastIntent = Intent()
                    broadcastIntent.apply {
                        action = "StartService"
                        setClass(this@RegisterActivity, ReceiverService::class.java)
                    }
                    sendBroadcast(broadcastIntent)
                }
                Status.ERROR -> {
                    binding.btnSubmit.visibility = View.VISIBLE
                    binding.loading.visibility = View.GONE
                    Snackbar.make(
                        window.decorView.rootView,
                        "Gagal registrasi!",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    companion object {
        fun getLaunchHome(from: Context) = Intent(from, HostActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}