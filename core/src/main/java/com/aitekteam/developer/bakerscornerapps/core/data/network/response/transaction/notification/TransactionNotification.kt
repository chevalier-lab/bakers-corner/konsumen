package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.notification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionNotification(
    @field:SerializedName("orderType")
    val orderType: String? = null,

    @field:SerializedName("trxId")
    val trxId: String? = null,

    @field:SerializedName("trxCode")
    val trxCode: String? = null,

    @field:SerializedName("idUser")
    val idUser: String? = null,

    @field:SerializedName("type")
    val type: Int? = null
) : Parcelable