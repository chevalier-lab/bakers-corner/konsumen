package com.aitekteam.developer.bakerscornerapps.core.domain.repository

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedbackDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ProductDomain
import kotlinx.coroutines.flow.Flow

interface IProductRepository {

    fun getProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<ProductDomain>>>

    fun recomendedProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<ProductsResponse>>

    fun getProductDetail(token: String, id: String): Flow<Resource<Product>>

    fun addToWishlist(token: String, id: String): Flow<Resource<Wishlist>>

    fun getWishlist(token: String, page: Int, direction: String): Flow<Resource<WishlistResponse>>

    fun getOnceWishlist(token: String, id: String): Flow<Resource<Wishlist>>

    fun deleteWishlist(token: String, id: String): Flow<Resource<Wishlist>>

    fun getFeedbackCheck(token: String): Flow<Resource<String?>>

    fun getMyFeedback(token: String): Flow<Resource<List<MyFeedback>>>

    fun getMyFeedbackDetail(token: String, id: String): Flow<Resource<MyFeedbackDetail>>
}