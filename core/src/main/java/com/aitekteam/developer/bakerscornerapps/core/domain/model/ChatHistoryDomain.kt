package com.aitekteam.developer.bakerscornerapps.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChatHistoryDomain(
    val content: String?,
    val createdAt: String?,
    val updatedAt: String?,
    val idTChatUser: String?,
    val idMUsers: String?
) : Parcelable