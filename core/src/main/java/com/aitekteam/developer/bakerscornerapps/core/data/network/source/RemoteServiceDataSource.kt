package com.aitekteam.developer.bakerscornerapps.core.data.network.source

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.Banner
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.Chat
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatHistory
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.NotificationResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.Setting
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.Voucher
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.AppService
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.RequestBody
import timber.log.Timber

class RemoteServiceDataSource(
    private val appService: AppService
) {

    fun getBanners(token: String): Flow<ApiResponse<List<Banner>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.getBanners(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getAnnouncements(token: String): Flow<ApiResponse<List<Announcement>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.getAnnouncements(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun activeAnnouncement(token: String): Flow<ApiResponse<Announcement>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.activeAnnouncement(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getNotifications(
        token: String,
        page: Int,
        direction: String
    ): Flow<ApiResponse<NotificationResponse>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.getNotifications(token, page, direction)

            when (response.code) {
                200 -> emit(ApiResponse.Success(response))
                else -> emit(ApiResponse.Error(response.message!!))
            }
            EspressoIdlingResource.decrement()

        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getSetting(token: String): Flow<ApiResponse<Setting>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.getSetting(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun changeVersionCode(token: String, jsonObject: JsonObject): Flow<ApiResponse<Setting>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = appService.changeVersionCode(token, jsonObject)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun getVouchers(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<ApiResponse<List<Voucher>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.getVouchers(token, page, search, direction)
            val dataArray = response.data

            dataArray?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun checkVoucher(token: String, code: String): Flow<ApiResponse<Voucher>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.checkVoucher(token, code)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun postChat(token: String, jsonObject: RequestBody): Flow<ApiResponse<Chat>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = appService.postChat(token, jsonObject)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun getHistoryChat(token: String, id: String): Flow<ApiResponse<List<ChatHistory>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = appService.getHistoryChat(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}