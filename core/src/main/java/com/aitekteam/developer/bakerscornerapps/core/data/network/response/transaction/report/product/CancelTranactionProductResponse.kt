package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CancelTranactionProductResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: TransactionProduct? = TransactionProduct()
) : Parcelable
