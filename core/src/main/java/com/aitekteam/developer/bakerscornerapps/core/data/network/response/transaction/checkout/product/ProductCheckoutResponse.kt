package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductCheckoutResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: ProductCheckout? = ProductCheckout()
) : Parcelable
