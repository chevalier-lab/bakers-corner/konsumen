package com.aitekteam.developer.bakerscornerapps.core.data.network.response.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.meta.MetaData
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductsResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<Product>? = listOf(),

    @field:SerializedName("meta")
    val meta: MetaData? = MetaData(),
) : Parcelable