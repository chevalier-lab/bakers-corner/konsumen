package com.aitekteam.developer.bakerscornerapps.core.domain.interactor

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.aitekteam.developer.bakerscornerapps.core.data.repository.MasterRepository
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.MasterUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

class MasterInteractor(
    private val masterRepository: MasterRepository
) : MasterUseCase {

    override fun createMedia(token: String, photo: MultipartBody.Part): Flow<Resource<Media>> =
        masterRepository.createMedia(token, photo)

    override fun getMedia(token: String, id: String): Flow<Resource<Media>> =
        masterRepository.getMedia(token, id)

    override fun getCategories(token: String): Flow<Resource<List<Category>>> =
        masterRepository.getCategories(token)

    override fun getCategoriesFilter(token: String, page: Int, search: String, direction: String): Flow<Resource<List<Category>>> =
        masterRepository.getCategoriesFilter(token, page, search, direction)

    override fun getProfile(token: String): Flow<Resource<User>> =
        masterRepository.getProfile(token)

    override fun updateProfile(token: String, jsonObject: JsonObject): Flow<Resource<User>> =
        masterRepository.updateProfile(token, jsonObject)
}