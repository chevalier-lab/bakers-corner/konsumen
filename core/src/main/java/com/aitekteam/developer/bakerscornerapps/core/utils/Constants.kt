package com.aitekteam.developer.bakerscornerapps.core.utils

object Constants {
    // base url
    const val BASE_URL = "http://213.190.4.40/bc/api/index.php/"
    private const val BASE_URL_CHAT = "http://213.190.4.40/administrator/index.php/services/service/chat/"

    // auth routes
    const val ROUTE_LOGIN = "auth/login"
    const val ROUTE_REGISTER = "auth/registration"

    // product routes
    const val ROUTE_GET_PRODUCTS = "products"
    const val ROUTE_DETAIL_PRODUCTS = "products/detail/{id}"
    const val ROUTE_RECOMENDED_PRODUCTS = "products/recomended"
    const val ROUTE_ADD_FAVORITE = "products/addToWishlist/{id}"
    const val ROUTE_ADD_PRODUCT = "products/addToCart/{id}"

    // master routes
    const val ROUTE_MEDIA = "master-data/medias/create"
    const val ROUTE_CATEGORY = "master-data/categories"

    // service routes
    const val ROUTE_BANNER = "services/promoAndBanners"
    const val ROUTE_ANNOUNCEMENT = "services/announcements"
    const val ROUTE_ACTIVE_ANNOUNCEMENT = "services/announcements/active"
    const val ROUTE_NOTIFICATION = "services/notifications"
    const val ROUTE_GET_WISHLIST = "services/wishlists"
    const val ROUTE_GET_ONCE_WISHLIST = "services/wishlists/get/{id}"
    const val ROUTE_DELETE_WISHLIST = "services/wishlists/delete/{id}"
    const val ROUTE_POST_CHAT = BASE_URL_CHAT + "sendChat"
    const val ROUTE_GET_HISTORY_CHAT = BASE_URL_CHAT + "detailRoomChatUser/{id}"

    // utilities routes
    const val ROUTE_SETTING = "utilities/settings"
    const val ROUTE_CHANGE_VERSION = "utilities/settings/update"
    const val ROUTE_GET_PROFILE = "utilities/profile"
    const val ROUTE_UPDATE_PROFILE = "utilities/profile/update"

    // request menu routes
    const val ROUTE_GET_REQUEST = "requestMenus"
    const val ROUTE_ADD_REQUEST = "requestMenus/addToCart"
    const val ROUTE_UPDATE_REQUEST = "requestMenus/update/{id}"
    const val ROUTE_DELETE_REQUEST = "requestMenus/delete/{id}"
    const val ROUTE_CHECKOUT_REQUEST = "requestMenus/checkout"

    // cart product routes
    const val ROUTE_GET_CART = "services/carts"
    const val ROUTE_UPDATE_CART = "services/carts/update/{id}"
    const val ROUTE_DELETE_CART = "services/carts/delete/{id}"
    const val ROUTE_CHECKOUT_PRODUCT = "services/carts/checkout"

    // voucher route
    const val ROUTE_GET_VOUCHER = "vouchers"
    const val ROUTE_CHECK_VOUCHER = "vouchers/get/{code}"

    // transaction product route
    const val ROUTE_LIST_TRANSACTION_PRODUCT = "transactions"
    const val ROUTE_GET_TRANSACTION_PRODUCT = "transactions/get/{id}"
    const val ROUTE_CANCEL_TRANSACTION_PRODUCT = "transactions/cancel/{id}"

    // transaction request menu route
    const val ROUTE_LIST_TRANSACTION_REQUEST = "transactionsRequestMenu"
    const val ROUTE_GET_TRANSACTION_REQUEST = "transactionsRequestMenu/get/{id}"
    const val ROUTE_CANCEL_TRANSACTION_REQUEST = "transactionsRequestMenu/cancel/{id}"

    // feedback route
    const val ROUTE_ADD_FEEDBACK = "transactions/addFeedback/{id}"
    const val ROUTE_GET_FEEDBACK = "products/feedbacks/{id}"
    const val ROUTE_GET_FEEDBACK_CHECK = "feedbacks/checkFeedback"
    const val ROUTE_GET_MY_FEEDBACK = "feedbacks"
    const val ROUTE_GET_MY_FEEDBACK_DETAIL = "feedbacks/detail/{id}"

}