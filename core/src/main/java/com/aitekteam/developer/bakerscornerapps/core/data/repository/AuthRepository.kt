package com.aitekteam.developer.bakerscornerapps.core.data.repository

import com.aitekteam.developer.bakerscornerapps.core.data.network.source.RemoteAuthDataSource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.Auth
import com.aitekteam.developer.bakerscornerapps.core.domain.repository.IAuthRepository
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody

class AuthRepository(
    private val remoteAuthDataSource: RemoteAuthDataSource
) : IAuthRepository {

    override fun login(email: RequestBody, password: RequestBody): Flow<Resource<Auth>> = flow {
        emit(Resource.loading())
        val response = remoteAuthDataSource.login(email, password)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Auth()))
            is ApiResponse.Error -> emit(Resource.error<Auth>(apiResponse.errorMessage))
        }
    }

    override fun register(jsonObject: JsonObject): Flow<Resource<Auth>> = flow {
        emit(Resource.loading())
        val response = remoteAuthDataSource.register(jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Auth()))
            is ApiResponse.Error -> emit(Resource.error<Auth>(apiResponse.errorMessage))
        }
    }

}