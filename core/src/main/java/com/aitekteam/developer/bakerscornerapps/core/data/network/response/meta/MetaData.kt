package com.aitekteam.developer.bakerscornerapps.core.data.network.response.meta

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MetaData(
    @field:SerializedName("page")
    val page: String? = null,

    @field:SerializedName("search")
    val search: String? = null,

    @field:SerializedName("order-direction")
    val direction: String? = null,

    @field:SerializedName("size")
    val size: MetaSize? = MetaSize()
) : Parcelable
