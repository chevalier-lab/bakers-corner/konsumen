package com.aitekteam.developer.bakerscornerapps.core.data.repository

import android.content.Context
import com.aitekteam.developer.bakerscornerapps.core.data.NetworkBoundResource
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.local.source.LocalProductDataSource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedbackDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.source.RemoteProductDataSource
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ProductDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.repository.IProductRepository
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.AppExecutors
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.Helpers.isOnline
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.Helpers.mapEntityProductToDomain
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.Helpers.mapNetworkProductToEntities
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class ProductRepository(
    private val remoteProductDataSource: RemoteProductDataSource,
    private val localProductDataSource: LocalProductDataSource,
    private val appExecutors: AppExecutors,
    private val ctx: Context
) : IProductRepository {

    override fun getProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<ProductDomain>>> =
        object : NetworkBoundResource<List<ProductDomain>, List<Product>>(appExecutors) {
            override fun loadFromDB(): Flow<List<ProductDomain>> {
                return localProductDataSource.getProducts().map { mapEntityProductToDomain(it) }
            }

            override fun shouldFetch(data: List<ProductDomain>?) = isOnline(ctx)

            override suspend fun createCall(): Flow<ApiResponse<List<Product>>> =
                remoteProductDataSource.getProducts(token, page, search, direction)

            override suspend fun saveCallResult(data: List<Product>) {
                val products = mapNetworkProductToEntities(data)
                localProductDataSource.deleteProduct()
                localProductDataSource.insertProduct(products)
            }
        }.asFlow()

    override fun getProductDetail(token: String, id: String): Flow<Resource<Product>> = flow {
        emit(Resource.loading())
        val response = remoteProductDataSource.getProductDetail(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Product()))
            is ApiResponse.Error -> emit(Resource.error<Product>(apiResponse.errorMessage))
        }
    }

    override fun addToWishlist(token: String, id: String): Flow<Resource<Wishlist>> = flow {
        emit(Resource.loading<Wishlist>())
        val response = remoteProductDataSource.addToWishlist(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Wishlist()))
            is ApiResponse.Error -> emit(Resource.error<Wishlist>(apiResponse.errorMessage))
        }
    }

    override fun getWishlist(token: String, page: Int, direction: String): Flow<Resource<WishlistResponse>> = flow {
        emit(Resource.loading<WishlistResponse>())
        val response = remoteProductDataSource.getWishlist(token, page, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(WishlistResponse()))
            is ApiResponse.Error -> emit(Resource.error<WishlistResponse>(apiResponse.errorMessage))
        }
    }

    override fun getOnceWishlist(token: String, id: String): Flow<Resource<Wishlist>> = flow {
        emit(Resource.loading<Wishlist>())
        val response = remoteProductDataSource.getOnceWishlist(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Wishlist()))
            is ApiResponse.Error -> emit(Resource.error<Wishlist>(apiResponse.errorMessage))
        }
    }

    override fun deleteWishlist(token: String, id: String): Flow<Resource<Wishlist>> = flow {
        emit(Resource.loading<Wishlist>())
        val response = remoteProductDataSource.deleteWishlist(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Wishlist()))
            is ApiResponse.Error -> emit(Resource.error<Wishlist>(apiResponse.errorMessage))
        }
    }

    override fun recomendedProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<ProductsResponse>> = flow {
        emit(Resource.loading<ProductsResponse>())
        val response = remoteProductDataSource.recomendedProducts(token, page, search, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(ProductsResponse()))
            is ApiResponse.Error -> emit(Resource.error<ProductsResponse>(apiResponse.errorMessage))
        }
    }

    override fun getFeedbackCheck(token: String): Flow<Resource<String?>> = flow {
        emit(Resource.loading<String?>())
        val response = remoteProductDataSource.getFeedbackCheck(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success<String?>(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success<String?>(null))
            is ApiResponse.Error -> emit(Resource.error<String?>(apiResponse.errorMessage))
        }
    }

    override fun getMyFeedback(token: String): Flow<Resource<List<MyFeedback>>> = flow {
        emit(Resource.loading<List<MyFeedback>>())
        val response = remoteProductDataSource.getMyFeedback(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<MyFeedback>()))
            is ApiResponse.Error -> emit(Resource.error<List<MyFeedback>>(apiResponse.errorMessage))
        }
    }

    override fun getMyFeedbackDetail(token: String, id: String): Flow<Resource<MyFeedbackDetail>> =
        flow {
            emit(Resource.loading<MyFeedbackDetail>())
            val response = remoteProductDataSource.getMyFeedbackDetail(token, id)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(MyFeedbackDetail()))
                is ApiResponse.Error -> emit(Resource.error<MyFeedbackDetail>(apiResponse.errorMessage))
            }
        }

}