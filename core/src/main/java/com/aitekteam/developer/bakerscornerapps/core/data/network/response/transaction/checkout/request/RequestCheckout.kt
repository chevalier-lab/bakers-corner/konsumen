package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RequestCheckout(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("transaction_code")
    val trxCode: String? = null,

    @field:SerializedName("total")
    val total: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null,

    @field:SerializedName("items")
    val items: List<RequestItem>? = listOf()
) : Parcelable
