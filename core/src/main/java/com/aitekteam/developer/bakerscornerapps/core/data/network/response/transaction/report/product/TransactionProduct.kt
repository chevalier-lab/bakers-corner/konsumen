package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductItem
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionProduct(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("transaction_code")
    val code: String? = null,

    @field:SerializedName("total")
    val total: Long? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("dicount_price")
    val discount: Long? = null,

    @field:SerializedName("is_already_feedback")
    val isAlreadyFeedback: Int? = null,

    @field:SerializedName("item")
    val item: ProductItem? = ProductItem(),

    @field:SerializedName("created_at")
    val date: String? = null
) : Parcelable
