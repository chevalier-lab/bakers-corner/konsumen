package com.aitekteam.developer.bakerscornerapps.core.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ChatEntity
import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ProductEntity

@Database(entities = [ProductEntity::class, ChatEntity::class], version = 1, exportSchema = false)
abstract class BCDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao
    abstract fun chatDao(): ChatDao

}