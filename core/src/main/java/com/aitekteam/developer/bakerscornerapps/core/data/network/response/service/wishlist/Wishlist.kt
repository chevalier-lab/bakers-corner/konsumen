package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Wishlist(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("product")
    val product: Product? = Product(),
) : Parcelable
