package com.aitekteam.developer.bakerscornerapps.core.utils.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import kotlin.properties.Delegates

class DiffUtilAdapter<T>(
    private val context: Context
) : RecyclerView.Adapter<DiffUtilAdapter<T>.ViewHolder>(){

    // utils
    var oldDataList = mutableListOf<T>()
    private var layout by Delegates.notNull<Int>()
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var staggedLayoutManager: StaggeredGridLayoutManager

    // callback
    private lateinit var adapterCallback: AdapterCallback<T>

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemCount(): Int = oldDataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(layout, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        adapterCallback.initComponent(holder.itemView, oldDataList[position], position)
        holder.itemView.setOnClickListener {
            adapterCallback.onItemClicked(it, oldDataList[position], position)
        }
    }

    fun setLayout(layout: Int): DiffUtilAdapter<T> {
        this.layout = layout
        return this
    }

    // append data
    fun setData(newDataList: List<T>) {
        val diffUtil = DiffCallback(oldDataList, newDataList)
        val diffResult = DiffUtil.calculateDiff(diffUtil)
        oldDataList.clear()
        oldDataList.addAll(newDataList)

        diffResult.dispatchUpdatesTo(this)
    }

    fun clearData() {
//        oldDataList.clear()
//        notifyDataSetChanged()

        val newData = mutableListOf<T>()
        val diffUtil = DiffCallback(oldDataList, newData)
        val diffResult = DiffUtil.calculateDiff(diffUtil)
        oldDataList.clear()
        diffResult.dispatchUpdatesTo(this)
    }

    // callback
    fun adapterCallback(adapterCallback: AdapterCallback<T>): DiffUtilAdapter<T> {
        this.adapterCallback = adapterCallback
        return this
    }

    // layout manager
    fun isVerticalView(): DiffUtilAdapter<T> {
        layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL, false
        )
        return this
    }

    fun isHorizontalView(): DiffUtilAdapter<T> {
        layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL, false
        )
        return this
    }
    fun isGridView(spanCount: Int): DiffUtilAdapter<T> {
        layoutManager = GridLayoutManager(
            context,
            spanCount, GridLayoutManager.VERTICAL, false
        )
        return this
    }

    fun isStaggeredGridView(spanCount: Int): DiffUtilAdapter<T> {
        staggedLayoutManager =
            StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL)
        return this
    }

    fun build(recyclerView: RecyclerView): DiffUtilAdapter<T> {
        recyclerView.apply {
            this.adapter = this@DiffUtilAdapter
            this.layoutManager = this@DiffUtilAdapter.layoutManager
        }
        return this
    }

    fun buildStagged(recyclerView: RecyclerView): DiffUtilAdapter<T> {
        recyclerView.apply {
            this.adapter = this@DiffUtilAdapter
            this.layoutManager = this@DiffUtilAdapter.staggedLayoutManager
        }
        return this
    }

    // diff callback
    class DiffCallback<T>(
        private val oldData: List<T>,
        private val newData: List<T>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldData.size
        }

        override fun getNewListSize(): Int {
            return newData.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldData[oldItemPosition] == newData[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldData[oldItemPosition] == newData[newItemPosition]
        }

    }
}