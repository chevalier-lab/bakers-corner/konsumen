package com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Media(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("file_name")
    val name: String? = null,

    @field:SerializedName("url")
    val url: String? = null
) : Parcelable