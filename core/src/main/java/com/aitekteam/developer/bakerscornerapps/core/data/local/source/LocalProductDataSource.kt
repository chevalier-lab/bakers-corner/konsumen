package com.aitekteam.developer.bakerscornerapps.core.data.local.source

import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ProductEntity
import com.aitekteam.developer.bakerscornerapps.core.data.local.room.ProductDao
import kotlinx.coroutines.flow.Flow

class LocalProductDataSource(private val productDao: ProductDao) {

    fun getProducts(): Flow<List<ProductEntity>> = productDao.getProducts()

    suspend fun insertProduct(data: List<ProductEntity>) = productDao.insertProduct(data)

    suspend fun deleteProduct() = productDao.deleteProduct()
}