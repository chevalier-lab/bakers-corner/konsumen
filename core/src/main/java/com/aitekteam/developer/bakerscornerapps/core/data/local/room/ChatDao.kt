package com.aitekteam.developer.bakerscornerapps.core.data.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ChatEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ChatDao {

    @Query("SELECT * FROM chat")
    fun getHistoryChat(): Flow<List<ChatEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertChat(product: List<ChatEntity>)

    @Query("DELETE FROM chat")
    suspend fun deleteChat()
}