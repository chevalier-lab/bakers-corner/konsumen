package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestItem
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionRequestDetail(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("transaction_code")
    val code: String? = null,

    @field:SerializedName("total")
    val total: Long? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("created_at")
    val date: String? = null,

    @field:SerializedName("items")
    val items: List<RequestItem>? = listOf()
) : Parcelable
