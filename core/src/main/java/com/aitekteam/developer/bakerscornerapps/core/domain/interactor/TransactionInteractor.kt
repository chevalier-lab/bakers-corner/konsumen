package com.aitekteam.developer.bakerscornerapps.core.domain.interactor

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.data.repository.TransactionRepository
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.TransactionUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class TransactionInteractor(
    private val transactionRepository: TransactionRepository
) : TransactionUseCase {
    override fun addRequestMenu(
        token: String,
        jsonObject: JsonObject
    ): Flow<Resource<CartRequest>> = transactionRepository.addRequestMenu(token, jsonObject)

    override fun getRequestMenu(token: String): Flow<Resource<List<CartRequest>>> =
        transactionRepository.getRequestMenu(token)

    override fun updateRequestMenu(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<Resource<CartRequest>> = transactionRepository.updateRequestMenu(token, id, jsonObject)

    override fun deleteRequestMenu(token: String, id: String): Flow<Resource<CartRequest>> =
        transactionRepository.deleteRequestMenu(token, id)

    override fun checkoutRequestMenu(token: String): Flow<Resource<RequestCheckout>> =
        transactionRepository.checkoutRequestMenu(token)

    override fun addToCartProduct(
        token: String,
        id: String,
        qty: Int
    ): Flow<Resource<CartProduct>> = transactionRepository.addToCartProduct(token, id, qty)

    override fun getCartProducts(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<ListCartProductResponse>> =
        transactionRepository.getCartProducts(token, page, direction)

    override fun updateCartProduct(
        token: String,
        id: String,
        qty: Int
    ): Flow<Resource<CartProduct>> = transactionRepository.updateCartProduct(token, id, qty)

    override fun deleteCartProduct(token: String, id: String): Flow<Resource<CartProduct>> =
        transactionRepository.deleteCartProduct(token, id)

    override fun checkoutProduct(token: String, voucher: String): Flow<Resource<ProductCheckout>> =
        transactionRepository.checkoutProduct(token, voucher)

    override fun getDetailTransaction(
        token: String,
        id: String
    ): Flow<Resource<TransactionProductDetail>> =
        transactionRepository.getDetailTransaction(token, id)

    override fun cancelTransaction(token: String, id: String): Flow<Resource<TransactionProduct>> =
        transactionRepository.cancelTransaction(token, id)

    override fun getDetailTransactionRequest(
        token: String,
        id: String
    ): Flow<Resource<TransactionRequestDetail>> =
        transactionRepository.getDetailTransactionRequest(token, id)

    override fun cancelTransactionRequest(
        token: String,
        id: String
    ): Flow<Resource<TransactionProduct>> =
        transactionRepository.cancelTransactionRequest(token, id)

    override fun addFeedback(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<Resource<TransactionProductDetail>> =
        transactionRepository.addFeedback(token, id, jsonObject)

    override fun getFeedback(
        token: String,
        id: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<Feedback>>> =
        transactionRepository.getFeedback(token, id, page, search, direction)
}