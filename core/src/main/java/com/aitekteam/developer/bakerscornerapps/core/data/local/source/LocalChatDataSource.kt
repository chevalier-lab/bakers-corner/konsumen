package com.aitekteam.developer.bakerscornerapps.core.data.local.source

import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ChatEntity
import com.aitekteam.developer.bakerscornerapps.core.data.local.room.ChatDao
import kotlinx.coroutines.flow.Flow

class LocalChatDataSource(private val chatDao: ChatDao) {
    fun getHistoryChat(): Flow<List<ChatEntity>> = chatDao.getHistoryChat()

    suspend fun insertChat(data: List<ChatEntity>) = chatDao.insertChat(data)

    suspend fun deleteChat() = chatDao.deleteChat()
}