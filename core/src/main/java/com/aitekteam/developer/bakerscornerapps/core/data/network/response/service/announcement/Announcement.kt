package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Announcement(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("subtitle")
    val subtitle: String? = null,

    @field:SerializedName("content")
    val content: String? = null,

    @field:SerializedName("is_visible")
    val visible: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null
) : Parcelable
