package com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("full_name")
    val name: String? = null,

    @field:SerializedName("phone_number")
    val phone_number: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("token")
    val token: String? = null,

    @field:SerializedName("media")
    val media: Media? = Media()
) : Parcelable