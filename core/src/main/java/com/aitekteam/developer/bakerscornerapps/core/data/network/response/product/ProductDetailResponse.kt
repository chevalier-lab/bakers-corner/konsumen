package com.aitekteam.developer.bakerscornerapps.core.data.network.response.product

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductDetailResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("error")
    val error: List<String>? = listOf(),

    @field:SerializedName("data")
    val data: Product? = Product()
) : Parcelable