package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductItem(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("sub_total")
    val subTotal: Long? = null,

    @field:SerializedName("id_t_transaction_product")
    val idTransaction: String? = null,

    @field:SerializedName("product")
    val product: Product? = Product()
) : Parcelable
