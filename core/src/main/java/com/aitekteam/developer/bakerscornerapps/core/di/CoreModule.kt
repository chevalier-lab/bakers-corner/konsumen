package com.aitekteam.developer.bakerscornerapps.core.di

import androidx.room.Room
import com.aitekteam.developer.bakerscornerapps.core.data.local.room.BCDatabase
import com.aitekteam.developer.bakerscornerapps.core.data.local.source.LocalChatDataSource
import com.aitekteam.developer.bakerscornerapps.core.data.local.source.LocalProductDataSource
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.*
import com.aitekteam.developer.bakerscornerapps.core.data.network.source.*
import com.aitekteam.developer.bakerscornerapps.core.data.repository.*
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.AppExecutors
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.NetworkHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

val databaseModule = module {
    factory { get<BCDatabase>().productDao() }
    factory { get<BCDatabase>().chatDao() }

    single {
        Room.databaseBuilder(
            androidContext(),
            BCDatabase::class.java,
            "BakersCorner.db"
        ).fallbackToDestructiveMigration().build()
    }
}

val networkModule = module {
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .build()
    }

    single { NetworkHelper(get(), AppService::class.java).createService }
    single { NetworkHelper(get(), AuthService::class.java).createService }
    single { NetworkHelper(get(), MasterService::class.java).createService }
    single { NetworkHelper(get(), ProductService::class.java).createService }
    single { NetworkHelper(get(), TransactionService::class.java).createService }
}

val dataSourceModule = module {
    single { LocalProductDataSource(get()) }
    single { LocalChatDataSource(get()) }
    single { RemoteAuthDataSource(get()) }
    single { RemoteMasterDataSource(get()) }
    single { RemoteProductDataSource(get()) }
    single { RemoteServiceDataSource(get()) }
    single { RemoteTransactionDataSource(get()) }
}

val repositoryModule = module {
    factory { AppExecutors() }
    single { AuthRepository(get()) }
    single { MasterRepository(get()) }
    single { ServiceRepository(get(), get(), get(), get()) }
    single { TransactionRepository(get()) }
    single { ProductRepository(get(), get(), get(), get()) }
}
