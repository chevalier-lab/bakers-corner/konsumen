package com.aitekteam.developer.bakerscornerapps.core.data.local.entity

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "products")
data class ProductEntity(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "productId")
    val productId: Int?,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "price")
    val price: Long?,

    @ColumnInfo(name = "discount")
    val discount: Int?,

    @ColumnInfo(name = "cateagory")
    val category: String?,

    @ColumnInfo(name = "image")
    val image: String?,

    @ColumnInfo(name = "visible")
    val visible: String?
) : Parcelable