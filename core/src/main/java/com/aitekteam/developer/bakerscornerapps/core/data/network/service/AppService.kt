package com.aitekteam.developer.bakerscornerapps.core.data.network.service

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedbackResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.ActiveAnnouncementResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.AnnouncementResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.BannerResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatHistoryResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.NotificationResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.SettingResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.SingleVoucherResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.VoucherResponse
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_ACTIVE_ANNOUNCEMENT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_ANNOUNCEMENT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_BANNER
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CHANGE_VERSION
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CHECK_VOUCHER
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_HISTORY_CHAT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_VOUCHER
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_NOTIFICATION
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_POST_CHAT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_SETTING
import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface AppService {

    @GET(ROUTE_BANNER)
    suspend fun getBanners(
        @Header("Authorization") authorization: String
    ): BannerResponse

    @GET(ROUTE_ANNOUNCEMENT)
    suspend fun getAnnouncements(
        @Header("Authorization") authorization: String
    ): AnnouncementResponse

    @GET(ROUTE_ANNOUNCEMENT)
    suspend fun getAnnouncementsPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int
    ): Response<AnnouncementResponse>

    @GET(ROUTE_ACTIVE_ANNOUNCEMENT)
    suspend fun activeAnnouncement(
        @Header("Authorization") authorization: String
    ): ActiveAnnouncementResponse

    @GET(ROUTE_NOTIFICATION)
    suspend fun getNotifications(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("order-direction") direction: String
    ): NotificationResponse

    @GET(ROUTE_SETTING)
    suspend fun getSetting(
        @Header("Authorization") authorization: String
    ): SettingResponse

    @POST(ROUTE_CHANGE_VERSION)
    suspend fun changeVersionCode(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): SettingResponse

    @GET(ROUTE_GET_VOUCHER)
    suspend fun getVouchers(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String
    ) : VoucherResponse

    @GET(ROUTE_CHECK_VOUCHER)
    suspend fun checkVoucher(
        @Header("Authorization") authorization: String,
        @Path("code") code: String
    ) : SingleVoucherResponse

    @POST(ROUTE_POST_CHAT)
    suspend fun postChat(
        @Header("Authorization") authorization: String,
        @Body jsonObject: RequestBody
    ): ChatResponse

    @GET(ROUTE_GET_HISTORY_CHAT)
    suspend fun getHistoryChat(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : ChatHistoryResponse
}