package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ActiveAnnouncementResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: Announcement? = Announcement()
) : Parcelable
