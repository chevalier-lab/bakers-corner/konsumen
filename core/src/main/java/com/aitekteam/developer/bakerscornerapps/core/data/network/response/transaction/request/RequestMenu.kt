package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RequestMenu(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("product_name")
    val name: String? = null,

    @field:SerializedName("product_description")
    val description: String? = null,

    @field:SerializedName("product_price")
    val price: Long? = null,

    @field:SerializedName("product_qty")
    val qty: String? = null,

    @field:SerializedName("is_visible")
    val visible: String? = null
) : Parcelable
