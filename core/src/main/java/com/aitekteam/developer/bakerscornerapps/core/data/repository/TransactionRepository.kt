package com.aitekteam.developer.bakerscornerapps.core.data.repository

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.data.network.source.RemoteTransactionDataSource
import com.aitekteam.developer.bakerscornerapps.core.domain.repository.ITransactionRepository
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class TransactionRepository(
    private val remoteTransactionDataSource: RemoteTransactionDataSource
) : ITransactionRepository {

    override fun addRequestMenu(
        token: String,
        jsonObject: JsonObject
    ): Flow<Resource<CartRequest>> = flow {
        emit(Resource.loading<CartRequest>())
        val response = remoteTransactionDataSource.addRequestMenu(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CartRequest()))
            is ApiResponse.Error -> emit(Resource.error<CartRequest>(apiResponse.errorMessage))
        }
    }

    override fun getRequestMenu(token: String): Flow<Resource<List<CartRequest>>> = flow {
        emit(Resource.loading<List<CartRequest>>())
        val response = remoteTransactionDataSource.getRequestMenu(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<CartRequest>()))
            is ApiResponse.Error -> emit(Resource.error<List<CartRequest>>(apiResponse.errorMessage))
        }
    }

    override fun updateRequestMenu(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<Resource<CartRequest>> = flow {
        emit(Resource.loading<CartRequest>())
        val response = remoteTransactionDataSource.updateRequestMenu(token, id, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CartRequest()))
            is ApiResponse.Error -> emit(Resource.error<CartRequest>(apiResponse.errorMessage))
        }
    }

    override fun deleteRequestMenu(token: String, id: String): Flow<Resource<CartRequest>> = flow {
        emit(Resource.loading<CartRequest>())
        val response = remoteTransactionDataSource.deleteRequestMenu(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CartRequest()))
            is ApiResponse.Error -> emit(Resource.error<CartRequest>(apiResponse.errorMessage))
        }
    }

    override fun checkoutRequestMenu(token: String): Flow<Resource<RequestCheckout>> = flow {
        emit(Resource.loading<RequestCheckout>())
        val response = remoteTransactionDataSource.checkoutRequestMenu(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(RequestCheckout()))
            is ApiResponse.Error -> emit(Resource.error<RequestCheckout>(apiResponse.errorMessage))
        }
    }

    override fun addToCartProduct(
        token: String,
        id: String,
        qty: Int
    ): Flow<Resource<CartProduct>> = flow {
        emit(Resource.loading<CartProduct>())
        val response = remoteTransactionDataSource.addToCartProduct(token, id, qty)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CartProduct()))
            is ApiResponse.Error -> emit(Resource.error<CartProduct>(apiResponse.errorMessage))
        }
    }

    override fun getCartProducts(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<ListCartProductResponse>> = flow {
        emit(Resource.loading<ListCartProductResponse>())
        val response = remoteTransactionDataSource.getCartProducts(token, page, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(ListCartProductResponse()))
            is ApiResponse.Error -> emit(Resource.error<ListCartProductResponse>(apiResponse.errorMessage))
        }
    }

    override fun updateCartProduct(
        token: String,
        id: String,
        qty: Int
    ): Flow<Resource<CartProduct>> = flow {
        emit(Resource.loading<CartProduct>())
        val response = remoteTransactionDataSource.updateCartProduct(token, id, qty)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CartProduct()))
            is ApiResponse.Error -> emit(Resource.error<CartProduct>(apiResponse.errorMessage))
        }
    }

    override fun deleteCartProduct(token: String, id: String): Flow<Resource<CartProduct>> = flow {
        emit(Resource.loading<CartProduct>())
        val response = remoteTransactionDataSource.deleteCartProduct(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CartProduct()))
            is ApiResponse.Error -> emit(Resource.error<CartProduct>(apiResponse.errorMessage))
        }
    }

    override fun checkoutProduct(token: String, voucher: String): Flow<Resource<ProductCheckout>> =
        flow {
            emit(Resource.loading<ProductCheckout>())
            val response = remoteTransactionDataSource.checkoutProduct(token, voucher)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(ProductCheckout()))
                is ApiResponse.Error -> emit(Resource.error<ProductCheckout>(apiResponse.errorMessage))
            }
        }

    override fun getDetailTransaction(
        token: String,
        id: String
    ): Flow<Resource<TransactionProductDetail>> = flow {
        emit(Resource.loading<TransactionProductDetail>())
        val response = remoteTransactionDataSource.getDetailTransaction(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TransactionProductDetail()))
            is ApiResponse.Error -> emit(Resource.error<TransactionProductDetail>(apiResponse.errorMessage))
        }
    }

    override fun cancelTransaction(token: String, id: String): Flow<Resource<TransactionProduct>> =
        flow {
            emit(Resource.loading<TransactionProduct>())
            val response = remoteTransactionDataSource.cancelTransaction(token, id)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(TransactionProduct()))
                is ApiResponse.Error -> emit(Resource.error<TransactionProduct>(apiResponse.errorMessage))
            }
        }

    override fun getDetailTransactionRequest(
        token: String,
        id: String
    ): Flow<Resource<TransactionRequestDetail>> = flow {
        emit(Resource.loading<TransactionRequestDetail>())
        val response = remoteTransactionDataSource.getDetailTransactionRequest(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TransactionRequestDetail()))
            is ApiResponse.Error -> emit(Resource.error<TransactionRequestDetail>(apiResponse.errorMessage))
        }
    }

    override fun cancelTransactionRequest(
        token: String,
        id: String
    ): Flow<Resource<TransactionProduct>> = flow {
        emit(Resource.loading<TransactionProduct>())
        val response = remoteTransactionDataSource.cancelTransactionRequest(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TransactionProduct()))
            is ApiResponse.Error -> emit(Resource.error<TransactionProduct>(apiResponse.errorMessage))
        }
    }

    override fun addFeedback(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<Resource<TransactionProductDetail>> = flow {
        emit(Resource.loading<TransactionProductDetail>())
        val response = remoteTransactionDataSource.addFeedback(token, id, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TransactionProductDetail()))
            is ApiResponse.Error -> emit(Resource.error<TransactionProductDetail>(apiResponse.errorMessage))
        }
    }

    override fun getFeedback(
        token: String,
        id: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<Feedback>>> = flow {
        emit(Resource.loading<List<Feedback>>())
        val response = remoteTransactionDataSource.getFeedback(token, id, page, search, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Feedback>()))
            is ApiResponse.Error -> emit(Resource.error<List<Feedback>>(apiResponse.errorMessage))
        }
    }
}