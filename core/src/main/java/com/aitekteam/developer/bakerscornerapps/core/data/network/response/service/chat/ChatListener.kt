package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChatListener(
    @field:SerializedName("username")
    val username: String? = null,

    @field:SerializedName("text")
    val text: String? = null,

    @field:SerializedName("time")
    val time: String? = null,

    @field:SerializedName("isUser")
    val isUser: Boolean? = null
) : Parcelable
