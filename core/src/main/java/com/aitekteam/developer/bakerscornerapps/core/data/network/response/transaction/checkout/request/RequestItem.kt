package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.RequestMenu
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RequestItem(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("sub_total")
    val subTotal: Long? = null,

    @field:SerializedName("id_t_transaction_request_menu")
    val idTransaction: String? = null,

    @field:SerializedName("product")
    val product: RequestMenu? = RequestMenu()
) : Parcelable
