package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Chat(
    @field:SerializedName("content")
    val content: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("id_t_chat_user")
    val idTChatUser: Int? = null,

    @field:SerializedName("id_m_users")
    val idMUsers: Int? = null
) : Parcelable
