package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChatEmit(
    @field:SerializedName("token")
    val token: String? = null,

    @field:SerializedName("msg")
    val msg: String? = null,

    @field:SerializedName("isUser")
    val isUser: Boolean? = null
) : Parcelable
