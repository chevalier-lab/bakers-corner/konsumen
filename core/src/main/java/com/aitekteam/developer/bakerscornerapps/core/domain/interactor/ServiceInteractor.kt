package com.aitekteam.developer.bakerscornerapps.core.domain.interactor

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.Banner
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.Chat
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.NotificationResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.Setting
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.Voucher
import com.aitekteam.developer.bakerscornerapps.core.data.repository.ServiceRepository
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ChatHistoryDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ServiceUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody

class ServiceInteractor(
    private val serviceRepository: ServiceRepository
) : ServiceUseCase {

    override fun getBanners(token: String): Flow<Resource<List<Banner>>> =
        serviceRepository.getBanners(token)

    override fun getAnnouncements(token: String): Flow<Resource<List<Announcement>>> =
        serviceRepository.getAnnouncements(token)

    override fun activeAnnouncements(token: String): Flow<Resource<Announcement>> =
        serviceRepository.activeAnnouncements(token)

    override fun getNotifications(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<NotificationResponse>> =
        serviceRepository.getNotifications(token, page, direction)

    override fun getSetting(token: String): Flow<Resource<Setting>> =
        serviceRepository.getSetting(token)

    override fun changeVersionCode(token: String, jsonObject: JsonObject): Flow<Resource<Setting>> =
        serviceRepository.changeVersionCode(token, jsonObject)

    override fun getVouchers(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<Voucher>>> = serviceRepository.getVouchers(token, page, search, direction)

    override fun checkVoucher(token: String, code: String): Flow<Resource<Voucher>> =
        serviceRepository.checkVoucher(token, code)

    override fun postChat(token: String, jsonObject: RequestBody): Flow<Resource<Chat>> =
        serviceRepository.postChat(token, jsonObject)

    override fun getHistoryChat(
        token: String,
        id: String
    ): Flow<Resource<List<ChatHistoryDomain>>> =
        serviceRepository.getHistoryChat(token, id)
}