package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.meta.MetaData
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ListCartProductResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<CartProduct>? = listOf(),

    @field:SerializedName("meta")
    val meta: MetaData? = MetaData(),
) : Parcelable