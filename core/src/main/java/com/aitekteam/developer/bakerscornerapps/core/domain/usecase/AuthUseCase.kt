package com.aitekteam.developer.bakerscornerapps.core.domain.usecase

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.Auth
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody

interface AuthUseCase {

    fun login(email: RequestBody, password: RequestBody): Flow<Resource<Auth>>

    fun register(jsonObject: JsonObject): Flow<Resource<Auth>>
}