package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CartProduct(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("qty")
    val qty: String? = null,

    @field:SerializedName("is_visible")
    val visible: String? = null,

    @field:SerializedName("updated_at")
    val date: String? = null,

    @field:SerializedName("product")
    val product: Product? = Product()
) : Parcelable
