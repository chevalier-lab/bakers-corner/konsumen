package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Banner(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("content")
    val content: String? = null,

    @field:SerializedName("is_visible")
    val visible: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null,

    @field:SerializedName("media")
    val media: Media? = Media()
) : Parcelable
