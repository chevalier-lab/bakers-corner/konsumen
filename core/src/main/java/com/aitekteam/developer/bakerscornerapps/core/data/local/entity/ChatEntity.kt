package com.aitekteam.developer.bakerscornerapps.core.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "chat")
data class ChatEntity(
    @PrimaryKey(autoGneerate = true)
    @ColumnInfo(name = "messageId")
    val messageId: Int? = null,

    @ColumnInfo(name = "content")
    val content: String? = null,

    @ColumnInfo(name = "createdAt")
    val createdAt: String? = null,

    @ColumnInfo(name = "updatedAt")
    val updatedAt: String? = null,

    @ColumnInfo(name = "idTChatUser")
    val idTChatUser: String? = null,

    @ColumnInfo(name = "idMUsers")
    val idMUsers: String? = null
) : Parcelable
