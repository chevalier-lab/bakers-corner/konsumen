package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Setting(
    @field:SerializedName("application_name")
    val appName: String? = null,

    @field:SerializedName("is_checkout_available")
    val available: String? = null,

    @field:SerializedName("version_code")
    val versionCode: String? = null,

    @field:SerializedName("media")
    val media: Media? = Media()
) : Parcelable
