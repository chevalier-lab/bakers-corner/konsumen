package com.aitekteam.developer.bakerscornerapps.core.data.repository

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.aitekteam.developer.bakerscornerapps.core.data.network.source.RemoteMasterDataSource
import com.aitekteam.developer.bakerscornerapps.core.domain.repository.IMasterRepository
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody

class MasterRepository(
    private val remoteMasterDataSource: RemoteMasterDataSource
) : IMasterRepository {
    override fun createMedia(token: String, photo: MultipartBody.Part): Flow<Resource<Media>> = flow {
        emit(Resource.loading<Media>())
        val response = remoteMasterDataSource.createMedia(token, photo)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Media()))
            is ApiResponse.Error -> emit(Resource.error<Media>(apiResponse.errorMessage))
        }
    }

    override fun getMedia(token: String, id: String): Flow<Resource<Media>> = flow {
        emit(Resource.loading<Media>())
        val response = remoteMasterDataSource.getMedia(token, id)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Media()))
            is ApiResponse.Error -> emit(Resource.error<Media>(apiResponse.errorMessage))
        }
    }

    override fun getCategories(token: String): Flow<Resource<List<Category>>> = flow {
        emit(Resource.loading<List<Category>>())
        val response = remoteMasterDataSource.getCategories(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Category>()))
            is ApiResponse.Error -> emit(Resource.error<List<Category>>(apiResponse.errorMessage))
        }
    }

    override fun getCategoriesFilter(token: String, page: Int, search: String, direction: String): Flow<Resource<List<Category>>> = flow {
        emit(Resource.loading<List<Category>>())
        val response = remoteMasterDataSource.getCategoriesFilter(token, page, search, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Category>()))
            is ApiResponse.Error -> emit(Resource.error<List<Category>>(apiResponse.errorMessage))
        }
    }

    override fun getProfile(token: String): Flow<Resource<User>> = flow {
        emit(Resource.loading<User>())
        val response = remoteMasterDataSource.getProfile(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(User()))
            is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
        }
    }

    override fun updateProfile(token: String, jsonObject: JsonObject): Flow<Resource<User>> = flow {
        emit(Resource.loading<User>())
        val response = remoteMasterDataSource.updateProfile(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(User()))
            is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
        }
    }
}