package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChatSend(
    @field:SerializedName("content")
    val content: String? = null,

    @field:SerializedName("id_t_chat_user")
    val id_t_chat_user: String? = null,

    @field:SerializedName("id_m_user")
    val id_m_user: String? = null
) : Parcelable
