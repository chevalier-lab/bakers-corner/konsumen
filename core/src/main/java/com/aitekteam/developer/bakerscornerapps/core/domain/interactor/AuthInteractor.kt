package com.aitekteam.developer.bakerscornerapps.core.domain.interactor

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.Auth
import com.aitekteam.developer.bakerscornerapps.core.data.repository.AuthRepository
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.AuthUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody

class AuthInteractor(
    private val authRepository: AuthRepository
) : AuthUseCase {

    override fun login(email: RequestBody, password: RequestBody): Flow<Resource<Auth>> =
        authRepository.login(email, password)

    override fun register(jsonObject: JsonObject): Flow<Resource<Auth>> =
        authRepository.register(jsonObject)
}