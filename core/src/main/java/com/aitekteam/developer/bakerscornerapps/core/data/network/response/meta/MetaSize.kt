package com.aitekteam.developer.bakerscornerapps.core.data.network.response.meta

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MetaSize(
    @field:SerializedName("fetch")
    val fetch: Int? = null,

    @field:SerializedName("total")
    val total: Int? = null
) : Parcelable
