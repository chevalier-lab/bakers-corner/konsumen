package com.aitekteam.developer.bakerscornerapps.core.data.network.response.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MyFeedback(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("comment")
    val comment: String? = null,

    @field:SerializedName("is_visible")
    val isVisible: Int? = null,

    @field:SerializedName("created_at")
    val date: String? = null,

    @field:SerializedName("id_t_products")
    val idProduct: String? = null,

    @field:SerializedName("user")
    val user: User? = User(),
) : Parcelable