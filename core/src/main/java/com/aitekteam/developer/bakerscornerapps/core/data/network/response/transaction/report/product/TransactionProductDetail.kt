package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductItem
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionProductDetail(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("transaction_code")
    val code: String? = null,

    @field:SerializedName("total")
    val total: Long? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("discount_price")
    val discount: Long? = null,

    @field:SerializedName("payment_method")
    val method: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null,

    @field:SerializedName("is_already_feedback")
    val isAlreadyFeedback: Int? = null,

    @field:SerializedName("items")
    val items: List<ProductItem>? = listOf(),
) : Parcelable
