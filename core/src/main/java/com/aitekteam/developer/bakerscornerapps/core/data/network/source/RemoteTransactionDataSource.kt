package com.aitekteam.developer.bakerscornerapps.core.data.network.source

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.TransactionService
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteTransactionDataSource(
    private val transactionService: TransactionService
) {

    fun addRequestMenu(token: String, jsonObject: JsonObject): Flow<ApiResponse<CartRequest>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = transactionService.addRequestMenu(token, jsonObject)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun getRequestMenu(token: String): Flow<ApiResponse<List<CartRequest>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.getRequestMenu(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updateRequestMenu(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<ApiResponse<CartRequest>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = transactionService.updateRequestMenu(token, id, jsonObject)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun deleteRequestMenu(token: String, id: String): Flow<ApiResponse<CartRequest>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.deleteRequestMenu(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun checkoutRequestMenu(token: String): Flow<ApiResponse<RequestCheckout>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.checkoutRequestMenu(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun addToCartProduct(token: String, id: String, qty: Int): Flow<ApiResponse<CartProduct>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = transactionService.addToCart(token, id, qty)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun getCartProducts(
        token: String,
        page: Int,
        direction: String
    ): Flow<ApiResponse<ListCartProductResponse>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.getCartProducts(token, page, direction)

            when (response.code) {
                200 -> emit(ApiResponse.Success(response))
                else -> emit(ApiResponse.Error(response.message!!))
            }
            EspressoIdlingResource.decrement()

        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updateCartProduct(token: String, id: String, qty: Int): Flow<ApiResponse<CartProduct>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = transactionService.updateCartProduct(token, id, qty)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun deleteCartProduct(token: String, id: String): Flow<ApiResponse<CartProduct>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.deleteCartProduct(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun checkoutProduct(token: String, voucher: String): Flow<ApiResponse<ProductCheckout>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.checkoutProduct(token, voucher)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    403 -> emit(ApiResponse.Success(ProductCheckout()))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDetailTransaction(
        token: String,
        id: String
    ): Flow<ApiResponse<TransactionProductDetail>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.getDetailTransaction(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun cancelTransaction(token: String, id: String): Flow<ApiResponse<TransactionProduct>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.cancelTransaction(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDetailTransactionRequest(
        token: String,
        id: String
    ): Flow<ApiResponse<TransactionRequestDetail>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.getDetailTransactionRequest(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun cancelTransactionRequest(token: String, id: String): Flow<ApiResponse<TransactionProduct>> =
        flow {
            try {
                EspressoIdlingResource.increment()
                val response = transactionService.cancelTransactionRequest(token, id)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        else -> emit(ApiResponse.Error(response.message!!))
                    }
                    EspressoIdlingResource.decrement()
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun addFeedback(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<ApiResponse<TransactionProductDetail>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.addFeedback(token, id, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getFeedback(
        token: String,
        id: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<ApiResponse<List<Feedback>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = transactionService.getFeedback(token, id, page, search, direction)
            val dataArray = response.data

            dataArray?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}