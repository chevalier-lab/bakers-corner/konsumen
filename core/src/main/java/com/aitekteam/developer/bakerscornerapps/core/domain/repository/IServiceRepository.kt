package com.aitekteam.developer.bakerscornerapps.core.domain.repository

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.Banner
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.Chat
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.NotificationResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.Setting
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.Voucher
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ChatHistoryDomain
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody

interface IServiceRepository {

    fun getBanners(token: String): Flow<Resource<List<Banner>>>

    fun getAnnouncements(token: String): Flow<Resource<List<Announcement>>>

    fun activeAnnouncements(token: String): Flow<Resource<Announcement>>

    fun getNotifications(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<NotificationResponse>>

    fun getSetting(token: String): Flow<Resource<Setting>>

    fun changeVersionCode(token: String, jsonObject: JsonObject): Flow<Resource<Setting>>

    fun getVouchers(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<Voucher>>>

    fun checkVoucher(token: String, code: String): Flow<Resource<Voucher>>

    fun postChat(token: String, jsonObject: RequestBody): Flow<Resource<Chat>>
    fun getHistoryChat(token: String, id: String): Flow<Resource<List<ChatHistoryDomain>>>
}