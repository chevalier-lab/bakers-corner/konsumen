package com.aitekteam.developer.bakerscornerapps.core.data.repository

import android.content.Context
import com.aitekteam.developer.bakerscornerapps.core.data.NetworkBoundResource
import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.local.source.LocalChatDataSource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.announcement.Announcement
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.banner.Banner
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.Chat
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatHistory
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.notification.NotificationResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.setting.Setting
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher.Voucher
import com.aitekteam.developer.bakerscornerapps.core.data.network.source.RemoteServiceDataSource
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ChatHistoryDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.repository.IServiceRepository
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.AppExecutors
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.Helpers
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.Helpers.isOnline
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import okhttp3.RequestBody

class ServiceRepository(
    private val remoteServiceDataSource: RemoteServiceDataSource,
    private val localChatDataSource: LocalChatDataSource,
    private val appExecutors: AppExecutors,
    private val ctx: Context
) : IServiceRepository {
    override fun getBanners(token: String): Flow<Resource<List<Banner>>> = flow {
        emit(Resource.loading<List<Banner>>())
        val response = remoteServiceDataSource.getBanners(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Banner>()))
            is ApiResponse.Error -> emit(Resource.error<List<Banner>>(apiResponse.errorMessage))
        }
    }

    override fun getAnnouncements(token: String): Flow<Resource<List<Announcement>>> = flow {
        emit(Resource.loading<List<Announcement>>())
        val response = remoteServiceDataSource.getAnnouncements(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Announcement>()))
            is ApiResponse.Error -> emit(Resource.error<List<Announcement>>(apiResponse.errorMessage))
        }
    }

    override fun activeAnnouncements(token: String): Flow<Resource<Announcement>> = flow {
        emit(Resource.loading<Announcement>())
        val response = remoteServiceDataSource.activeAnnouncement(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Announcement()))
            is ApiResponse.Error -> emit(Resource.error<Announcement>(apiResponse.errorMessage))
        }
    }

    override fun getNotifications(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<NotificationResponse>> = flow {
        emit(Resource.loading<NotificationResponse>())
        val response = remoteServiceDataSource.getNotifications(token, page, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(NotificationResponse()))
            is ApiResponse.Error -> emit(Resource.error<NotificationResponse>(apiResponse.errorMessage))
        }
    }

    override fun getSetting(token: String): Flow<Resource<Setting>> = flow {
        emit(Resource.loading<Setting>())
        val response = remoteServiceDataSource.getSetting(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Setting()))
            is ApiResponse.Error -> emit(Resource.error<Setting>(apiResponse.errorMessage))
        }
    }

    override fun changeVersionCode(token: String, jsonObject: JsonObject): Flow<Resource<Setting>> =
        flow {
            emit(Resource.loading<Setting>())
            val response = remoteServiceDataSource.changeVersionCode(token, jsonObject)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(Setting()))
                is ApiResponse.Error -> emit(Resource.error<Setting>(apiResponse.errorMessage))
            }
        }

    override fun getVouchers(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<Voucher>>> = flow {
        emit(Resource.loading<List<Voucher>>())
        val response = remoteServiceDataSource.getVouchers(token, page, search, direction)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Voucher>()))
            is ApiResponse.Error -> emit(Resource.error<List<Voucher>>(apiResponse.errorMessage))
        }
    }

    override fun checkVoucher(token: String, code: String): Flow<Resource<Voucher>> = flow {
        emit(Resource.loading<Voucher>())
        val response = remoteServiceDataSource.checkVoucher(token, code)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Voucher()))
            is ApiResponse.Error -> emit(Resource.error<Voucher>(apiResponse.errorMessage))
        }
    }

    override fun postChat(token: String, jsonObject: RequestBody): Flow<Resource<Chat>> = flow {
        emit(Resource.loading<Chat>())
        val response = remoteServiceDataSource.postChat(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Chat()))
            is ApiResponse.Error -> emit(Resource.error<Chat>(apiResponse.errorMessage))
        }
    }

    override fun getHistoryChat(
        token: String,
        id: String
    ): Flow<Resource<List<ChatHistoryDomain>>> =
        object : NetworkBoundResource<List<ChatHistoryDomain>, List<ChatHistory>>(appExecutors) {

            override fun loadFromDB(): Flow<List<ChatHistoryDomain>> {
                return localChatDataSource.getHistoryChat()
                    .map { Helpers.mapEntityChatToDomain(it) }
            }

            override fun shouldFetch(data: List<ChatHistoryDomain>?) = isOnline(ctx)

            override suspend fun createCall(): Flow<ApiResponse<List<ChatHistory>>> =
                remoteServiceDataSource.getHistoryChat(token, id)

            override suspend fun saveCallResult(data: List<ChatHistory>) {
                val history = Helpers.mapNetworkChatToEntities(data)
                localChatDataSource.deleteChat()
                localChatDataSource.insertChat(history)
            }

//            emit(Resource.loading<List<ChatHistory>>())
//            val response = remoteServiceDataSource.getHistoryChat(token, id)
//
//            when (val apiResponse = response.first()) {
//                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
//                is ApiResponse.Empty -> emit(Resource.success(listOf<ChatHistory>()))
//                is ApiResponse.Error -> emit(Resource.error<List<ChatHistory>>(apiResponse.errorMessage))
//            }

        }.asFlow()
}