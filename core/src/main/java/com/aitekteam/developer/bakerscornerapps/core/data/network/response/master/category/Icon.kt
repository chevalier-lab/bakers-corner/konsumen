package com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Icon(
    @field:SerializedName("icon")
    val icon: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("media")
    val image: Media? = Media()
) : Parcelable