package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.voucher

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Voucher(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("voucher_code")
    val code: String? = null,

    @field:SerializedName("discount_price")
    val discountPrice: Long? = null,

    @field:SerializedName("start_date")
    val startDate: String? = null,

    @field:SerializedName("end_date")
    val endDate: String? = null,

    @field:SerializedName("media")
    val media: Media? = Media()
) : Parcelable
