package com.aitekteam.developer.bakerscornerapps.core.domain.repository

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

interface IMasterRepository {

    fun createMedia(token: String, photo: MultipartBody.Part): Flow<Resource<Media>>

    fun getMedia(token: String, id: String): Flow<Resource<Media>>

    fun getCategories(token: String): Flow<Resource<List<Category>>>

    fun getCategoriesFilter(token: String, page: Int, search: String, direction: String): Flow<Resource<List<Category>>>

    fun getProfile(token: String): Flow<Resource<User>>

    fun updateProfile(token: String, jsonObject: JsonObject): Flow<Resource<User>>
}