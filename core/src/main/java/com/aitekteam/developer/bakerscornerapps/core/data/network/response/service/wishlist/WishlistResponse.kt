package com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist

import android.os.Parcelable
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.meta.MetaData
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class WishlistResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<Wishlist>? = listOf(),

    @field:SerializedName("meta")
    val meta: MetaData? = MetaData(),
) : Parcelable
