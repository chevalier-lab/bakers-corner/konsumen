package com.aitekteam.developer.bakerscornerapps.core.domain.interactor

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedbackDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.data.repository.ProductRepository
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ProductDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.usecase.ProductUseCase
import kotlinx.coroutines.flow.Flow

class ProductInteractor(
    private val productRepository: ProductRepository
) : ProductUseCase {

    override fun getProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<ProductDomain>>> =
        productRepository.getProducts(token, page, search, direction)

    override fun recomendedProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<ProductsResponse>> =
        productRepository.recomendedProducts(token, page, search, direction)

    override fun getProductDetail(token: String, id: String): Flow<Resource<Product>> =
        productRepository.getProductDetail(token, id)

    override fun addToWishlist(token: String, id: String): Flow<Resource<Wishlist>> =
        productRepository.addToWishlist(token, id)

    override fun getWishlist(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<WishlistResponse>> =
        productRepository.getWishlist(token, page, direction)

    override fun getOnceWishlist(token: String, id: String): Flow<Resource<Wishlist>> =
        productRepository.getOnceWishlist(token, id)

    override fun deleteWishlist(token: String, id: String): Flow<Resource<Wishlist>> =
        productRepository.deleteWishlist(token, id)

    override fun getFeedbackCheck(token: String): Flow<Resource<String?>> =
        productRepository.getFeedbackCheck(token)

    override fun getMyFeedback(token: String): Flow<Resource<List<MyFeedback>>> =
        productRepository.getMyFeedback(token)

    override fun getMyFeedbackDetail(token: String, id: String): Flow<Resource<MyFeedbackDetail>> =
        productRepository.getMyFeedbackDetail(token, id)
}