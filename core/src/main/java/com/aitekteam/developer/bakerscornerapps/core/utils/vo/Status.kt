package com.aitekteam.developer.bakerscornerapps.core.utils.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}