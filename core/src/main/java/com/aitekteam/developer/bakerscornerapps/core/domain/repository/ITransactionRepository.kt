package com.aitekteam.developer.bakerscornerapps.core.domain.repository

import com.aitekteam.developer.bakerscornerapps.core.data.Resource
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Feedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckout
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProduct
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequest
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

interface ITransactionRepository {

    fun addRequestMenu(token: String, jsonObject: JsonObject): Flow<Resource<CartRequest>>

    fun getRequestMenu(token: String): Flow<Resource<List<CartRequest>>>

    fun updateRequestMenu(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<Resource<CartRequest>>

    fun deleteRequestMenu(token: String, id: String): Flow<Resource<CartRequest>>

    fun checkoutRequestMenu(token: String): Flow<Resource<RequestCheckout>>

    fun addToCartProduct(token: String, id: String, qty: Int): Flow<Resource<CartProduct>>

    fun getCartProducts(
        token: String,
        page: Int,
        direction: String
    ): Flow<Resource<ListCartProductResponse>>

    fun updateCartProduct(token: String, id: String, qty: Int): Flow<Resource<CartProduct>>

    fun deleteCartProduct(token: String, id: String): Flow<Resource<CartProduct>>

    fun checkoutProduct(token: String, voucher: String): Flow<Resource<ProductCheckout>>

    fun getDetailTransaction(token: String, id: String): Flow<Resource<TransactionProductDetail>>

    fun cancelTransaction(token: String, id: String): Flow<Resource<TransactionProduct>>

    fun getDetailTransactionRequest(
        token: String,
        id: String
    ): Flow<Resource<TransactionRequestDetail>>

    fun cancelTransactionRequest(token: String, id: String): Flow<Resource<TransactionProduct>>

    fun addFeedback(
        token: String,
        id: String,
        jsonObject: JsonObject
    ): Flow<Resource<TransactionProductDetail>>

    fun getFeedback(
        token: String,
        id: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<Resource<List<Feedback>>>
}