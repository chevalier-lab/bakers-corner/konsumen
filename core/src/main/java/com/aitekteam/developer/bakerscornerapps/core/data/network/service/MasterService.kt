package com.aitekteam.developer.bakerscornerapps.core.data.network.service

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.CategoryResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.MediaResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.UserResponse
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CATEGORY
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_PROFILE
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_MEDIA
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_UPDATE_PROFILE
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.http.*

interface MasterService {

    @Multipart
    @POST(ROUTE_MEDIA)
    suspend fun createMedia(
        @Header("Authorization") authorization: String,
        @Part photo: MultipartBody.Part
    ) : MediaResponse

    @GET(ROUTE_MEDIA)
    suspend fun getMedia(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : MediaResponse

    @GET(ROUTE_CATEGORY)
    suspend fun getCategories(
        @Query("order-direction") orderDirection: String,
        @Header("Authorization") authorization: String
    ) : CategoryResponse

    @GET(ROUTE_CATEGORY)
    suspend fun getCategoriesFilter(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String,
    ) : CategoryResponse

    @GET(ROUTE_GET_PROFILE)
    suspend fun getProfile(
        @Header("Authorization") authorization: String
    ) : UserResponse

    @POST(ROUTE_UPDATE_PROFILE)
    suspend fun updateProfile(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ) : UserResponse
}