package com.aitekteam.developer.bakerscornerapps.core.data.network.source

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedback
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.MyFeedbackDetail
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.ProductsResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.Wishlist
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.ProductService
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteProductDataSource(
    private val productService: ProductService
) {

    fun getProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<ApiResponse<List<Product>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.getProducts(token, page, search, direction)
            val dataArray = response.data

            dataArray?.let {
                if (it.isNotEmpty())
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getProductDetail(token: String, id: String): Flow<ApiResponse<Product>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.productDetail(token, id)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)


    fun recomendedProducts(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<ApiResponse<ProductsResponse>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.recomendedProduct(token, page, search, direction)

            if (response.code == 200)
                emit(ApiResponse.Success(response))
            else
                emit(ApiResponse.Empty)
            EspressoIdlingResource.decrement()

        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun addToWishlist(token: String, id: String): Flow<ApiResponse<Wishlist>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.addToWishlist(token, id)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getWishlist(token: String, page: Int, direction: String): Flow<ApiResponse<WishlistResponse>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.getWishlist(token, page, direction)

            if (response.code == 200)
                emit(ApiResponse.Success(response))
            else
                emit(ApiResponse.Empty)
            EspressoIdlingResource.decrement()

        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getOnceWishlist(token: String, id: String): Flow<ApiResponse<Wishlist>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.getOnceWishlist(token, id)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun deleteWishlist(token: String, id: String): Flow<ApiResponse<Wishlist>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.deleteWishlist(token, id)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    500 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getFeedbackCheck(token: String): Flow<ApiResponse<String?>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.getFeedbackCheck(token)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getMyFeedback(token: String): Flow<ApiResponse<List<MyFeedback>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.getMyFeedback(token)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getMyFeedbackDetail(token: String, id: String): Flow<ApiResponse<MyFeedbackDetail>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = productService.getMyFeedbackDetail(token, id)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}