package com.aitekteam.developer.bakerscornerapps.core.data.network.source

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.Auth
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.AuthService
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.RequestBody
import timber.log.Timber

class RemoteAuthDataSource(
    private val authService: AuthService
) {

    fun login(email: RequestBody, password: RequestBody): Flow<ApiResponse<Auth>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = authService.login(email, password)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    403 -> emit(ApiResponse.Empty)
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun register(jsonObject: JsonObject): Flow<ApiResponse<Auth>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = authService.register(jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

}