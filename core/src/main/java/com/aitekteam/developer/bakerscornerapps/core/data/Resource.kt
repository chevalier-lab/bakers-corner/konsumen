package com.aitekteam.developer.bakerscornerapps.core.data

import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.Status.*

data class Resource<T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> = Resource(SUCCESS, data, null)

        fun <T> error(msg: String?): Resource<T> = Resource(ERROR, null, msg)

        fun <T> loading(): Resource<T> = Resource(LOADING, null, null)
    }
}