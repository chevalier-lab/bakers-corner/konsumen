package com.aitekteam.developer.bakerscornerapps.core.data.network.service

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.*
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.SingleWishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.wishlist.WishlistResponse
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_ADD_FAVORITE
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_DELETE_WISHLIST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_DETAIL_PRODUCTS
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_FEEDBACK_CHECK
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_MY_FEEDBACK
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_MY_FEEDBACK_DETAIL
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_ONCE_WISHLIST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_PRODUCTS
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_WISHLIST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_RECOMENDED_PRODUCTS
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductService {
    @GET(ROUTE_GET_PRODUCTS)
    suspend fun getProducts(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String
    ): ProductsResponse

    @GET(ROUTE_GET_PRODUCTS)
    suspend fun filterProductsPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String,
        @Query("is_price_order") is_price_order: Boolean?,
        @Query("is_discount_order") is_discount_order: Boolean?
    ): Response<ProductsResponse>

    @GET(ROUTE_GET_PRODUCTS)
    suspend fun getProductsCategoryPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("category_id") categoryId: String
    ) : Response<ProductsResponse>

    @GET(ROUTE_DETAIL_PRODUCTS)
    suspend fun productDetail(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): ProductDetailResponse

    @GET(ROUTE_RECOMENDED_PRODUCTS)
    suspend fun recomendedProduct(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String
    ): ProductsResponse

    @GET(ROUTE_RECOMENDED_PRODUCTS)
    suspend fun recomendedProductPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String
    ): Response<ProductsResponse>

    @GET(ROUTE_ADD_FAVORITE)
    suspend fun addToWishlist(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): SingleWishlistResponse

    @GET(ROUTE_GET_WISHLIST)
    suspend fun getWishlist(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("order-direction") direction: String
    ): WishlistResponse

    @GET(ROUTE_GET_ONCE_WISHLIST)
    suspend fun getOnceWishlist(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): SingleWishlistResponse

    @GET(ROUTE_DELETE_WISHLIST)
    suspend fun deleteWishlist(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): SingleWishlistResponse

    @GET(ROUTE_GET_FEEDBACK_CHECK)
    suspend fun getFeedbackCheck(
        @Header("Authorization") authorization: String
    ): FeedbackCheck

    @GET(ROUTE_GET_MY_FEEDBACK)
    suspend fun getMyFeedback(
        @Header("Authorization") authorization: String
    ): MyFeedbackResponse

    @GET(ROUTE_GET_MY_FEEDBACK)
    suspend fun getMyFeedbackPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
    ): Response<MyFeedbackResponse>

    @GET(ROUTE_GET_MY_FEEDBACK_DETAIL)
    suspend fun getMyFeedbackDetail(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): MyFeedbackDetailResponse
}