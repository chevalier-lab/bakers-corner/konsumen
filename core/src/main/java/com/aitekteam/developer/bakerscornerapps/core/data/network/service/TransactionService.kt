package com.aitekteam.developer.bakerscornerapps.core.data.network.service

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.FeedbackResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.product.ProductCheckoutResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.checkout.request.RequestCheckoutResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.CartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.product.ListCartProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.CancelTranactionProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductDetailResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.product.TransactionProductResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestDetailResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.report.request.TransactionRequestResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.CartRequestResponse
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request.ListCartRequestResponse
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_ADD_FEEDBACK
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_ADD_PRODUCT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_ADD_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CANCEL_TRANSACTION_PRODUCT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CANCEL_TRANSACTION_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CHECKOUT_PRODUCT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_CHECKOUT_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_DELETE_CART
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_DELETE_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_CART
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_FEEDBACK
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_TRANSACTION_PRODUCT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_GET_TRANSACTION_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_LIST_TRANSACTION_PRODUCT
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_LIST_TRANSACTION_REQUEST
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_UPDATE_CART
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_UPDATE_REQUEST
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.*

interface TransactionService {

    @POST(ROUTE_ADD_REQUEST)
    suspend fun addRequestMenu(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ) : CartRequestResponse

    @GET(ROUTE_GET_REQUEST)
    suspend fun getRequestMenu(
        @Header("Authorization") authorization: String
    ) : ListCartRequestResponse

    @POST(ROUTE_UPDATE_REQUEST)
    suspend fun updateRequestMenu(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Body jsonObject: JsonObject
    ) : CartRequestResponse

    @GET(ROUTE_DELETE_REQUEST)
    suspend fun deleteRequestMenu(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : CartRequestResponse

    @GET(ROUTE_CHECKOUT_REQUEST)
    suspend fun checkoutRequestMenu(
        @Header("Authorization") authorization: String
    ) : RequestCheckoutResponse

    @GET(ROUTE_ADD_PRODUCT)
    suspend fun addToCart(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Query("qty") qty: Int
    ) : CartProductResponse

    @GET(ROUTE_GET_CART)
    suspend fun getCartProducts(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("order-direction") direction: String
    ) : ListCartProductResponse

    @GET(ROUTE_UPDATE_CART)
    suspend fun updateCartProduct(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Query("qty") qty: Int
    ) : CartProductResponse

    @GET(ROUTE_DELETE_CART)
    suspend fun deleteCartProduct(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : CartProductResponse

    @GET(ROUTE_CHECKOUT_PRODUCT)
    suspend fun checkoutProduct(
        @Header("Authorization") authorization: String,
        @Query("voucher") voucher: String
    ) : ProductCheckoutResponse

    @GET(ROUTE_LIST_TRANSACTION_PRODUCT)
    suspend fun getListTransactionPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String,
        @Query("status") status: Int?
    ) : Response<TransactionProductResponse>

    @GET(ROUTE_GET_TRANSACTION_PRODUCT)
    suspend fun getDetailTransaction(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : TransactionProductDetailResponse

    @GET(ROUTE_CANCEL_TRANSACTION_PRODUCT)
    suspend fun cancelTransaction(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : CancelTranactionProductResponse

    @GET(ROUTE_LIST_TRANSACTION_REQUEST)
    suspend fun getListTransactionRequestPaging(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String,
        @Query("status") status: Int?
    ) : Response<TransactionRequestResponse>

    @GET(ROUTE_GET_TRANSACTION_REQUEST)
    suspend fun getDetailTransactionRequest(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : TransactionRequestDetailResponse

    @GET(ROUTE_CANCEL_TRANSACTION_REQUEST)
    suspend fun cancelTransactionRequest(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ) : CancelTranactionProductResponse

    @POST(ROUTE_ADD_FEEDBACK)
    suspend fun addFeedback(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Body jsonObject: JsonObject
    ) : TransactionProductDetailResponse

    @GET(ROUTE_GET_FEEDBACK)
    suspend fun getFeedback(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String
    ) : FeedbackResponse

    @GET(ROUTE_GET_FEEDBACK)
    suspend fun getFeedbackPaging(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") direction: String
    ) : Response<FeedbackResponse>
}