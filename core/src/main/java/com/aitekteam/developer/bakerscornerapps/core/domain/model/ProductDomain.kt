package com.aitekteam.developer.bakerscornerapps.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductDomain(
    val productId: String?,
    val name: String?,
    val price: Long?,
    val discount: Int?,
    val category: String?,
    val image: String?,
    val visible: String?
) : Parcelable