package com.aitekteam.developer.bakerscornerapps.core.data.network.service

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.auth.AuthResponse
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_LOGIN
import com.aitekteam.developer.bakerscornerapps.core.utils.Constants.ROUTE_REGISTER
import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface AuthService {
    @Multipart
    @POST(ROUTE_LOGIN)
    suspend fun login(
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody
    ) : AuthResponse

    @POST(ROUTE_REGISTER)
    suspend fun register(
        @Body jsonObject: JsonObject
    ) : AuthResponse
}