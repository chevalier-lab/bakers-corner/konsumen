package com.aitekteam.developer.bakerscornerapps.core.data.network.source

import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.category.Category
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.media.Media
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.master.user.User
import com.aitekteam.developer.bakerscornerapps.core.data.network.service.MasterService
import com.aitekteam.developer.bakerscornerapps.core.utils.helper.EspressoIdlingResource
import com.aitekteam.developer.bakerscornerapps.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import timber.log.Timber

class RemoteMasterDataSource(
    private val masterService: MasterService
) {

    fun createMedia(token: String, photo: MultipartBody.Part): Flow<ApiResponse<Media>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = masterService.createMedia(token, photo)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getMedia(token: String, id: String): Flow<ApiResponse<Media>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = masterService.getMedia(token, id)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getCategories(token: String): Flow<ApiResponse<List<Category>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = masterService.getCategories("ASC", token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    403 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getCategoriesFilter(
        token: String,
        page: Int,
        search: String,
        direction: String
    ): Flow<ApiResponse<List<Category>>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = masterService.getCategoriesFilter(token, page, search, direction)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    403 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getProfile(token: String): Flow<ApiResponse<User>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = masterService.getProfile(token)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updateProfile(token: String, jsonObject: JsonObject): Flow<ApiResponse<User>> = flow {
        try {
            EspressoIdlingResource.increment()
            val response = masterService.updateProfile(token, jsonObject)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Empty)
                EspressoIdlingResource.decrement()
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}