package com.aitekteam.developer.bakerscornerapps.core.data.network.response.transaction.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CartRequest(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("qty")
    val qty: String? = null,

    @field:SerializedName("is_visible")
    val visible: String? = null,

    @field:SerializedName("updated_at")
    val date: String? = null,

    @field:SerializedName("product")
    val product: RequestMenu? = RequestMenu(),
) : Parcelable
