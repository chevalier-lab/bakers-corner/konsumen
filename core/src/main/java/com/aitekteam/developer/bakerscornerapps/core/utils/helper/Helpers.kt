package com.aitekteam.developer.bakerscornerapps.core.utils.helper

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ChatEntity
import com.aitekteam.developer.bakerscornerapps.core.data.local.entity.ProductEntity
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.product.Product
import com.aitekteam.developer.bakerscornerapps.core.data.network.response.service.chat.ChatHistory
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ChatHistoryDomain
import com.aitekteam.developer.bakerscornerapps.core.domain.model.ProductDomain

object Helpers {

    fun mapNetworkProductToEntities(data: List<Product>): List<ProductEntity> {
        val products = ArrayList<ProductEntity>()

        data.map {
            val product = ProductEntity(
                productId = it.id?.toInt(),
                name = it.name,
                price = it.price,
                discount = it.discount?.toInt(),
                category = it.category?.category,
                image = it.thumbnail?.url,
                visible = it.visible
            )
            products.add(product)
        }

        return products
    }

    fun mapEntityProductToDomain(data: List<ProductEntity>): List<ProductDomain> =
        data.map {
            ProductDomain(
                productId = it.productId.toString(),
                name = it.name,
                price = it.price,
                discount = it.discount,
                category = it.category,
                image = it.image,
                visible = it.visible
            )
        }

    fun mapDomainProductToEntity(data: ProductDomain) = ProductEntity(
        productId = data.productId?.toInt(),
        name = data.name,
        price = data.price,
        discount = data.discount,
        category = data.category,
        image = data.image,
        visible = data.visible
    )

    fun mapNetworkChatToEntities(data: List<ChatHistory>): List<ChatEntity> {
        val history = ArrayList<ChatEntity>()

        data.map {
            val chat = ChatEntity(
                content = it.content,
                createdAt = it.createdAt,
                updatedAt = it.updatedAt,
                idTChatUser = it.idTChatUser,
                idMUsers = it.idMUsers
            )
            history.add(chat)
        }

        return history
    }
    fun mapEntityChatToDomain(data: List<ChatEntity>): List<ChatHistoryDomain> =
        data.map {
            ChatHistoryDomain(
                content = it.content,
                createdAt = it.createdAt,
                updatedAt = it.updatedAt,
                idTChatUser = it.idTChatUser,
                idMUsers = it.idMUsers
            )
        }

//    fun isOnline(context: Context): Boolean {
//        val connectivityManager =
//            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            val capabilities =
//                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
//
//            if (capabilities != null) {
//                when {
//                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
//                        Timber.i("NetworkCapabilities.TRANSPORT_CELLULAR")
//                        return true
//                    }
//                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
//                        Timber.i("NetworkCapabilities.TRANSPORT_WIFI")
//                        return true
//                    }
//                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
//                        Timber.i("NetworkCapabilities.TRANSPORT_ETHERNET")
//                        return true
//                    }
//                }
//            }
//        }
//        return false
//    }

    fun isOnline(ctx: Context): Boolean {
        val connectionManager: ConnectivityManager =
            ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectionManager.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting === true
    }
}